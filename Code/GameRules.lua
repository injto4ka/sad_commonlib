AppendClass.GameRuleDef = {
	properties = {
		{ category = "General", id = "effect_type", editor = "choice", name = "Effect type", default = "positive",
			items = {"positive", "neutral", "negative"}, help = "The game rules with positive effects will disable certain achievements",
		},
	},
}

local origGetDisplayName = DisplayPreset.GetDisplayName
function GameRuleDef:GetDisplayName()
	local str = origGetDisplayName(self)
	if self.effect_type == "negative" then
		return TLookupTag("<negative>") .. str .. TLookupTag("</negative>")
	end
	return str
end

local origGetDescription = DisplayPreset.GetDescription
function GameRuleDef:GetDescription()
	local str = origGetDescription(self)
	if self.effect_type ~= "positive" then
		return str .. Untranslated("<newline>") .. Untranslated("<newline>") .. TLookupTag("<positive>") .. T(685843426756, "Unlock All Achievements") .. TLookupTag("</positive>")
	end
	if self.mod then
		return str .. Untranslated("<newline>") .. Untranslated("<newline>") .. T(758873891950, "Mod") .. Untranslated(" <em>" .. self.mod.title .. "</em>")
	end
	return str
end

local origGetGameSettingsTable = GetGameSettingsTable
function GetGameSettingsTablePositiveRules(...)
	local settings = origGetGameSettingsTable(...)
	local game_rules = {}
	for _, rule in ipairs(settings.game_rules) do
		if rule.effect_type == "positive" then
			game_rules[#game_rules + 1] = rule
		end
	end
	settings.game_rules = game_rules
	return settings
end

local origReloadMsgReactions = ReloadMsgReactions
function ReloadMsgReactions()
	local handlers = {}
	ForEachPreset("Achievement", function(preset)
		for _, reaction in ipairs(preset.msg_reactions) do
			local handler = reaction.Handler
			handlers[reaction] = handler
			reaction.Handler = function(...)
				GetGameSettingsTable = GetGameSettingsTablePositiveRules
				handler(...)
				GetGameSettingsTable = origGetGameSettingsTable
			end
		end
	end)

	origReloadMsgReactions()

	for reaction, handler in pairs(handlers) do
		reaction.Handler = handler
	end
end

local function ReloadMsgReactionsDelayed()
	DelayedCall(0, ReloadMsgReactions)
end

OnMsg.ModsReloaded = ReloadMsgReactionsDelayed
OnMsg.DataLoaded = ReloadMsgReactionsDelayed
OnMsg.PresetSave = ReloadMsgReactionsDelayed
OnMsg.DataReloadDone = ReloadMsgReactionsDelayed
