local function CanIgnore(id, reason)
	if id == "idDeleteScreen" then
		return true
	end
	if id == "idLoadingScreen" then
		if reason == "main menu" then
			return true
		end
		if string.starts_with(reason, "SaveAccountStorage") then
			return true
		end
		if reason == "save load" then
			return SavegamesList and not SavegamesList.invalid
		end
		if reason == "pregame menu" then
			return GetMap() == ""
		end
	end
end

-- Remove loading screen on savegame removal
local origLoadingScreenOpen = LoadingScreenOpen
function LoadingScreenOpen(id, reason, ...)
	--print("LoadingScreenOpen", id, reason)
	if CanIgnore(id, reason) then
		return
	end
	return origLoadingScreenOpen(id, reason, ...)
end

local origLoadingScreenClose = LoadingScreenClose
function LoadingScreenClose(id, reason, ...)
	local class = LoadingScreenGetClassById(id)
	local dlg = class and GetDialog(class)
	if not dlg or not dlg:GetOpenReasons()[reason] then
		return
	end
	return origLoadingScreenClose(id, reason, ...)
end