DefineModItemPreset("AttachedUIPreset", { EditorName = "Attached UI Preset", EditorSubmenu = "UI" })
DefineModItemPreset("BindingsMenuCategory", { EditorName = "Key Bindings Category", EditorSubmenu = "UI" })
DefineModItemPreset("TutorialHint", { EditorName = "Tutorial Hint", EditorSubmenu = "UI" })
DefineModItemPreset("Relationship", { EditorName = "Relationship", EditorSubmenu = "Gameplay" })
DefineModItemPreset("CooldownDef", { EditorName = "Cooldown", EditorSubmenu = "Gameplay" })
DefineModItemPreset("CRMaterial", { EditorName = "CR Material", EditorSubmenu = "ActionFX" })
DefineModItemPreset("WindDef", { EditorName = "Wind", EditorSubmenu = "ActionFX" })
DefineModItemPreset("UnitTags", { EditorName = "Unit Tag", EditorSubmenu = "Other" })
DefineModItemPreset("BugReportTag", { EditorName = "Bug Report Tag", EditorSubmenu = "Other" })

local function crm_classes()
	local list = ClassDescendantsList("CRMaterial", function(name, def)
		return not IsKindOf(def, "ModItem")
	end)
	table.sort(list)
	table.insert(list, 1, "")
	return list
end
AppendClass.ModItemCRMaterial = {
	properties = {
		{ id = "MaterialClass", name = "Type", editor = "choice", default = "", items = crm_classes, },
	},
}
function CR_CreateMaterial(id)
	local preset
	for _, group in ipairs(Presets.PersistedRenderVars) do
		preset = group[id]
		if preset then
			break
		end
	end
	if not preset then return end
	local class = preset.MaterialClass or ""
	local classdef = class ~= "" and g_Classes[class]
	if not classdef then
		return preset:Clone()
	end
	local mat = classdef:new()
	mat:CopyProperties(preset)
	return mat
end

function ModItemCRMaterial:GetProperties()
	local classdef = g_Classes[self.MaterialClass]
	if not classdef then
		return self.properties
	end
	local properties = table.icopy(self.properties)
	local ignore = {}
	for _, prop in ipairs(properties) do
		ignore[prop.id] = true
	end
	for _, prop in ipairs(CRMaterial.properties) do
		ignore[prop.id] = true
	end
	for _, prop in ipairs(classdef.properties) do
		if not ignore[prop.id] then
			properties[#properties + 1] = prop
		end
	end
	return properties
end

function ModItemCRMaterial:SetMaterialClass(class)
	local classdef = g_Classes[class]
	self.shader_id = classdef and classdef.shader_id
	self.MaterialClass = class
end

----

function GetModTemplates(item)
	local storage = LocalStorage
	local templates = table.get(LocalStorage, "ModTemplates", item.class)
	local names = templates and table.keys(templates, true) or {}
	table.insert(names, 1, "")
	return names
end

local GetParentTableOfKindNoCheck = GetParentTableOfKindNoCheck
function OtherModsCombo(self)
	local owner = GetParentTableOfKindNoCheck(self, "ModDef")
	local result = { }
	for id, mod in pairs(Mods) do
		if owner ~= mod then
			table.insert(result, { value = id, text = string.format("%s - %s (", mod.title, id) })
		end
	end
	table.sort(result, function(e1, e2) return CmpLower(e1.text, e2.text) end)
	return result
end

AppendClass.ModItem = {
	properties = {
		{ category = "Item", id = "NameColor", name = "Name Color", editor = "color", default = false, alpha = false, },
		{ category = "Mod", id = "Obsolete", name = "Disabled", editor = "bool", default = false, 
			no_edit = function(self) return not self.HasObsolete end, 
			dont_save = function(self) return not self.HasObsolete end, 
			help = "Disabled items are kept for compatibility (backwards or forward) and should not be visible in the game", },
		{ category = "Mod", id = "DisableIf",     name = "DisableIf",       editor = "func", default = empty_func, params = "self, mod", reapply = true, no_edit = PropChecker("HasDisableConditions", false), help = "The item won't be loaded if the expression returns true (requires Common Lib)" },
		{ category = "Mod", id = "DisableIfMods", name = "Disable If Mods", editor = "string_list", default = false, items = OtherModsCombo, reapply = true, no_edit = PropChecker("HasDisableConditions", false), help = "The item won't be loaded in the presence of tehse mods (requires Common Lib)" },
		{ category = "Mod", id = "RequiredDlc", name = "Required DLC", editor = "choice", default = "", items = DlcCombo(), no_edit = PropChecker("HasRequiredDlc", false), dont_save = PropChecker("HasRequiredDlc", false), help = "The item won't be loaded without these DLCs (requires Common Lib)" },
		{ category = "Mod", id = "ModTemplate", name = "Template", editor = "combo", default = "", items = GetModTemplates, dont_save = true, buttons = {{"Store", "StoreTemplate"}, {"Load", "LoadTemplate"}} },
		{ category = "Help", id = "ModHelp", editor = "help", default = "", help = function(self) return self.Documentation end, no_edit = function(self) return (self.Documentation or "") == "" end },
	},
	HasObsolete = false,
	HasRequiredDlc = true,
	OnLoadReject = empty_func,
	HasDisableConditions = false,
}

local GetModLoaded = GetModLoaded
function ModItem:IsModItemDisabled()
	if self.HasDisableConditions then
		for _, id in ipairs(self.DisableIfMods) do
			if GetModLoaded(id) then
				return true
			end
		end
		local condition = self.DisableIf or empty_func
		if condition ~= empty_func then
			local ok, disabled = procall(condition, self)
			if ok and disabled then
				return true
			end
		end
	end
	if self.HasRequiredDlc and (self.RequiredDlc or "") ~= "" and not IsDlcAvailable(self.RequiredDlc) then
		return true
	end
end

ModItemCode.HasRequiredDlc = false
ModItemCode.CodePriority = 100

function OnMsg.ClassesGenerate(classdefs)
	for classname, classdef in pairs(classdefs) do
		local origOnModLoad = classdef.OnModLoad
		if origOnModLoad then
			classdef.OnModLoad = function(self, mod, ...)
				if self:IsModItemDisabled() then
					self:OnLoadReject(mod)
					return
				end
				return origOnModLoad(self, mod, ...)
			end
		end
	end
end

function ModItem:StoreTemplate(obj, prop_id, ged)
	if self.ModTemplate == "" then
		return
	end
	local template = table.get(LocalStorage, "ModTemplates", self.class, self.ModTemplate)
	if template then
		if "ok" ~= ged:WaitQuestion(T(6902, "Warning"), T{883071764117, "Are you sure you want to overwrite <savename>?", savename = '"' .. Untranslated(self.ModTemplate) .. '"'}) then
			return
		end
	end
	local ignore_props = {
		name = true,
		Id = true,
		Group = true,
	}
	template = nil
	for _, prop in ipairs(self:GetProperties()) do
		if prop.editor and not prop_eval(prop.read_only, self, prop) and not prop_eval(prop.dont_save, self, prop)  then
			local id = prop.id
			if not ignore_props[id] then
				local value = self:GetProperty(id)
				if type(value) ~= "table" and not self:IsDefaultPropertyValue(id, prop, value) then
					template = template or {}
					template[id] = value
				end
			end
		end
	end
	table.set(LocalStorage, "ModTemplates", self.class, self.ModTemplate, template)
	SaveLocalStorageDelayed()
end

function ModItem:LoadTemplate(obj, prop_id, ged)
	local template = table.get(LocalStorage, "ModTemplates", self.class, self.ModTemplate)
	if not template then
		ged:ShowMessage(T(634182240966, "Error"), T(950959678764, "File not found."))
		return	
	end
	if "ok" ~= ged:WaitQuestion(T(6895, "Confirm"), T{883071764117, "Are you sure you want to overwrite <savename>?", savename = self.EditorView, self}) then
		return
	end
	self:SetProperties(template)
end

function ModItem:GetModItemDescription()
	local dscr = self.ModItemDescription
	if self.NameColor then
		dscr = Untranslated(ColorToTag(self.NameColor)) .. dscr .. Untranslated("</color>")
	end
	return self:AddEditorPostfix(dscr)
end

AutoResolveMethods.GetNewInModTag = "or"
ModItem.GetNewInModTag = empty_func

function ModItem:AddEditorPostfix(str)
	local tag = self:GetNewInModTag()
	if tag then
		str = self.NewFeatureTag .. str
	end
	if self.Obsolete then
		str = str .. Untranslated(" <image UI/Hud/check_false><style GedError>disabled</style>")
	end
	return str
end

local function GetModParent(parent)
	if IsKindOf(parent, "ModDef") then
		return parent
	end
	if not ParentTableCache[parent] then
		PopulateParentTableCache(Mods)
	end
	return GetParentTableOfKind(parent, "ModDef")
end

function ModItem:OnAfterEditorNew(parent, ged, is_paste)
	self.mod = self.mod or GetModParent(parent)
end

function ModItemOption:OnEditorNew(parent, ...)
	local mod = self.mod
	if mod then
		mod.has_options = true
		mod:LoadOptions()
	end
	return ModItem.OnEditorNew(self, parent, ...)
end

----
-- Obsolete! Use a const def instead!
DefineClass.Translation = {
	__parents = { "Preset", },

	properties = {
		{ id = "Text", name = "Text", editor = "text", default = "", translate = true, wordwrap = true, lines = 3, max_lines = 30 },
		{ id = "help", editor = "help", help = "This item is obsolete. Use a const def with a translated text instead!" },
	},

	GlobalMap = "Translations",
}

DefineModItemPreset("Translation", { EditorName = "Translation", EditorSubmenu = "Obsolete" })

function OnMsg.ModUnloadLua(id)
	if id == CurrentModId then
		Presets.Translation = nil
	end
end

----

-- remove the message "Your code has been loaded and is currently active in the game."
local origShowMessage = GedGameSocket.ShowMessage
function GedGameSocket:ShowMessage(title, text, ...)
	if title == "Information" and text == "Your code has been loaded and is currently active in the game." then
		return
	end
	return origShowMessage(self, title, text, ...)
end

-- alter the question "Mod <ModLabel> will be uploaded to Steam."
local origWaitQuestion = GedGameSocket.WaitQuestion
function GedGameSocket:WaitQuestion(title, text, ...)
	if type(text) == "table" and string.find_plain(text[2], "will be uploaded to Steam") and IsKindOf(text[3], "ModDef") then
		local mod = text[3]
		text[3] = text[3]:CreateInstance{
			version = text[3].version + 1, 
		}
	end
	return origWaitQuestion(self, title, text, ...)
end

local origTestModItem = ModItemCode.TestModItem
function ModItemCode:TestModItem(ged)
	local update = self.mod:UpdateCode()
	if not update then return end
	self.mod.code_hash = nil -- force update
	GedSetUiStatus("lua_reload", "Reloading Lua...")
	origTestModItem(self, ged)
	GedSetUiStatus("lua_reload")
end

----

DefineModItemPreset("MapGen", { EditorName = "Map Gen", EditorSubmenu = "Assets" })

function ModItemMapGen:TestModItem(ged)
	local prev_state = self.run_state or empty_table
	local state = {
		ged = ged,
		run_mode = self.RunMode,
		grids = prev_state.grids,
		params = prev_state.params,
	}
	self:ScheduleRun(state)
end

function ModItemMapGen:Run(...)
	GedSetUiStatus("map_gen", "Generating...")
	local err_msg = GridProcPreset.Run(self, ...)
	GedSetUiStatus("map_gen")
	return err_msg
end

AppendClass.ModItemMapGen = {
	properties = {
		{ category = "Operations", id = "OnChange", name = "On Change", editor = "choice", default = "", no_edit = true },
		{ category = "Operations", id = "RunMode",  name = "Run Mode",  editor = "choice", default = "Release", no_edit = true },
	},
}

----

local global_groups = { config = true, Platform = true, hr = true }

local ModItemConstDef_AssignToConsts = ModItemConstDef.AssignToConsts
function ModItemConstDef:AssignToConsts()
	if global_groups[self.group] and (self.id or "") ~= "" then
		local value = self.value
		if value == nil then
			value = ConstDef:GetDefaultValueOf(self.type)
		end
		ModSmartLogF(self.mod, "%s.%s = %s", tostring(self.group), tostring(self.id), SimpleValueToStr(value, 60))
		_G[self.group][self.id] = value
		return
	end
	ModSmartLogF(self.mod, "const.%s.%s = %s", tostring(self.group), tostring(self.id), SimpleValueToStr(self.value, 60))
	return ModItemConstDef_AssignToConsts(self)
end

function ModItemConstDef:GetLuaCode()
	if global_groups[self.group] and (self.id or "") ~= "" then
		return string_format("%s.%s", tostring(self.group), tostring(self.id))
	end
	return ConstDef.GetLuaCode(self)
end

local idx = table.find(ConstDef.properties, "id", "translate") or #ConstDef.properties
table.insert(ConstDef.properties, idx + 1, { id = "multiline", editor = "bool", default = false, no_edit = function(obj) return obj.type ~= "text" end, })
function FuncPieces:ConstDefLines()
	return self.multiline and 1
end
function FuncPieces:ConstDefItems(prop)
	return prop_eval(self.items, self, prop)
end
SetPropMeta("ConstDef", "value", "lines", FuncPieces.ConstDefLines, CurrentModDef)
SetPropMeta("ConstDef", "value", "items", FuncPieces.ConstDefItems, CurrentModDef)

DefineClass("ModItemConstDefAutoResolve")

AppendClass.ModItemConstDef = {
	__parents = { "ModItemConstDefAutoResolve" },
	properties = {
		{ category = "Item", id = "ValueColor", name = "Value Color", editor = "color", default = false, alpha = false, },
		{ id = "LoadBeforeCode", name = "Load Before Code", editor = "bool", default = false },
	},
	CodePriority = 10000,
	IgnoreLoadBeforeCode = false,
}

AppendClass.ConstDef = {
	properties = {
		{ id = "items", editor = "expression", default = false, params = "self, prop", no_edit = function(obj) return obj.type ~= "string_list" end, },
	},
}

function ModItemConstDef:GetModItemDescription()
	local name = self.id
	if self.group ~= "Default" then
		name = self.group .. "." .. name
	end
	local value = self:GetValueText()
	if self.ValueColor then
		value = ColorToTag(self.ValueColor) .. value .. "</color>"
	end
	return Untranslated(name .. " = " .. value)
end

function ModItemConstDef:GetCodeFileName(name, forced)
	name = name or self.id or ""
	if name == "" or not self.LoadBeforeCode and not self.IgnoreLoadBeforeCode then return end
	return string.format("Const/%s.lua", name:gsub('[/?<>\\:*|"]', "_"))
end

function ModItemConstDefAutoResolve:OnEditorSetProperty(prop_id, old_value, ged)
	if prop_id == "LoadBeforeCode" and old_value then
		CreateRealTimeThread(function()
			self.IgnoreLoadBeforeCode = true
			procall(self.OnEditorSetProperty, self, "Id", self.id, ged)
			self.IgnoreLoadBeforeCode = nil
		end)
	end
end

function ModItemConstDef:GenerateCompanionFileCode(code)
	if (self.id or "") == "" then return end
	local group = self.group or ""
	if group ~= "" and group ~= "Default" and not global_groups[group] then
		code:appendf("const.%s = const.%s or {}\n", group, group)
	end
	code:append(self:GetLuaCode())
	code:append(" = ")
	code:append(ValueToLuaCode(self.value))
end

local origGetValueText = ConstDef.GetValueText
function ConstDef:GetValueText()
	local text = origGetValueText(self)
	if type(text) ~= "string" then
		text = SimpleValueToStr(text, 60)
	end
	if text == "???" then
		text = SimpleValueToStr(self.value, 60)
	end
	local idx = string.find_plain(text, "\n")
	if idx then
		text = string.sub(text, 1, idx-1)
	end
	return text
end

----

-- allow the convert assets item to work with uppercase extensions
for name, info in pairs(ModAssetTypeInfo) do
	local exts = {}
	for _, ext in ipairs(info.ext) do
		exts[string.lower(ext)] = true
		exts[string.upper(ext)] = true
	end
	if next(exts) then
		info.ext = table.keys(exts, true)
	end
end

ModItemConvertAsset.name = "ConvertAsset"
ModItemConvertAsset.OnEditorNew = nil
ModItemConvertAsset.OnEditorSetProperty = nil

function ModItemConvertAsset:GetdestFolder()
	if not self.mod or not ModAssetTypeInfo[self.assetType] then return "" end
	return self.mod.content_path .. ModAssetTypeInfo[self.assetType].folder
end

function ModItemConvertAsset:GetallowedExt()
	local info = ModAssetTypeInfo[self.assetType] or {}
	local exts = {}
	for _, ext in ipairs(info.ext) do
		exts[string.lower(ext)] = true
	end
	exts = table.keys(exts, true)
	return table.concat(exts, " | ")
end

local ModItemConvertAsset_Import = ModItemConvertAsset.Import
function ModItemConvertAsset:Import(...)
	self.destFolder = self:GetdestFolder()
	return ModItemConvertAsset_Import(self, ...)
end

----

AppendClass.ModItemPreset = {
	properties = {
		{
			category = "Preset", id = "OpenPresetEditor",
			editor = "buttons", default = false,
			buttons = {{ func = "OpenModItemPresetEditor", name = "Open in Preset Editor" }},
			no_edit = function(self)
				return table.get(Presets, self.ModdedPresetClass, self.group, self.id) ~= self
			end,
		},
		{ category = "Mod", id = "Obsolete", name = "Disabled", editor = "bool", default = false, 
			no_edit = function(self) return not self.HasObsolete end, 
			dont_save = function(self) return not self.HasObsolete end, 
			help = "Disabled items are kept for compatibility (backwards or forward) and should not be visible in the game",
		},
	},
	HasObsolete = true,
}

function ModItemPreset:OpenModItemPresetEditor(root, prop_id, ged)
	if self.ModdedPresetClass then
		OpenPresetEditor(self.ModdedPresetClass)
	end
end

local origSaveAll = Preset.SaveAll
function Preset:SaveAll(force_save_all, by_user_request, ged)
	PauseInfiniteLoopDetection("ModItemPreset:SaveAll")
	
	local dirty_mods = {}
	local class = self.PresetClass or self.class
	ForEachPresetExtended(class, function(preset, group)
		if preset.mod and not dirty_mods[preset.mod] and (force_save_all or preset:IsDirty()) then
			dirty_mods[preset.mod] = true
			dirty_mods[#dirty_mods + 1] = preset.mod
		end
	end)
	
	for _, mod in ipairs(dirty_mods) do
		mod:SaveItems()
	end
	
	ForEachPresetExtended(class, function(preset, group)
		if preset.mod and (force_save_all or preset:IsDirty()) then
			preset:MarkClean()
		end
	end)
	
	ResumeInfiniteLoopDetection("ModItemPreset:SaveAll")

	return origSaveAll(self, force_save_all, by_user_request, ged)
end

function ModItemPreset:OnLoadReject(mod)
	-- hacky unregister
	procall(Preset.Done, self, "")
end

----

DefineModItemPreset("AnimMetadata", { EditorName = "Anim Metadata", EditorSubmenu = "Other" })

AppendClass.ModItemAnimMetadata = {
	properties = {
		{ category = "Preset", id = "Group", name = "Group", editor = "choice", default = "", items = GetAllAnimatedEntities },
		{ category = "Preset", id = "Id",    name = "Id",    editor = "choice", default = "idle", items = function(self) return IsValidEntity(self.group) and GetStates(self.group) or {} end },
	}
}

function AppearanceLocateByAnimation(animation, target)
	return target
end

function ModItemAnimMetadata:OpenModItemPresetEditor(root, prop_id, ged)
	OpenAnimationMomentsEditor(self.group, self.id)
end

----

function ModItem:TryStoreOSPath(prop_meta)
	if prop_meta and prop_meta.os_path and prop_meta.dont_save then
		local prop_id = prop_meta.id
		self:StoreOSPath(prop_id, self:GetProperty(prop_id))
	end
end

function ModItem:StoreOSPaths()
	for _, prop_meta in ipairs(self:GetProperties()) do
		self:TryStoreOSPath(prop_meta)
	end
end

function ModItem:OnEditorSetProperty(prop_id, old_value, ged)
	self:TryStoreOSPath(self:GetPropertyMetadata(prop_id))
end

function ModItem:StoreOSPath(prop_id, value)
	local key = self:GetPropOSPathKey(prop_id)
	if type(value) ~= "string" or value == "" then
		value = nil
		local prev_value = table.get(LocalStorage, "ModItemOSPaths", self.mod.id, key)
		if prev_value == nil then
			return
		end
	end
	table.set(LocalStorage, "ModItemOSPaths", self.mod.id, key, value)
	SaveLocalStorageDelayed()
end

----

local classes
local function BaseEntityClasses()
	if not classes then
		classes = {}
		local entities = EntityData
		for name, def in pairs(g_Classes) do
			if not entities[name] then
				classes[#classes + 1] = name
			end
		end
		table.sort(classes)
	end
	return classes
end

AppendClass.BasicEntitySpecProperties = {
	properties = {
		{ id = "ClassParents", name = "Class Parents", editor = "string_list", items = BaseEntityClasses, default = false, category = "Misc", help = "Classes which this entity class should inherit (comma separated)." }, 
	},
}

function BasicEntitySpecProperties:GetClassParents()
	local classes = {}
	for class in string.gmatch(self.class_parent, '([^,; ]+)') do
		classes[#classes + 1] = class
	end	
	return classes
end

function BasicEntitySpecProperties:SetClassParents(classes)
	self.class_parent = table.concat(classes, ",")
end

----

DefineModItemPreset("AnimalPerk", {
	EditorName = "Animal Perk",
	EditorSubmenu = "Animals",
	Documentation = "Used only for UI",
})

----

ModItemOptionToggle.EditorName = "Option Toggle"
ModItemOptionNumber.EditorName = "Option Number"
ModItemOptionChoice.EditorName = "Option Choice"

----

DefineClass.ModItemUpdate = {
	__parents = { "ModItem" },

	properties = {
		{ id = "whats_new_show", name = "Update notification", editor = "choice", default = "only once", items = { "only once", "on new game", "always" }, help = "Show a notification and a popup about the mod being updated."},
		{ id = "mod_map_only", name = "Mod map only", editor = "bool", default = false, help = "Show only on the mod editor map"},
		{ id = "large_popup", name = "Use Large Popup", editor = "bool", default = false, help = "Show a larger popup dialog"},
		{ id = "version_major", name = "Major version", editor = "number", default = 0, help = "Used to show the update's mod version, to sort the notifications when being shown, and to check if the notification has been shown previously"},
		{ id = "version_minor", name = "Minor version", editor = "number", default = 0, help = "Used to show the update's mod version, to sort the notifications when being shown, and to check if the notification has been shown previously"},
		{ id = "whats_new_message", name = "Update text to show", editor = "text", default = "", translate = true, lines = 3, max_lines = 10, help = "Notification text about the update."},
		{ id = "whats_new_image", name = "Update image to show", editor = "ui_image", default = "", help = "If not used, the mod's image will be used."},
		{ id = "whats_new_time", editor = "number", default = 0, no_edit = true},
		{ id = "WhatsNewTime", name = "Update date to show", editor = "text", default = 0, read_only = true, dont_save = true, buttons = {{"Update", "UpdateDate"}}},
	},
	
	EditorName = "Mod Update",
	EditorSubmenu = "UI",
	HasObsolete = true,
}

function ModItemUpdate:OnEditorNew(parent, ...)
	local mod = self.mod
	self.version_major = mod.version_major
	self.version_minor = mod.version_minor
	self.whats_new_time = mod.saved
	return ModItem.OnEditorNew(self, parent, ...)
end

function ModItemUpdate:GetWhatsNewTime()
	return TTranslate(Untranslated("<os_date(whats_new_time)>"), self, false)
end

function ModItemUpdate:UpdateDate()
	self.whats_new_time = self.mod.saved
end

function ModItemUpdate:ShowNotification()
	local mod = self.mod
	local title = Untranslated(mod.title)
	local version_str = string_format("%d.%02d", self.version_major, self.version_minor)
	local id = "WhatsNew_" .. mod.id .. "_" .. self.name
	local date_str = Untranslated(self:GetWhatsNewTime())
	local short_version = T{10484, "v.<version>", version = Untranslated(version_str)}
	local full_title = TTranslate(table.concat{title, Untranslated(" <color 138 138 45>"), short_version, Untranslated("</color> <color 45 138 138>"), date_str, Untranslated("</color>")}, nil, false)
	local rollover_text = TTranslate(T{423675131193, "Version <u(ver)><opt(build, ' - ', '')>", ver = version_str, build = date_str}, nil, false)
	local image = self.whats_new_image
	local dummy = StoryBit:new{
		LargePopup = self.large_popup,
		Category = "",
		Enabled = true,
		Image = (image or "") ~= "" and image or mod.image,
		NotificationRolloverText = Untranslated(rollover_text),
		NotificationRolloverTitle = T(976054118486, "New Update Available"),
		NotificationTitle = title,
		ExpirationTime = max_int,
		Text = self.whats_new_message,
		Title = Untranslated(full_title),
		Trigger = "",
		id = id,
	}
	StoryBits[id] = dummy
	ForceActivateStoryBit(id, nil, "immediate", nil, true)
end

function ModItemUpdate:TestModItem(ged, ...)
	self:ShowNotification()
end

function ModItemUpdate:Compare(other)
	if self.version_major ~= other.version_major then
		return self.version_major < other.version_major
	end
	return self.version_minor < other.version_minor
end

local function ShowWhatsNewNotification(new_game)
	CreateRealTimeThread(function()
		WaitSurvivorsPlaced()
		local shown = 0
		local storage = CurrentModStorageTable or {}
		CurrentModStorageTable = storage
		storage.whats_new_shown = nil -- compatibility
		for _, mod in ipairs(ModsLoaded) do
			local updates = mod:GetItemsOfKind("ModItemUpdate")
			if #updates > 0 then
				table.sort(updates, ModItemUpdate.Compare)
				for _, update in ipairs(updates) do
					local update_shown = table.get(storage, "updates_shown", mod.id, update.name)
					local show = update.whats_new_show
					if update.whats_new_message ~= ""
					and not update.Obsolete
					and (show == "always" or not update_shown or show == "on new game" and new_game)
					and (not update.mod_map_only or IsModEditorMap()) then
						table.set(storage, "updates_shown", mod.id, update.name, true)
						shown = shown + 1
						update:ShowNotification()
					end
				end
			end
		end
		if shown > 0 then
			WriteModPersistentStorageTable()
		end
	end)
end
	
function OnMsg.NewMapLoaded()
	ShowWhatsNewNotification(true)
end

function OnMsg.LoadGame()
	ShowWhatsNewNotification()
end

----

function LoadModTranslationTables()
	ForEachLoadedModItem("ModItemLocTable", function(item, language)
		if item.language == language or item.language == "Any" then
			LoadTranslationTableFile(item.filename)
		end
	end, GetLanguage())
end

local translation_tables_changed
local origLoadTranslationTables = LoadTranslationTables
function LoadTranslationTables()
	TranslationGenderTable = {}
	if not Loading then
		translation_tables_changed = true
	end
	return origLoadTranslationTables()
end

local origLoadTranslationTablesFolder = LoadTranslationTablesFolder
function LoadTranslationTablesFolder(...)
	if translation_tables_changed then
		translation_tables_changed = false
		procall(LoadModTranslationTables)
	end
	return origLoadTranslationTablesFolder(...)
end

function CleanupTranslationTables()
	local tbl = TranslationGenderTable
	for id, gender in pairs(tbl) do
		if (gender or "") == "" then
			tbl[id] = nil
		end
	end
end

function OnMsg.TranslationChanged()
	DelayedCall(0, CleanupTranslationTables)
end


----

if Platform.debug then

local orig_GridProcPreset_ScheduleRun = GridProcPreset.ScheduleRun
function GridProcPreset:ScheduleRun(state, ...)
	state = state or {}
	state.run_mode = state.run_mode or self.RunMode
	return orig_GridProcPreset_ScheduleRun(self, state, ...)
end

end -- Platform.debug