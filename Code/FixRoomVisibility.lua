local gofPermanent = const.gofPermanent
local EncodeVoxelPos = EncodeVoxelPos

function EncodeAndPushXYZ(x, y, z, pos_map)
	pos_map[EncodeVoxelPos(x, y, z)] = true
end
function EncodeAndPopObj(obj, pos_map)
	return pos_map[EncodeVoxelPos(obj)]
end

function ComplexRoom:CalcRoomVisibilityLevel()
	local pos_map = {}
	self:ForEachVoxel(EncodeAndPushXYZ, pos_map)
	local support_slabs = MapGet(self.box, "HorizontalSupportSlab", nil, nil, gofPermanent, EncodeAndPopObj, pos_map)
	if not next(support_slabs) then
		return 0
	end
	
	local GetTopmostContainer, GetTopmostBuilding = GetTopmostContainer, GetTopmostBuilding
	local max_level = GetConst("Gameplay", "MaxRoomLevels", 6) - 1
	local levels_sum, levels_count = 0, 0
	for _, slab in ipairs(support_slabs) do
		local container = GetTopmostContainer(slab, "VolumeElementBuilding") or GetTopmostBuilding(slab)
		local level = Min(max_level, GetMaxRoomVisibilityLevelForObj(container, 0))
		levels_sum = levels_sum + level
		levels_count = levels_count + 1
	end
	
	local visibility_level = Max(1, (levels_sum + levels_count/2) / levels_count)
	return visibility_level
end

function SavegameFixups.ZUpdateRoomVisibilityLevels3()
	EnumVolumes("ComplexRoom", function(room)
		room:UpdateRoomVisibilityLevel()
	end)
end