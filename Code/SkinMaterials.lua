local format = string_format
local IsValidEntity = IsValidEntity
local tostring = tostring
local weighted_rand = table.weighted_rand
local noneWallMat = const.SlabNoMaterial
local clrNoModifier = const.clrNoModifier
local GetTopmostBuilding = GetTopmostBuilding
local SetOrigColorModifier = SetOrigColorModifier

AppendClass.SlabPreset = {
	properties = {
		{ id = "EntitySet", name = "Custom Skin", editor = "preset_id", default = "",
			preset_class = "SlabPreset", preset_group = function(self) return self.group end,
			help = "Force a custom appearance based on another existing material",
		},
	},
}

local origRefreshColors = CSlab.RefreshColors
function CSlab:RefreshColors()
	local bld = GetTopmostBuilding(self)
	local clr = bld and bld.FinalColor or clrNoModifier
	if clr ~= clrNoModifier then
		SetOrigColorModifier(self, clr)
	end
	return origRefreshColors(self)
end

SetPropMeta("SimBuilding", "FinalColor", "editor", "rgbrm", CurrentModDef)

SetPropMeta("SlabPreset", "SimMaterial", "name", "Sim Material", CurrentModDef)
SetPropMeta("SlabPreset", "SimMaterial", "help", "Specifies the thermal insulation properties", CurrentModDef)

local function GetSkinMaterial(self)
	local preset = self:GetMaterialPreset()
	local skin = preset and preset.EntitySet or ""
	return skin ~= "" and skin or self.material
end

function GetSlabEntityWithType(self)
	return format("%s_%s_%s", self.entity_base_name, GetSkinMaterial(self), self.element_type)
end

FortSlab.GetBaseEntityName = GetSlabEntityWithType
CeilingSlabElement.GetBaseEntityName = GetSlabEntityWithType
TerraceSlabElement.GetBaseEntityName = GetSlabEntityWithType
TerraceWallElement.GetBaseEntityName = GetSlabEntityWithType

function GetSlabEntity(self)
	return format("%s_%s", self.entity_base_name, GetSkinMaterial(self))
end

CSlab.GetBaseEntityName = GetSlabEntity
LadderSlab.GetBaseEntityName = GetSlabEntity
FloorSlab.GetBaseEntityName = GetSlabEntity
CeilingSlab.GetBaseEntityName = GetSlabEntity
StairSlab.GetBaseEntityName = GetSlabEntity

function Slab:GetBaseEntityName()
	return format("%sExt_%s_Wall_%s", self.entity_base_name, GetSkinMaterial(self), variantToVariantName[self.variant])
end

function ArchSlabElement:GetBaseEntityName()
	return format("%s%s_%s", self.entity_base_name, self.element_type, GetSkinMaterial(self))
end

function ComplexRoomCorner:GetBaseEntityName()
	return format("WallExt_%s_Corner", GetSkinMaterial(self))
end

function RoomWallColumnSlab:GetBaseEntityName()
	return format("WallExt_%s_Marker", GetSkinMaterial(self))
end

local roofCompToSubvariantArr = {
	Plane = "subvariants",
	Eave = "eave_subvariants",
	Rake = "rake_subvariants",
	Ridge = "ridge_subvariants",
	Gable = "gable_subvariants",
	RakeGable = "rake_gable_subvariants",
	RakeRidge = "rake_ridge_subvariants",
	RakeEave = "rake_eave_subvariants",
	GableCrest = "crest_subvariants",
	RakeGableCrestTop = "crest_top_subvariants",
	RakeGableCrestBot = "crest_bot_subvariants",
	GableSlope = "slope_subvariants",
	RakeGableSlopeTop = "slope_top_subvariants",
	RakeGableSlopeBot = "slope_bot_subvariants",
}

function RoofSlab:ComposeEntityName()
	local presets = Presets.SlabPreset
	local material_list = presets[self.MaterialListClass] or presets.RoofSlabMaterials
	local svd = material_list[self.material]
	local skin = svd and svd.EntitySet or ""
	local material = skin ~= "" and skin or self.material or noneWallMat
	
	local sm = roofCompToSubvariantArr[self.roof_comp]
	local subvariants = svd and svd[sm]
	if subvariants and #subvariants > 0 then
		if self.subvariant ~= -1 then --user selected subvar
			local digit = ((self.subvariant - 1) % #subvariants) + 1 --assumes "01, 02, etc. suffixes
			local digitStr = digit < 10 and "0" .. tostring(digit) or tostring(digit)
			return format("Roof_%s_%s_%s", material, self.roof_comp, digitStr)
		else
			local subvariant, i = weighted_rand(subvariants, "chance", self:GetSeed())
			while subvariant do
				local name = format("Roof_%s_%s_%s", material, self.roof_comp, subvariant.suffix)
				if IsValidEntity(name) then
					return name
				end
				i = i - 1
				subvariant = subvariants[i]
			end
		end
	end
	return format("Roof_%s_%s_01", material, self.roof_comp)
end

----

ModItemSlabMaterials.EditorName = "Slab material: Wall"
ModItemShelterSlabMaterials.EditorName = "Slab material: Shelter"
ModItemRoofSlabMaterials.EditorName = "Slab material: Roof"
ModItemFloorSlabMaterials.EditorName = "Slab material: Floor"
ModItemTerraceSlabMaterials.EditorName = "Slab material: Foundation"
ModItemTerraceSlabMaterials.EditorName = "Slab material: Foundation"
ModItemRackSlabMaterials.EditorName = "Slab material: Rack"
ModItemLadderSlabMaterials.EditorName = "Slab material: Ladder"
ModItemFortificationSlabMaterials.EditorName = "Slab material: Fortification"
ModItemFenceSlabMaterials.EditorName = "Slab material: Fence"
ModItemCeilingSlabMaterials.EditorName = "Slab material: Ceiling"
ModItemBalconySlabMaterials.EditorName = "Slab material: Balcony"
ModItemStairsSlabMaterials.EditorName = "Slab material: Stairs"
