if FirstLoad then
	XEditorSelectSingleObjects = 1 -- single selection in map editor by default
end

local options = CurrentModOptions or empty_table

SetPropMeta("XEditorObjectPalette", "ArtSets", "default", { "All" }, CurrentModDef)

function ShowMapEditorHelp(host)
	CreateMessageBox(host or XShortcutsTarget, const.Translation.SAD_MapEditorHelp_Title,  const.Translation.SAD_MapEditorHelp_Body)
end

function OnMsg.GameEnterEditor()
	local storage = CurrentModStorageTable or {}
	if storage.editor_help_shown then return end
	ShowMapEditorHelp()
	storage.editor_help_shown = true
	WriteModPersistentStorageTable()
end

local XEditorFilters_GetCategories = XEditorFilters.GetCategories
function XEditorFilters.GetCategories()
	if Platform.developer then
		return XEditorFilters_GetCategories()
	end
	Platform.developer = true
	local res = XEditorFilters_GetCategories()
	Platform.developer = nil
	return res
end

----

if Platform.debug then
	function OnMsg.ClassesGenerate(classdefs)
		for _, class in pairs(classdefs) do
			for _, prop_meta in ipairs(class.properties) do
				prop_meta.developer = nil
			end
		end
	end
end

AppendClass.XEditorSettings = {
	properties = {
		{ category = "UI", id = "EditorToolbarWidth",  editor = "number", name = "Editor Toolbar Width", default = 180 },
	}
}

function ApplyEditorToolbarWidth(width)
	for _, parent in pairs{XTemplates.XEditorToolbar, XShortcutsTarget} do
		UIForEachControl(parent, function(control, parent_control)
			if control.Id == "idToolbar" and parent_control.Dock == "left" then
				parent_control:SetProperty("MaxWidth", width)
				control:SetProperty("MaxWidth", width)
				return true
			end
		end)
	end
end

function OnMsg.ModsReloaded()
	ApplyEditorToolbarWidth(XEditorSettings:GetEditorToolbarWidth())
end

function OnMsg.EditorSettingChanged(prop, value)
	if prop == "EditorToolbarWidth" then
		ApplyEditorToolbarWidth(value)
	end
end

----

if FirstLoad then
	GameStarting = true
end
local FixInitialCamera = false
local origOpenPreGameMainMenu = OpenPreGameMainMenu
function OpenPreGameMainMenu()
	if GameStarting then
		GameStarting = false
		local autostart = LocalStorage.AutoStart
		if autostart or options.StartOnEditorMap and Platform.debug then
			local mod, map, select
			if autostart then
				mod, map, select = autostart.Mod, autostart.Map, autostart.Select
				LocalStorage.AutoStart = nil
				SaveLocalStorageDelayed()
			end
			ResetGameSession()
			if ChangingMap then
				WaitMsg("ChangeMapDone")
			end
			CloseMenuDialogs()
			FixInitialCamera = true
			if not map or not MapData[map] then
				map = ModEditorMapName
			end
			ChangeMap(map)
			CreateRealTimeThread(function()
				WaitOpenModManager()
				if mod and Mods[mod] then
					local editor = OpenModEditor(Mods[mod])
					if editor and select then
						editor:Send("rfnApp", "SetSelection", "root", { 1, select })
					end
				end
			end)
			return
		end
	end
	return origOpenPreGameMainMenu()
end

function OnMsg.NewMapLoaded()
	if FixInitialCamera then
		FixInitialCamera = true
		SetupInitialCamera()
	end
end

----

function OnMsg.Autorun()
	local ged = FindGedApp("XEditorSettings")
	if ged then
		CloseGedApp(ged)
	end
end

