local options = CurrentModOptions or empty_table

-- Save any errors in the save game in order to debug

MapVar("LuaErrors", false)

GamePath = ConvertToOSPath(".") .. "\\"
AppdataPath = ConvertToOSPath("AppData/.") .. "\\"

local GamePath, AppdataPath = GamePath, AppdataPath

local function HandleLuaError(err, stack)
	if not stack then
		return
	end
	local errors = LuaErrors
	if not errors then
		errors = {}
		LuaErrors = errors
	end
	if errors[err] or string.find_plain(stack, "ConsoleExec") then
		return
	end
	-- remove player info
	stack = string.remove(stack, GamePath)
	stack = string.remove(stack, AppdataPath)
	errors[err] = stack
end

local in_error
function OnMsg.OnLuaError(err, stack)
	if in_error then return end
	in_error = true
	pcall(HandleLuaError, err, stack)
	in_error = false
end

----

ReportedModLuaError = true
ReportModLuaError = empty_func

local report_via_UI = not Platform.debug
if report_via_UI then

	local ModsToReport = false

	function ReportModLuaError(mod, err, stack)
		if options.DisableErrorReporting then
			return
		end
		local reported = ReportedMods
		if not reported then
			reported = {}
			ReportedMods = reported
		end
		if reported[mod.id] then
			return
		end
		reported[mod.id] = RealTime()
		if ModsToReport then
			ModsToReport[#ModsToReport + 1] = mod
			return
		end
		ModsToReport = {mod}
		CreateRealTimeThread(function()
			Sleep(1)
			local list = ModsToReport
			ModsToReport = false
			local mods = {}
			for _, mod in ipairs(list) do
				local v_major = mod.version_major or ModDef.version_major
				local v_minor = mod.version_minor or ModDef.version_minor
				local v = mod.version or ModDef.version
				local ver_str = string_format("%d.%02d-%03d", v_major or 0, v_minor or 0, v or 0)
				mod_print("Error in mod %s (id %s, v%s) from %s", mod.title, mod.id, ver_str, mod.source)
				mods[#mods + 1] = mod.title
			end
			local mods_str = table.concat(mods, "\n")

			local err = table.concat{
				T(12646, "Mod-related problem detected in the game logic. Try disabling the mods in case of unexpected game behavior."),
				Untranslated("\n\n"),
				T(796336029093, "Mod Flagged"),
				Untranslated(": "),
				Untranslated("\n\n"),
				Untranslated(mods_str)
			}
			CreateMessageBox(GetInGameInterface(), T(6902, "Warning"), err, T(1000136, "OK"))
		end)
	end
end

function HandleFolderCreationErr(path, create_err, context_str)
	if (create_err or "") == "" or not MarkHash("HandleFolderCreationErr", path, create_err) then
		return
	end
	local title = T(768699500779, "Critical Error")
	local err = table.concat{
		Untranslated("<center><em>"),
		context_str or "",
		Untranslated("</em><left><newline><newline><newline>"),
		T{ 311163830130, "Error creating folder <u(folder)>: <u(err)>", folder = ConvertToOSPath(path), err = create_err },
		Untranslated("<newline>"),
	}
	CreateMessageBox(GetInGameInterface(), title, err, T(1000136, "OK"))
end

local origGetPCSaveFolder = GetPCSaveFolder
function GetPCSaveFolder()
	local create_err
	local orig_print = print
	print = function(msg, err, ...)
		if msg == "Failed to create save path" then
			create_err = err
		end
		return orig_print(msg, err, ...)
	end
	local ok, path = procall(origGetPCSaveFolder)
	print = orig_print
	if not ok then
		return "AppData/ExternalSaves/"
	end
	HandleFolderCreationErr(path, create_err, T(278399852865, "Savegame"))
	return path
end

----

if not Platform.debug then return end

if config.EnableHaerald then
	function OnMsg.ModsReloaded()
		StartDebugger()
	end
end

----

-- Story bit log
GameVar("g_StoryBitsLog", {})
GameVar("g_StoryBitsScopeStack", false)

g_StoryBitsLogOld = false
function OnMsg.ChangeMap()
	g_StoryBitsLogOld = g_StoryBitsLog
end

local function UpdateStoryBitLog()
	local log_limit = options.StoryBitLogSize or 20
	if #g_StoryBitsLog > log_limit then table.remove(g_StoryBitsLog, 1) end
	SuspendErrorOnMultiCall("StoryBitLog")
	ObjModified(g_StoryBitsLog)
	ResumeErrorOnMultiCall("StoryBitLog")
end

function StoryBitLogScope(...)
	local stack = g_StoryBitsScopeStack or { g_StoryBitsLog }
	if #stack == 0 then stack = { g_StoryBitsLog } end
	g_StoryBitsScopeStack = stack
	
	local scope = stack[#stack]
	scope[#scope + 1] = { name = print_format(...) }
	stack[#stack + 1] = scope[#scope]
	UpdateStoryBitLog()
end

function StoryBitLogScopeEnd()
	table.remove(g_StoryBitsScopeStack)
end

function StoryBitLog(...)
	local stack = g_StoryBitsScopeStack or { g_StoryBitsLog }
	if #stack == 0 then stack = { g_StoryBitsLog } end
	g_StoryBitsScopeStack = stack
	
	local scope = stack[#stack]
	scope[#scope + 1] = print_format(...)
	UpdateStoryBitLog()
end

function StoryBitLogDescribe(obj)
	return TTranslate(obj:GetEditorView(), obj, false)
end

AreCheatsEnabled = return_true
DbgSetVectorZTest(false)