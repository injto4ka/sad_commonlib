local origLoadMetadataCallback = LoadMetadataCallback

function LoadMetadataCallback(...)
	Msg("GameMetadataLoad")
	local dev = Platform.developer
	Platform.developer = true -- allows to load the save without the DLC
	local err = origLoadMetadataCallback(...)
	Platform.developer = dev
	return err
end

local preorder_remap = {
	Room_Planks = "Room_WoodLogs",
	RoomNew_Planks = "Room_WoodLogs",
	Wall_Planks = "Wall_WoodLogs",
	Roof_Planks = "Roof_Wood",
	RoofPiece_Planks = "RoofPiece_Wood",
	Arch_Planks = "Arch_Wood",
	Ceiling_Planks = "Ceiling_Wood",
	Door_Planks = "Door_Wood",
	Floor_Planks = "Floor_Logs",
	Stairs_Planks = "Stairs_Logs",
}

for class, placeholder in sorted_pairs(preorder_remap) do
	local classdef = TryDefineClass(class, {
		__parents = { "CompatibilityBuilding" },
		__generated_by_class = "BuildingCompositeDef",
		object_class = "CompatibilityBuilding",
		LockState = "hidden",
		ConvertToBuilding = placeholder,
	})
	if classdef then
		ModSmartLogF(CurrentModDef, "Preorder placeholder '%s' = '%s'.", class, placeholder)
	end
end

function OnMsg.ModsReloaded()
	ClassDescendantsList("CompatibilityBuilding", function(class, def)
		if not BuildingDefs[class] then
			PlaceObj('BuildingCompositeDef', {
				ConvertToBuilding = def.ConvertToBuilding,
				LockState = "hidden",
				group = "Obsolete",
				id = class,
				object_class = "CompatibilityBuilding",
			})
		end
	end)
end

function SlabCompositeObject:OnBuildingConverted()
	self:DoneElements()
	self:RecreateElementDelayed()
	self:ComputeVisibilityAround()
end

function StairsBuilding:OnBuildingConverted()
	self:OnUpgrade(self)
end

function OnMsg.PostLoadGame()
	if not IsValid(GameStartCapsule) or GameStartCapsule.class ~= "LandingCapsule" or not Game or Game.scenario ~= "CrashLanding" or IsDlcAvailable("Preorder") then return end
	GameStartCapsule:ChangeEntity("LandingCapsule")
end