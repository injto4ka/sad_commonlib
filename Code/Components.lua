local IsKindOf = IsKindOf
local table_get = table.get
local table_find = table.find

function AddBuildingComponent(target_class, component_class, source)
	AppendClass[target_class] = {
		__parents = { component_class },
		[component_class] = true,
	}
	OnMsg.ModsReloaded = function()
		local target_def = g_Classes[target_class]
		local component_def = g_Classes[component_class]
		local composite_def = BuildingDefs[target_class]
		local valid_component = composite_def and IsKindOf(target_def, "Building") and IsKindOf(component_def, "BuildingComponent")
		assert(valid_component)
		if valid_component then
			composite_def[component_class] = true
		end
		ModSmartLogF(source, "%s is %s", target_class, component_class)
	end
end

----

DefineClass.UnitExplodingCorpse =
{
	__parents = { "UnitComponent" },
	properties = {
		{ category = "Corpse",	id = "ExplodingCorpseWeapon", name = "Exploding Corpse weapon", editor = "preset_id", preset_class = "WeaponResource", default = "", template = true, help = "Weapon to use to damage the units around on death" },
	},
	exploded_on_death = false,
}

function UnitExplodingCorpse:GetError()
	local weapon_def = Resources[self.ExplodingCorpseWeapon]
	if not weapon_def then
		return "No valid exploding corpse weapon specified!"
	end
end

function UnitExplodingCorpse:OnDie(reason)
	if self.exploded_on_death then
		return
	end
	local weapon_def = Resources[self.ExplodingCorpseWeapon]
	if weapon_def and not self.exploded_on_death then
		self.exploded_on_death = true
		DoDamageAOE(weapon_def, self, self)
	end
end

----

DefineClass.UnitReanimate =
{
	__parents = { "UnitComponent" },
	properties = {
		{ category = "Corpse",	id = "ReanimateChance",    name = "Reanimate Chance",      editor = "number", default = 30, scale = "%", min = 0, max = 100, slider = true, template = true, help = "The chance for the unit to reanimate after death" },
		{ category = "Corpse",	id = "ReanimateHealthMin", name = "Reanimate Health Min",  editor = "number", default = 20, scale = "%", min = 0, max = 100, slider = true, template = true, help = "Minimum health % after reanimate" },
		{ category = "Corpse",	id = "ReanimateHealthMax", name = "Reanimate Health Max",  editor = "number", default = 60, scale = "%", min = 0, max = 100, slider = true, template = true, help = "Maximum health % after reanimate" },
		{ category = "Corpse",	id = "ReanimateDelayMin",  name = "Reanimate Min Delay",   editor = "number", default = 5*1000,  scale = "sec", template = true, min = 0, help = "Minimum delay to reanimate after death" },
		{ category = "Corpse",	id = "ReanimateDelayMax",  name = "Reanimate Max Delay",   editor = "number", default = 10*1000, scale = "sec", template = true, min = 0, help = "Maximum delay to reanimate after death" },
		{ category = "Corpse",	id = "ReanimateAnim",      name = "Reanimate Anim",        editor = "text",   default = false, template = true, help = "Default reanimate animation" },
		{ category = "Corpse",	id = "ReanimateRotate",    name = "Reanimate Rotate",      editor = "number", default = 0, scale = "deg", min = -180*60, max = 180*60, slider = true, template = true, help = "Rotate the body before playing animation" },
		{ category = "Corpse",	id = "CanReanimate",       name = "Reanimate Condition",   editor = "expression", params = "self", template = true },
	},
	OnReconstruct = empty_func,
	OnReanimateStatus = empty_func,
	reanimating = false,
}

AutoResolveMethods.OnReanimateStatus = true

function UnitReanimate:Done()
	DeleteThread(self.reanimating)
end

function UnitReanimate:GetError()
	if self.ReanimateDelayMin > self.ReanimateDelayMax then
		return "Invalid Reanimate Delay range"
	end
	if self.ReanimateHealthMin > self.ReanimateHealthMax then
		return "Invalid Reanimate Health range"
	end
end

function UnitReanimate:CanReanimate()
	return self:IsDead() and not self.decomposed_parts and not self.exploded_on_death
end

function UnitReanimate:WaitReanimate(delay)
	Sleep(delay)
	if not IsValidPos(self) or not self:CanReanimate() then return end
	self:SetCommandImportance(100, "Reanimate")
end

function UnitReanimate:ReanimateDstr()
	if self.WaitFalling then
		self:WaitFalling()
	end
	local anim = self.recover_anim or self.ReanimateAnim and self:HasState(self.ReanimateAnim) and self.ReanimateAnim
	if anim then
		if self.ReanimateRotate then
			self.recover_anim = nil
			local duration = self:GetAnimDuration(anim)
			self:SetAngle(self:GetAngle() + self.ReanimateRotate)
			self:RestoreAxisAngle(duration, "keep_angle")
			self:PlayMomentTrackedAnim(anim, nil, nil, 0)
		else
			self.recover_anim = anim
		end
	end
	self:PlayRecoverAnim()
end

function UnitReanimate:Reanimate()
	if not IsValidPos(self) then return end
	self:PushDestructor("ReanimateDstr")
 	self:RemoveOutsidePathfinderReason("Dead", "ignore error")
	ObjModifiedDelayed(self)
	self.destroyed = nil -- handle edge case
	local pct_min, pct_max = self.ReanimateHealthMin, self.ReanimateHealthMax
	local health_pct = pct_min < pct_max and self:RandRange(pct_min, pct_max, "Reanimate") or pct_min
	local health = Max(self.Health, MulDivRound(self.MaxHealth, health_pct, 100))
	self:SetHealth(health)
	self:OnResurrected()
	self:PopAndCallDestructor()
end

function UnitReanimate:OnDie(reason)
	local chance = self.ReanimateChance
	local success = not self.reanimating and (chance >= 100 or chance > 0 and chance > self:Random(100, "Reanimate"))
	if success then
		local delay_min, delay_max = self.ReanimateDelayMin, self.ReanimateDelayMax
		local delay = delay_min < delay_max and self:RandRange(delay_min, delay_max, "Reanimate") or delay_min
		self.reanimating = CreateGameTimeThread(self.WaitReanimate, self, delay)
	end
	return self:OnReanimateStatus(success)
end

function IntegrityProps:OnResurrected()
	return self:OnReconstruct()
end

----

local origInteract = NetSyncEvents.Interact
function NetSyncEvents.Interact(objs, target, ...)
	for _, obj in ipairs(objs) do
		local OnInteract = obj.OnInteractOrder
		if OnInteract then
			obj.interaction_order = (obj.interaction_order or 0) + 1
			procall(OnInteract, obj, target)
		end
	end
	return origInteract(objs, target, ...)
end

AutoResolveMethods.OnManualControlSet = true
AutoResolveMethods.OnInteractOrder = true
AutoResolveMethods.NeedsRescue = "or"
Unit.OnInteractOrder = empty_func
Unit.NeedsRescue = empty_func
Unit.interaction_order = 0

HumanAutoResolve.ManuallyInteract = Human.ManuallyInteract
Human.ManuallyInteract = nil

----

----

AppendClass.ComponentAnim = {
	properties = {
		{ category = "Animation", id = "InheritedEntitiesList", name = "Inherited Entities", editor = "string_list", default = false, read_only = true, dont_save = true, arbitrary_value = true},
	},
}

local MaxAnimSpeed = const.MaxAnimSpeed
local MaxAnimWeight = const.MaxAnimWeight
local AnimWeightScale = const.AnimWeightScale
local AnimSpeedScale = const.AnimSpeedScale

for i=1,const.MaxAnimChannels do
	local props = {
		{ id = "AnimText",         name = "Anim",              editor = "choice", default = false, items = function(self) return self:GetStates() end },
		{ id = "AnimSpeed",        name = "Anim Speed",        editor = "number", default = AnimSpeedScale, scale = AnimSpeedScale, min = 0, max = MaxAnimSpeed, exponent = 2, slider = true },
		{ id = "AnimWeight",       name = "Anim Weight",       editor = "number", default = function() return i == 1 and AnimWeightScale or 0 end, scale = AnimWeightScale, min = 0, max = MaxAnimWeight, exponent = 2, slider = true },
		{ id = "AnimMaskName",     name = "Anim Mask",         editor = "choice", default = false, items = function(self) return self:GetAnimMasks() end },
		{ id = "AnimMaskInverse",  name = "Anim Mask Inverse", editor = "bool",   default = false },
		{ id = "AnimDurationChnl", name = "Anim Duration",     editor = "number", default = 0, read_only = true },
		{ id = "AnimStepChnl",     name = "Anim Step",         editor = "number", default = 0, read_only = true },
		{ id = "AnimLooping",      name = "Anim Looping",      editor = "bool",   default = false, read_only = true, },
		{ id = "AnimStatic",       name = "Anim Static",       editor = "bool",   default = false, read_only = true, },
	}
	for _, prop in ipairs(props) do
		local setter, getter = "Set" .. prop.id, "Get" .. prop.id
		prop.id = prop.id .. i
		prop.category = "Animation"
		prop.name = "[" .. i .. "] " .. prop.name
		prop.dont_save = true
		table.insert(ComponentAnim.properties, prop)
		if not prop.read_only then
			ComponentAnim[setter .. i] = function(self, value)
				self[setter](self, i, value)
			end
		end
		ComponentAnim[getter .. i] = function(self)
			return self[getter](self, i)
		end
	end
end

local InvalidStateName = const.InvalidStateName
function ComponentAnim:GetAnimText(i)
	local anim = self:GetAnim(i) or -1
	local name = anim ~= -1 and GetStateName(anim) or InvalidStateName
	return name ~= InvalidStateName and name or false
end
function ComponentAnim:SetAnimText(i, anim)
	anim = anim or ""
	if anim ~= "" then
		self:SetAnim(i, anim)
	elseif i == 1 then
		self:SetState(0)
	else
		self:ClearAnim(i)
	end
end
function ComponentAnim:GetAnimLooping(i)
	return self:IsAnimLooping(i)
end
function ComponentAnim:GetAnimStatic(i)
	return self:IsStaticAnim(self:GetAnim(i))
end
function ComponentAnim:GetInheritedEntitiesList()
	return table.keys(GetInheritedEntities(self:GetEntity()), true)
end
function ComponentAnim:GetAnimDurationChnl(i)
	return self:GetAnimDuration(self:GetAnim(i))
end
function ComponentAnim:GetAnimStepChnl(i)
	return self:GetStepLength(self:GetAnim(i))
end

function ComponentAnim:SetAnimMask(channel, maskName, inverse)
	if type(channel) ~= "number" then
		return self:SetAnimMask(1, channel, maskName, inverse)
	end
	self.anim_masks = table.set(self.anim_masks, channel, maskName)
	return CObject.SetAnimMask(self, channel, maskName, inverse)
end

function ComponentAnim:SetAnimMaskName(channel, name)
	self:SetAnimMask(channel, name, self:GetAnimMaskInverse(channel))
end

function ComponentAnim:SetAnimMaskInverse(channel, inverse)
	local mask = self:GetAnimMaskName(channel)
	if mask then
		self:SetAnimMask(channel, self:GetAnimMaskName(channel), inverse)
	end
end

function ComponentAnim:GetAnimMaskName(channel)
	return table.get(self.anim_masks, channel)
end

function ComponentAnim:GetAnimMaskInverse(channel)
	return (self:GetAnimFlags(channel) & const.eInverseAnimMask) ~= 0
end

AppendClass.Movable = {
	__parents = { "ComponentAnim" }
}

----



DefineClass.UnitHeat =
{
	__parents = { "UnitComponent" },
	properties = {
		{ category = "Heat", id = "PowerTemperature",     name = "Power temperature",   editor = "number", default = 0, scale = "C", help = "The temperature to maintain. Requires power ouput to be set.", },
		{ category = "Heat", id = "PowerOutput",          name = "Power output (W)",    editor = "number", default = 0, min = 0, help = "More energy equals higher power temperature radiation per tick", },
		{ category = "Heat", id = "RadiationTemperature", name = "Instant temperature", editor = "number", default = 0, scale = "C", help = "The temperature to instantly set around. Requires instant radius to be set.", },
		{ category = "Heat", id = "RadiationRange",       name = "Instant radius",      editor = "number", default = 0, scale = "m", min = 0, help = "The radius of the instant temperature effect", },
		{ category = "Heat", id = "HeatAttachSpot",       name = "Attach Spot",         editor = "combo",  default = "Origin", items = GetEntitySpotsItems, help = "Where to attach the heat source", },
		{ category = "Heat", id = "HeatAutoEnable",       name = "Auto Enable",         editor = "choice", default = "attacking", items = { "disabled", "always", "attacking" }, help = "When to activate the heat", },
	},
	heat_source = false,
}

function UnitHeat:GameInit()
	if self.HeatAutoEnable == "always" then
		self:EnableHeat(true)
	end
end
function UnitHeat:Done()
	self:EnableHeat(false)
end
function UnitHeat:OnDie()
	self:EnableHeat(false)
end
function UnitHeat:OnAttackTargetChanged(target)
	if self.HeatAutoEnable == "attacking" then
		self:EnableHeat(target)
	end
end

function UnitHeat:EnableHeat(enable)
	local heat_source = self.heat_source
	local enabled = IsValid(heat_source)
	if enable and not enabled then
		heat_source = HeatSource:new()
		heat_source:SetPowerOutput(self.PowerOutput)
		heat_source:SetPowerTemperature(self.PowerTemperature)
		heat_source:SetRadiationTemperature(self.RadiationTemperature)
		heat_source:SetRadiationRange(self.RadiationRange)
		heat_source:MakeSync()
		self:Attach(heat_source, self:GetSpotBeginIndex(self.HeatAttachSpot))
		self.heat_source = heat_source
	elseif not enable and enabled then
		DoneObject(heat_source)
		self.heat_source = nil
	end
end

function UnitHeat:GetError()
	if self.PowerOutput == 0 and self.RadiationRange == 0 then
		return "Both power output and radiation range are set to 0"
	end
end

----

AddBuildingComponent("CookStove_ScrapMetal", "HeatSourceComponent", CurrentModDef)
AddBuildingComponent("CookStove_Electric", "HeatSourceComponent", CurrentModDef)

----

OwnedComponent.GetError = nil

function OwnedObject:Init()
	if not self.ownership_class then
		self.ownership_class = self
	end
end

function SavegameFixups.FixOwnershipClass()
	MapForEach("map", "OwnedComponent", function(obj)
		obj.ownership_class = obj.ownership_class or obj
	end)
end

SetPropMeta("OwnedObject", "ownership_class", "help", "If set then there could be only one owned object from that class", CurrentModDef)

function OnMsg.ClassesGenerate(classdefs)
	local find = table.find
	local make_owned = {"RelaxationDeviceComponent", "ProductionDeviceComponent", "StorageDepotComponent"}
	local blacklist = { "StorageWithUiContainer" }
	for classname, classdef in pairs(classdefs) do
		if not classdef.OwnedComponent then
			local exclude
			for _, parent in ipairs(blacklist) do
				if find(classdef.__parents, parent) then
					exclude = true
					break
				end
			end
			if not exclude then
				for _, comp in ipairs(make_owned) do
					if classdef[comp] then
						AddBuildingComponent(classname, "OwnedComponent", CurrentModDef)
						classdef.ChangeOwnerIcon = "UI/Icons/Infopanels/assign_owner"
						break
					end
				end
			end
		end
	end
end


local origCanUnitPerformProduction = ProductionActivityObject.CanUnitPerformActivity
function ProductionActivityObject:CanUnitPerformActivity(unit, skill_level, direct_order, ...)
	local target = self.building
	if not direct_order and target and target.OwnedComponent and not target:CanBeOwnedBy(unit) then
		return
	end
	return origCanUnitPerformProduction(self, unit, skill_level, direct_order, ...)
end

local origCanUnitPerformResearch = ResearchActivityObject.CanUnitPerformActivity
function ResearchActivityObject:CanUnitPerformActivity(unit, skill_level, direct_order, ...)
	local target = self.building
	if not direct_order and target and target.OwnedComponent and not target:CanBeOwnedBy(unit) then
		return
	end
	return origCanUnitPerformResearch(self, unit, skill_level, direct_order, ...)
end

local origIsValidRelaxation = return_true
local prop = table.find_value(RelaxationRoutine.properties, "id", "IsValidTarget")
if prop then
	origIsValidRelaxation = prop.default
	prop.default = nil
end
function RelaxationRoutine:IsValidTarget(target, unit, direct_order)
	if not direct_order and target and target.OwnedComponent and not target:CanBeOwnedBy(unit) then
		return
	end
	return origIsValidRelaxation(self, target, unit, direct_order)
end

local function CheckStorageOwnership(building, unit)
	if not building or not unit or not building.OwnedComponent or building:CanBeOwnedBy(unit) then
		return true
	end
	local label = table_get(building.player, "labels", building.OwnerLabel)
	return not table_find(label, unit)
end

local origCheckSupplyVisualBuilding = CheckSupplyVisualBuilding
function CheckSupplyVisualBuilding(req_bld, res, unit, demand_bld, ...)
	local result, visual_building = origCheckSupplyVisualBuilding(req_bld, res, unit, demand_bld, ...)
	result = result and CheckStorageOwnership(visual_building, unit)
	return result, visual_building
end

local origCheckDemandVisualBuilding = CheckDemandVisualBuilding
function CheckDemandVisualBuilding(req_bld, unit)
	local result, visual_building, treat_storage_as_device = origCheckDemandVisualBuilding(req_bld, unit)
	result = result and CheckStorageOwnership(visual_building, unit)
	return result, visual_building, treat_storage_as_device
end