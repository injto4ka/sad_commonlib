local function EntityDataClasses(self)
	local items = {}
	local get = table.get
	for name, data in pairs(EntityData) do
		local class_parent = get(data, "entity", "class_parent")
		if class_parent then
			items[class_parent] = true
		end
	end
	return table.keys(items, true)
end

AppendClass.ModItemEntity = {
	properties = {
		{ category = "Results", id = "import_error", name = "Import errors", editor = "text",        default = '', read_only = true, dont_save = true,no_edit = true },
		{ category = "Results", id = "material",     name = "Materials",     editor = "string_list", default = false, read_only = true, arbitrary_value = true },
		{ category = "Results", id = "mesh",         name = "Meshes",        editor = "string_list", default = false, read_only = true, arbitrary_value = true },
		{ category = "Results", id = "texture",      name = "Textures",      editor = "string_list", default = false, read_only = true, arbitrary_value = true },
		{ category = "Results", id = "animation",    name = "Animations",    editor = "string_list", default = false, read_only = true, arbitrary_value = true },
		{ category = "Misc",    id = "class_parent", name = "Class",         editor = "combo",       default = "", items = EntityDataClasses, help = "Classes which this entity class should inherit (comma separated).", entitydata = true, }, 
	},
	GetSpecSubitems = EntitySpec.GetSpecSubitems,
	SortSubItems = EntitySpec.SortSubItems,
	HasRequiredDlc = false,
}

local prop = table.find_value(ModItemEntity.properties, "id", "buttons")
if prop then
	table.insert(prop.buttons, {name = "Inspect Loaded Entity", func = "Inspect"})
	table.insert(prop.buttons, {name = "Restart Game", func = "RestartGameWithMod"})
end

function BaseModItemEntity:RestartGameWithMod()
	if not self.mod then return end
	local idx = table.find(self.mod.items, self)
	LocalStorage.AutoStart = {
		Mod = self.mod.id,
		Select = idx,
		Map = GetMapName(),
	}
	SaveLocalStorage()
	quit("restart")
end

if FirstLoad then
	__testing_entity_pos = false
end

function ModItemEntity:Inspect()
	if not IsValidEntity(self.entity_name) then
		return
	end
	if GetMap() ~= "" then
		CreateRealTimeThread(function()
			local viewer = FindGedApp("GedEntityViewer")
			if viewer then
				CloseGedApp(viewer)
			end
			DelayedLoadEntity(self.mod, self.entity_name)
			WaitDelayedLoadEntities()
			Msg("BinAssetsLoaded") -- force entity-related structures reload - not needed for this visualization, but necessary to use this e.g. in a building a bit later
			local obj
			if __testing_entity_pos then
				obj = MapGetFirst(__testing_entity_pos, 0, "Shapeshifter", function(obj) return obj.testing_entity end)
			end
			if not obj then
				__testing_entity_pos = GetTerrainCursorXY(UIL.GetScreenSize()/2)
				obj = PlaceObject("Shapeshifter")
				obj.testing_entity = true
				obj:SetPos(__testing_entity_pos)
			end
			obj:ChangeEntity(self.entity_name)
			CreateEntityViewer(obj)
		end)
	else
		Inspect(GetEntityAsLuaTable(self.entity_name))
	end
end

function ModItemEntity:GetError()
    return self.import_error ~= "" and self.import_error
end

function ModItemEntity:RegisterSpec()
	if self.entity_name ~= "" then
		EntitySpecPresets[self.entity_name] = self
	end
end
function ModItemEntity:UnregisterSpec()
	if self.entity_name ~= "" and EntitySpecPresets[self.entity_name] == self then
		EntitySpecPresets[self.entity_name] = nil
	end
end

local origOnModLoad = ModItemEntity.OnModLoad or empty_func
function ModItemEntity:OnModLoad()
	self:RegisterSpec()
	return origOnModLoad(self)
end

local origOnModUnload = ModItemEntity.OnModUnload or empty_func
function ModItemEntity:OnModUnload()
	self:UnregisterSpec()
	return origOnModUnload(self)
end

local origParseEntity = ParseEntity
function ParseEntity(root, name)
	local err, entity = origParseEntity(root, name)
	if not err and entity then
		for i, value in ipairs(entity.meshes) do
			if string.starts_with(value, "Meshes/") then
				entity.meshes[i] = string.sub(value, 8)
			end
		end
		for i, value in ipairs(entity.materials) do
			if string.starts_with(value, "Materials/") then
				entity.materials[i] = string.sub(value, 11)
			end
		end
	end
	return err, entity
end

-- Fix wrong method override of ModItem.OnEditorDelete
local origOnEditorDelete = BaseModItemEntity.OnEditorDelete
function BaseModItemEntity:OnEditorDelete(...)
	self:UnregisterSpec()
	origOnEditorDelete(self, ...)
	return ModItem.OnEditorDelete(self, ...)
end

local origModLogF = ModLogF
local origImport = ModItemEntity.Import
function ModItemEntity:Import(...)
    if self.entity_name ~= "" then
		self:UnregisterSpec()
        origOnEditorDelete(self)
		self.material = nil
        self.mesh = nil
        self.texture = nil
		self.animation = nil
        self.entity_name = nil
    end
    self.import_error = nil
    ModLogF = function(log_or_fmt, fmt, arg1, ...)
		if log_or_fmt == "Importing %s %s" then
			self[fmt] = table.create_add(self[fmt], arg1)
		elseif type(log_or_fmt) ~= "string" then
            local err = self.import_error
            if err ~= "" then
                err = err .. '\n'
            end
            self.import_error = err .. string_format(fmt, arg1, ...)
        end
        return origModLogF(log_or_fmt, fmt, arg1, ...)
    end
    origImport(self, ...)
	self:RegisterSpec()
    ModLogF = origModLogF
end

function ModItemEntity:GetLastChange()
	return ""
end

function ModItemEntity:GetCreationTime() -- the time is was marked as Ready in the art spec
	return 0
end

function ModItemEntity:GetModificationTime() -- the latest modification time as per the file system
	return 0
end

----

-- Fix LOD view in the Entity viewer
if FirstLoad then
	orig_GetStateMeshFile = false
end
orig_GetStateMeshFile = orig_GetStateMeshFile or GetStateMeshFile

function GetStateMeshFile(entity, state_idx, lod)
	lod = lod or 0
	local mesh_file = orig_GetStateMeshFile(entity, state_idx)
	if lod > 0 then
		local data = GetEntityAsLuaTable(entity)
		local state = GetStateName(state_idx)
		local mesh_name = table.get(data, "states", "mesh_ref") or "mesh"
		local mesh_lods = table.get(data, "meshes", mesh_name, "lods")
		local mesh_lod_name = table.get(mesh_lods, lod + 1, "mesh")
		if mesh_lod_name then
			local last_idx = 0
			while true do
				local idx = string.find(mesh_file, '/', last_idx + 1, true)
				if not idx then break end
				last_idx = idx
			end
			local mesh_folder = string.sub(mesh_file, 1, last_idx)
			local lod_mesh_file = mesh_folder .. mesh_lod_name
			if GetMeshProperties(lod_mesh_file) then
				return lod_mesh_file
			end
		end
		local idx = string.find(mesh_file, '.', 1, true)
		local lod_mesh_file = mesh_file:sub(1, idx) .. lod .. "." .. mesh_file:sub(idx + 1)
		if GetMeshProperties(lod_mesh_file) then
			return lod_mesh_file
		end
	end
	return mesh_file
end

function GetEntitySpec(entity, expect_missing)
	return EntitySpecPresets[entity]
end

-- Fixed in game version 1.22.240321
if LuaRevision < 352127 then
	local last_reload
	local function ReloadFadeCategoriesOnce()
		if last_reload == RealTime() then return end
		last_reload = RealTime()
		ReloadFadeCategories()
	end
	OnMsg.NewMap = ReloadFadeCategoriesOnce
	OnMsg.PreLoadGame = ReloadFadeCategoriesOnce
end

----

if FirstLoad then
	LoadingGame = false
	ForceSyncEntityBlend = false
end

function OnMsg.GameMetadataLoad()
	LoadingGame = true
end
function OnMsg.LoadGame()
	LoadingGame = false
end

function ForceRecomposeAllBlendedBodies()
	local objs = MapGet("map", "BlendedCompositeBody")
	for i,obj in ipairs(objs) do
		obj:ForceRevertBlendedBodyParts()
	end
	if LoadingGame then return end
	for i,obj in ipairs(objs) do
		obj:ForceComposeBlendedBodyParts()
	end
end

function BlendedEntityIsLocked(entity_name)
	return IsValidThread(g_EntityBlendLocks[entity_name])
end

function BlendedEntityLock(entity_name)
	assert(not BlendedEntityIsLocked(entity_name), "Locking a blended entity that is already locked!")
	g_EntityBlendLocks[entity_name] = CurrentThread()
end

function BlendedEntityUnlock(entity_name)
	assert(BlendedEntityIsLocked(entity_name), "Unlocking a blended entity that isn't locked!")
	g_EntityBlendLocks[entity_name] = nil
	Msg("BlendedEntityUnlocked")
end

function WaitBlendEntityLocks(obj, entity_name)
	while BlendedEntityIsLocked(entity_name) do
		if obj and not IsValid(obj) then
			return false
		end
		WaitMsg("BlendedEntityUnlocked", 30)
	end
	return true
end

----

if Platform.debug then

local orig_EntityViewerRoot_Init = EntityViewerRoot.Init
function EntityViewerRoot:Init(...)
	orig_EntityViewerRoot_Init(self, ...)
	table.sort(self, function(a, b)
		if a.lod ~= b.lod then
			return a.lod < b.lod
		end
		return a.MeshName < b.MeshName
	end)
end

-- Fix EntityViewer non-dev fields
function OpenTextureViewerIngame(root, obj, prop)
	local ged = FindGedApp("GedImageViewer")
	if ged then
		CloseGedApp(ged)
	end
	local game_path = obj[prop] or ""
	OpenGedApp("GedImageViewer", false, { file_name = game_path }, nil, true)
end

local button_funcs_to_replace = {
	View = "OpenTextureViewerIngame",
	Alpha = "",
	Locate = "",
	InGame = "",
	Open = "",
}

for _, def in ipairs{ EV_State, EV_Material } do
	for i, prop in ripairs(def.properties) do
		if string.ends_with(prop.id, "Preview") then
			local orig_no_edit = prop.no_edit or empty_func
			prop.no_edit = function(obj, meta, ...)
				if orig_no_edit(obj, meta, ...) then
					return true
				end
				return string.starts_with(obj[meta.id], "Textures/")
			end
		else
			for i, button in ripairs(prop.buttons) do
				local new_func = button_funcs_to_replace[button.name]
				if new_func then
					if new_func == "" then
						table.remove(prop.buttons, i)
					else
						button.func = new_func
					end
				end
			end
		end
	end
end

local function FileNameFromPath(path)
	path = string.gsub( path, "\\","/")
	path = string.gsub(path, "([^/]+)/", "")
	if path == "" then
		return false
	else
		return path
	end
end

EV_Material.Create = function(self, name, index, sub_material, parent)
	self.parent = parent
	self.filename = "Materials/".. string.gsub(name, ".mtl.", ".hmtl.")
	self.index = index
	if not index or not sub_material then
		self.itemtext = FileNameFromPath(name)
	else
		self.itemtext = "SubMaterial "..tostring(index + 1)
	end
end

end -- Platform.debug



