function GetResStackInfo(obj, res, slot_num)
	local info = slot_num and obj.res_stacks_info
	if not info then return end
	local idx = table.get(obj.res_stacks_indices, res, slot_num)
	return idx and info[idx]
end
local GetResStackInfo = GetResStackInfo

function FuncPieces.VisStorageSlot_Setup(self, res, amount, max_amount, slot_num, crafting, storage_status, storage_temperature, dismantling, delivering, amount_short)
	local res_def = res and Resources[res]
	local free_slot = (res == "free")
	local obj = ResolvePropObj(self:GetContext())
	local unfinished
	local orig_res = res
	local orig_def = res_def
	if res_def and res_def.is_unfinished_item then
		local stack = GetResStackInfo(obj, res, slot_num) or (crafting and obj.unfinished_item_data)
		if stack then
			unfinished = true
			local output = GetRecipeFirstOutputResource(stack.recipe_id)
			if output then
				res = output.id
				res_def = output
			end
		end
	end
	rawset(self, "unfinished", unfinished)
	local bar_container = self.idSlotBarContainer
	rawset(self, "dismantling", nil)
	rawset(self, "crafting", nil)
	rawset(self, "slot_num", nil)
	rawset(self, "res", nil)
	rawset(self, "orig_res", nil)
	rawset(self, "has_dirtiness", nil)
	rawset(self, "decaying", nil)
	rawset(self, "has_uses", nil)
	rawset(self, "has_shelf_life_stats", nil)
	rawset(self, "ShelfLifeBarDisplayName", nil)
	rawset(self, "transforming", nil)
	rawset(self, "TransformDisplayName", nil)
	rawset(self, "consumable", nil)
	rawset(self, "consumption_type", nil)
	rawset(self, "health_condition", nil)
	rawset(self, "health_condition_chance", nil)
	rawset(self, "happiness_factor", nil)
	rawset(self, "vegetarian", nil)
	rawset(self, "serving_amount", nil)
	rawset(self, "status", nil)
	rawset(self, "storage_temperature", nil)
	rawset(self, "has_temperature", nil)
	rawset(self, "resource_type",  nil)
	rawset(self, "AttackRange",  nil)
	rawset(self, "AttackCooldown",  nil)
	rawset(self, "AttackCooldownMaxSkill", nil)
	rawset(self, "HitChance",  nil)
	rawset(self, "HitChanceMaxSkill",  nil)
	rawset(self, "CritChance",  nil)
	rawset(self, "CritChanceMaxSkill",  nil)
	rawset(self, "has_damage_negation", nil)
	self:SetRolloverTitle(nil)
	self.GetRolloverText = nil
	self:SetRolloverHint(nil)
	self:SetRolloverHintGamepad(nil)
	self.idIcon:SetVisible(false)
	local bar = bar_container:ResolveId("idCraftingBar")
	if bar then
		bar:delete()
	end
	bar = bar_container:ResolveId("idShelfLifeBar")
	if bar then
		bar:delete()
	end
	bar = bar_container:ResolveId("idResourceUsesBar")
	if bar then
		bar:delete()
	end
	bar = bar_container:ResolveId("idTransformationBar")
	if bar then
		bar:delete()
	end
	bar = bar_container:ResolveId("idDeliveryBar")
	if bar then
		bar:delete()
	end
	self.idLock:SetVisible(dismantling)
	if res_def then
		if res_def:IsHumanEdible() then
			self.idLock:SetImage("UI/Hud/food_throw_away")
		end
		if dismantling or obj:CanDismantleRes(orig_res, slot_num, not not dismantling) then
			local hint, hint_gamepad
			if res_def:IsHumanEdible() then
				hint = T(787185485927, "<right_click> Throw away")
				hint_gamepad = T(532297634079, "<ButtonX> Throw away")
			else
---------------------------------------------
				local stack = GetResStackInfo(obj, res, slot_num)
---------------------------------------------
				if not stack then
					local left, total = obj:GetTotalResourcePropAmountLeft(res, slot_num, "times_used", res_def.Uses, not not dismantling)
					local decay, max_decay, ok = obj:GetAverageResourcePropAmount(res, slot_num, "decay", nil, dismantling)
					stack = {times_used = {total - left}, decay = decay}
				end
				local text = GetDismantleResourcesText(res, amount, stack)
				hint = T{871471161647, "<right_click> Dismantle for <em><text></em>", text = text}
				hint_gamepad = T{260244324864, "<ButtonX> Dismantle for <em><text></em>", text = text}
			end
			self:SetRolloverHint(hint)
			self:SetRolloverHintGamepad(hint_gamepad)
		end
		if dismantling then
			rawset(self, "dismantling", true)
		end
		if crafting then
			rawset(self, "crafting", true)
			local bar = XTemplateSpawn("VisStorageSlotCraftingBar", bar_container, self)
			bar:SetId("idCraftingBar")
		end
		if delivering then
			rawset(self, "delivering", true)
			rawset(self, "available_amount", amount)
			local bar = XTemplateSpawn("VisStorageSlotDeliveryBar", bar_container, self)
			bar:SetId("idDeliveryBar")
			
			amount = max_amount
			max_amount = nil
		end
		rawset(self, "slot_num", slot_num)
		rawset(self, "res", res)
		rawset(self, "orig_res", orig_res)
		self.idIcon:SetVisible(true)
		if unfinished and not crafting then
			self.idIcon:SetImage(Resources[orig_res].alt_icon)
		else
			self.idIcon:SetImage(res_def.alt_icon)
		end
		if unfinished then
			if res_def.is_unfinished_item then
				self:SetRolloverTitle(T{809519432199, "<name> <em>(<percent(progress)>)</em>", name = res_def.display_name, progress = self:GetResourceCraftingPercent(slot_num)})
			else
				self:SetRolloverTitle(T{911755362119, "Unfinished <name> <em>(<percent(progress)>)</em>", name = res_def.display_name, progress = self:GetResourceCraftingPercent(slot_num)})
			end
		else
			self:SetRolloverTitle(res_def.display_name)
		end
		self.GetRolloverText = function(self)
			return res_def.description
		end
		if obj:IsStorageBuilding() then
			local has_uses = (res_def.Uses or 0) > 0
			rawset(self, "has_uses", has_uses)
			if has_uses then
				local bar = XTemplateSpawn("VisStorageSlotShelfLifeBar", self.idSlotBarContainer, self)
				bar:SetId("idResourceUsesBar")
				bar:SetBindTo("ResourceUsesPercent")
			end
			rawset(self, "has_dirtiness", ResHasDirtiness(res))
			if obj:CanResourceDecay(res) then
				local decaying, has_shelf_life_stats, bar_display_name, bar_color = GetResourceDecayParams(res)
				if not decaying then
					decaying = obj:GetAverageResourceHealth(res, slot_num, dismantling) < 100
				end
				rawset(self, "decaying", decaying)
				rawset(self, "has_shelf_life_stats", has_shelf_life_stats)
				rawset(self, "ShelfLifeBarDisplayName", bar_display_name)
				if decaying then
					rawset(self, "status", storage_status)
					rawset(self, "storage_temperature", storage_temperature)
					local bar = XTemplateSpawn("VisStorageSlotShelfLifeBar", self.idSlotBarContainer, self:GetShelfLifeBarContext())
					bar:SetId("idShelfLifeBar")
					if bar_color then
						bar:SetBarColor(bar_color)
					end
				end
			end
			if obj.ResourceTransformComponent and table.find(obj.TransformDefinitions, "InputResource", res) then
				rawset(self, "transforming", true)
				rawset(self, "TransformDisplayName", obj.TransformDisplayName)
				local bar = XTemplateSpawn("VisStorageSlotTransformationBar", self.idSlotBarContainer, self)
				bar:SetId("idTransformationBar")
			end
		end
		rawset(self, "consumable", res_def:IsHumanEdible())
		if rawget(self, "consumable") then
			rawset(self, "consumption_type", res_def.consumption_type)
			rawset(self, "health_condition", res_def.health_condition)
			if rawget(self, "health_condition") then
				rawset(self, "health_condition_chance", res_def.health_condition_chance)
			end
			rawset(self, "happiness_factor", res_def.happiness_factor)
			rawset(self, "vegetarian", res_def.vegetarian)
			rawset(self, "serving_amount", res_def.serving_amount)
		end
		rawset(self, "has_temperature", not not IsKindOf(res_def, "EquipResource"))
		if rawget(self, "has_temperature") then
			local high = table.find_value(res_def.Modifiers or empty_table, "prop", "TemperatureHigh")
			local low = table.find_value(res_def.Modifiers or empty_table, "prop", "TemperatureLow")
			rawset(self, "temperature_low", low and low.add or 0)
			rawset(self, "temperature_high", high and high.add or 0)
		end
		rawset(self, "resource_type", res_def.resource_type)
		if rawget(self, "resource_type") == "Weapon" then
			rawset(self, "AttackRange", res_def.AttackRange)
			rawset(self, "AttackCooldown", res_def.AttackCooldown)
			rawset(self, "AttackCooldownMaxSkill", res_def.AttackCooldownMaxSkill)
			rawset(self, "HitChance", res_def.HitChance)
			rawset(self, "HitChanceMaxSkill",  res_def.HitChanceMaxSkill)
			rawset(self, "CritChance", res_def.CritChance)
			rawset(self, "CritChanceMaxSkill",  res_def.CritChanceMaxSkill)
		end
		rawset(self, "has_damage_negation", not not IsKindOf(res_def, "EquipResource"))
		if rawget(self, "has_damage_negation") then
			for i,damage_type in ipairs(const.WeaponDamageTypes) do
				local negation = res_def.HitNegation and table.find_value(res_def.HitNegation, "DamageType", damage_type)
				local negation_chance = negation and negation.NegationChance or 0
				rawset(self, damage_type.."_negation_chance", negation_chance)
			end
		end
	end
	rawset(self, "res_props_set", true)
	if amount == 0 and not max_amount then
		self:SetVisible(false)
		return
	end
	
	--amount
	local single_amount = const.ResourceScale
	local has_amount = (amount or 0) > 0 or (max_amount or 0) > 0
	local one_per_stack = orig_def and orig_def.stack_size == single_amount and amount == single_amount
	if free_slot or (has_amount and (not one_per_stack or max_amount)) then
		self.idCount:SetVisible(true)
		if res then
			local text
			if amount_short then
				text = T{186229196146, "<res_amount_short(res, amount, max_amount)>", res = res, amount = amount, max_amount = max_amount}
			else
				text = T{921572201137, "<res_amount(res, amount, max_amount)>", res = res, amount = amount, max_amount = max_amount}
			end
			self.idCount:SetText(text)
		else
			self.idCount:SetText(T{784239521473, "<amount>", amount = amount})
		end
	else
		self.idCount:SetVisible(false)
	end
	
	--"free" slot
	self.idFree:SetVisible(free_slot)
end

------------------

local match = {
	class = "XTemplateFunc",
	name = "Setup(self, res, amount, max_amount, slot_num, crafting, storage_status, storage_temperature, dismantling, delivering, amount_short)",
}
UIChangeControl("VisStorageSlot", match, "func", FuncPieces.VisStorageSlot_Setup)


function FuncPieces.VisStorageSlot_GetResourceCraftingPercent(self, slot_num, ...)
	local crafting = rawget(self, "crafting")
	slot_num = slot_num or rawget(self, "slot_num")
	local orig_res = rawget(self, "orig_res")
	local obj = ResolvePropObj(self:GetContext())
	if crafting then
		return obj:GetCraftingProgressPercent(slot_num, orig_res)
	else
		-- unfinished item in storage
---------------------------------------------
		local stack = GetResStackInfo(obj, orig_res, slot_num)
---------------------------------------------
		return stack and stack.crafting_progress or 0
	end
	return 0
end
		
local match = {
	class = "XTemplateFunc",
	name = "GetResourceCraftingPercent(self, slot_num, ...)",
}
UIChangeControl("VisStorageSlot", match, "func", FuncPieces.VisStorageSlot_GetResourceCraftingPercent)