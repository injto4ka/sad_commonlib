local options = CurrentModOptions or empty_table

UnitAnimal.AutoWorkActionOnKill = "Butcher"

AppendClass.UnitInvader = {
	properties = {
		{ category = "Invader", id = "AutoWorkActionOnKill", name = "Work Action On Kill", editor = "preset_id", default = false, preset_class = "WorkAction", remplate = true },
	},
}

function UnitInvader:TryIssueAutoWorkActionOnKill(reason)
	if not options.AutoButcherCorpses then
		return
	end
	local attacker = self:GetValidAttacker()
	if not attacker or attacker.CombatGroup ~= Human.CombatGroup or not attacker.player then
		return
	end
	local action = self.AutoWorkActionOnKill
	if not action or GetWorkActionInstance(self, action) then
		return
	end
	local preset = WorkActions[action]
	if not preset or not preset:IsValidActionTarget(self, attacker.player) then
		return
	end
	return preset:OnAction(self, attacker.player, nil, nil, attacker)
end

local origOnDead = UnitInvaderAutoResolve.OnDead or empty_func
function UnitInvaderAutoResolve:OnDead(reason)
	origOnDead(self, reason)
	self:TryIssueAutoWorkActionOnKill(reason)
end

----

ActivityInvaderRobotBase.AutoWorkActionOnKill = "DisassembleRobot"