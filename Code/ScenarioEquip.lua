AppendClass.ScenarioDef = {
	properties = {
		{
			category = "Equipment", id = "equipment_apply",  name = "Apply",
			editor = "choice", default = false, items = { false, "game start", "always" },
		},
		{
			category = "Equipment", id = "equipment_check",  name = "Filter",
			editor = "expression", params = "self, character", default = return_true,
			no_edit = PropChecker("equipment_apply", false)
		},
		{
			category = "Equipment", id = "force_equipment",  name = "Equipment",
			editor = "preset_id_list", default = {}, preset_class = "EquipResource", item_default = "",
			preset_filter = function(preset)
				return preset.class == "EquipResource"
			end,
			no_edit = PropChecker("equipment_apply", false),
		},
	}
}

local function Equip(self, equipment_id)
	local preset = Resources[equipment_id]
	if not preset or preset.EquipSlot == "" then
		print("Invalid equipment", equipment_id)
	elseif not self:IsEquipSlotForbidden(preset.EquipSlot) then
		UnitEquipment.Equip(self, equipment_id)
	end
end

local function ApplyScenarioEquipement(self)
	local apply_equipment = Scenario and Scenario.equipment_apply
	if not apply_equipment or apply_equipment == "game start" and GameStartTime then
		return
	end
	if Scenario.equipment_check then
		local success, ok = procall(Scenario.equipment_check, Scenario, self)
		if not success or not ok then
			return
		end
	end
	for _, equipment_id in ipairs(Scenario.force_equipment) do
		Equip(self, equipment_id)
	end
	return true
end

-- Override the entire InitEquipement as it was impossible to make available the custom starting equipment otherwise
function Human:InitEquipement(def)
	for _,slot in ipairs(EquipSlots) do
		self:Unequip(slot)
	end

	local init_equipment = self.init_equipment
	self.init_equipment = nil

	if ApplyScenarioEquipement(self) then
		return
	end
	for _, id in ipairs(init_equipment) do
		Equip(self, id)
	end
	for _, id in ipairs(def and def.Equipment) do
		Equip(self, id)
	end
end