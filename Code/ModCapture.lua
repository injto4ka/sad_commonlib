local options = CurrentModOptions or empty_table

function ModCaptureGet()
	return CurrentModStorageTable.saved_mods
end

function ChooseModCapture(host, to_save)
	local storage = CurrentModStorageTable or {}
	local saved_mods = storage.saved_mods or {}
	local list = table.keys(saved_mods, true)
	local name
	if next(list) then
		name = WaitListChoice(host, list, "Choose Capture Name", storage.last_mod_capture, 5, to_save)
	elseif to_save then
		name = WaitInputText(host, "Choose Capture Name", "My Mods")
	else
		WaitMessage(host, T(634182240966, "Error"), Untranslated("No available mod captures found!"))
	end
	if not name then
		return
	end
	storage.last_mod_capture = name
	local capture = saved_mods[name]
	local selected = GetModsToLoad()
	if to_save then
		if not next(selected) then
			print(name, "removed")
			saved_mods[name] = nil
		else
			if capture and table.iequal(capture, selected) then
				return
			end
			print(name, "saved", #selected, "mods")
			saved_mods[name] = selected
		end
		storage.saved_mods = saved_mods
	else
		if not capture or selected and table.iequal(selected, capture) then
			return
		end
		print(name, "loaded", #capture, "mods")
		if not g_ModsUIContextObj then
			AllModsOff()
			for _, id in ipairs(capture) do
				TurnModOn(id)
			end
			TurnModOn(CurrentModId)
			ModsUIDialogEnd(host)
		else
			local enable = table.invert(capture)
			local enabled = table.invert(selected)
			for idx, mod in ipairs(ModsList) do
				local id = mod.id
				if enabled[id] ~= enable[id] then
					ObjModified(mod)
					if enable[mod.id] then
						g_ModsUIContextObj.enabled[id] = true
						TurnModOn(id)
					else
						g_ModsUIContextObj.enabled[id] = nil
						TurnModOff(id)
					end
					mod.Corrupted = nil
					mod.Warning = nil
					mod.Warning_id = nil
				end
			end
			if host and host.window_state ~= "destroying" then
				local dlg = GetDialog(host)
				dlg:UpdateActionViews(dlg)
			end
			ObjModified(g_ModsUIContextObj)
		end
	end
	if host then
		ObjModified(host.context) -- will update the action states
	end
	WriteModPersistentStorageTable()
end

function OpenModList(host)
	CreateRealTimeThread(function()
		local storage = CurrentModStorageTable or {}
		local list = {}
		local text_to_id = {}
		local select
		for _, mod in ipairs(ModsLoaded) do
			if not mod.packed then
				local mod_id = mod.id
				local text = mod.title .. " (" .. mod_id .. ")"
				list[#list + 1] = text
				text_to_id[text] = mod_id
				if mod_id == storage.last_edit_mod then
					select = text
				end
			end
		end
		local item = WaitListChoice(host, list, "Choose mod", select)
		local mod_id = item and text_to_id[item]
		local mod = mod_id and table.find_value(ModsLoaded, "id", mod_id)
		if mod then
			storage.last_edit_mod = mod_id
			WriteModPersistentStorageTable()
			return OpenModEditor(mod)
		end
	end)
end

----

local function AppendModCaptureActions()
	if UIFindControl("ModsUIDialog", "idModStore") then
		return
	end
	local _, parent, idx = UIFindControl("ModsUIDialog", "idModEditor")
	if not parent then
		return
	end
	table.insert(parent, idx + 1, PlaceObj('XTemplateAction', {
		'ActionId', "idModStore",
		'ActionName', T(162535700852, "Save"),
		'ActionToolbar', "ActionBar",
		'ActionGamepad', "LeftTrigger-ButtonA",
		'ActionState', function (self, host)
			return not ModsUIIsSubmenuFocused(host) or "hidden"
		end,
		'OnAction', function (self, host, source, ...)
			CreateRealTimeThread(ChooseModCapture, host, true)
		end,
	}))
	table.insert(parent, idx + 2, PlaceObj('XTemplateAction', {
		'ActionId', "idModRestore",
		'ActionName', T(719213652871, "Load"),
		'ActionToolbar', "ActionBar",
		'ActionGamepad', "LeftTrigger-ButtonA",
		'ActionState', function (self, host)
			if ModsUIIsSubmenuFocused(host) then return "hidden" end
			if not next(ModCaptureGet()) then return "disabled" end
		end,
		'OnAction', function (self, host, source, ...)
			CreateRealTimeThread(ChooseModCapture, host)
		end,
	}))
end

local function AppendModListAction()
	if UIFindControl("IGMainActions", "idModList") then
		return
	end
	local _, parent, idx = UIFindControl("IGMainActions", "idMainMenu")
	table.insert(parent, idx + 1, PlaceObj('XTemplateAction', {
		'ActionId', "idModList",
		'ActionName', T(758873891950, "Mod"),
		'ActionToolbar', "mainmenu",
		'OnAction', function (self, host, source, ...)
			OpenModList(host)
		end,
		'__condition', function (parent, context)
			for _, mod in ipairs(ModsLoaded) do
				if not mod.packed then
					return true
				end
			end
		end,
	}))
end

function OnMsg.ModsReloaded()
	AppendModCaptureActions()
	AppendModListAction()
end

----

if Platform.debug and Platform.steam then

function ModWaitDownload(mods)
	local existing = table.invert(table.map(ModsList, "id"))
	local download = {}
	for _, mod in ipairs(mods) do
		if existing[mod.id] then
			--
		elseif not mod.steam_id then
			print("[Mod Sync] Mod without Steam ID: ", mod.id, mod.title)
		else
			download[#download + 1] = mod
		end
	end
	if #download == 0 then
		return
	end
	print("[Mod Sync] Downloading mods:")
	for _, mod in ipairs(download) do
		print("   ID", mod.id, "- Steam", mod.steam_id, "-", mod.title)
	end
	local threads = {}
	Msg("DebugDownloadExternalMods", download, threads)
	if #threads == 0 then
		return
	end
	while next(threads) do
		for i = #threads, 1, -1 do
			if not IsValidThread(threads[i]) then
				table.remove(threads, i)
			end
		end
		Sleep(50)
	end
	return ForceModReload()
end

function ModSync(active_mods)
	if next(active_mods) and IsRealTimeThread() and options.AutoSyncMissingMods and IsSteamLoggedIn() then
		ModWaitDownload(active_mods)
		local mods = table.map(active_mods, "id")
		local enabled = SetEnabledMods(mods, true) or ""
		if #enabled > 0 then
			print("[Mod Sync] Enabled mods:")
			for _, mod in ipairs(enabled) do
				print("   ", mod.id, ":", mod.title)
			end
		end
	end
end

local origGetMissingMods = GetMissingMods
function GetMissingMods(active_mods, max_mods)
	ModSync(active_mods)
	return origGetMissingMods(active_mods, max_mods)
end

end -- Platform.debug