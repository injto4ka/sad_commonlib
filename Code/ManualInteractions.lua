function Building:ManuallyInteract(unit)
	if not self.can_turn_off then return end
	NetSyncEvents.TurnOnOff(UIPlayer, self, not self.on)
	local obj = self:GetManipulationActivityObject()
	if not obj or not obj:IsActivityFree() then return end
	local activity_id = obj.activity_id
	local skill_level, efficiency = unit:GetSkillAndEfficiency(activity_id)
	unit:EnqueueAndTryExecuteActivity(activity_id, obj, skill_level, efficiency, "clear_queue")
end

function ManuallyToggle(device, unit)
	if not device.can_turn_off or unit.operated_mech or (unit.IsActivityAllowed and not unit:IsActivityAllowed("Manipulation")) then return end
	return device.on and device.turn_off_text or device.turn_on_text
end

function OnMsg.ClassesPostprocess()
	ClassLeafDescendantsList("Building", function(class, classdef)
		if classdef.IsManuallyInteractable ~= empty_func then
			--
		elseif classdef.can_turn_off then
			classdef.IsManuallyInteractable = ManuallyToggle
		end
	end)
end

----

AppendClass.OptionsObject = {
	properties = {
		{ name = T("No interactions in draft mode"), id = "DisableDraftInteract", category = "Gameplay", editor = "bool", default = false },
	}
}

function Unit:CanManuallyInterractWithObjects()
	return not GetAccountStorageOptionValue("DisableDraftInteract")
end

----

function IsBinSuitableForDraftMode(bin)
	if #(bin or "") == 0 or not IsKindOf(bin[1], "Unit") then return end
	for _, unit in ipairs(bin) do
		if unit:IsManuallyControlled() then
			return true
		end
	end
end

function UIReactivateDraftMode()
	local bin = g_CurrentSelectionBin or {SelectedObj}
	if IsBinSuitableForDraftMode(bin) then
		DelayedCall(0, UISetDraftMode, true, bin)
		for _, unit in ipairs(bin) do
			ObjModified(unit)
		end
	end
end

function CalcVisualPath(unit, dest)
	local pf_obj = GetPathTestObj()
	pf.ClearPath(pf_obj)
	pf.SetPfClass(pf_obj, unit:GetPfClass())
	pf.ChangePathFlags(pf_obj, unit.pfflags)
	pf.SetCollisionRadius(pf_obj, unit:GetCollisionRadius())
	pf.SetDestlockRadius(pf_obj, unit:GetRadius())
	pf.RestrictArea(pf_obj)
	local x, y, z = unit:GetVisualPosXYZ()
	if not unit:IsValidZ() then
		z = nil
	end
	pf_obj:SetPos(x, y, z)
	local status = pf_obj:FindPath(dest)
	local reachable = status == 0
	local path = GetVisualPathPoints(pf_obj)
	return path, reachable
end

----
