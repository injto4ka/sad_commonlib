function UnitInvader:CmdDespawn(despawn_start)
	self.despawn_start = despawn_start or self.despawn_start or GameTime()
	local dest = self.despawn_pos
	if dest and self:IsValidPos() and self:CheckPassable() and GameTime() - self.despawn_start < const.DayDuration then
		self.roam_start_pos = nil
		local min_dist, max_dist = 2*self:GetRadius(), 300*guim
		local pfclass = self:GetPfClass()
		local has_path = terrain.IsPassable(dest, pfclass) and self:CheckConnectivity(dest)
		if not has_path or not self:IsCloser(dest, max_dist) then
			local dist = self:GetDist(dest)
			local src = self:GetPos()
			local dir = dest - src
			if not has_path then
				dest = false
			end
			for i=1,9 do
				local test = src + MulDivRound(dir, 10 - i, 10)
				if not dest or GetIrritationAt(test) == 0 then
					test = terrain.FindPassable(test, pfclass, Max(10*guim, dist/20))
					if test and self:CheckConnectivity(test) then
						dest = test
						if self:IsCloser(dest, max_dist) then
							break
						end
					end
				end
			end
		end
		if not dest or not self:Goto(dest, min_dist, 0) then
			self:Roam()
			return self:PlayIdleAnim(const.HourDuration/2 + self:Random(const.HourDuration))
		end
	end
	if IsValid(self) then
		return self:ExecuteUninterruptable(self.DespawnWithAnim)
	end
end