local ResolveDamageToBuildings = ResolveDamageToBuildings
local Max = Max
local IsValid = IsValid
local pairs = pairs

AutoResolveMethods.ModifyDamageReceived = "modify"
DamageableObject.ModifyDamageReceived = empty_func

AutoResolveMethods.ModifyDamageInflicted = "modify"
CombatObject.ModifyDamageInflicted = empty_func

function UnitCombat:ModifyDamageReceived(damage, weapon_def, attacker)
	return self:CallReactions_Modify("ModifyDamageReceived", damage, weapon_def, attacker)
end

function UnitCombat:ModifyDamageInflicted(damage, weapon_def, target)
	return self:CallReactions_Modify("ModifyDamageInflicted", damage, weapon_def, target)
end

----

function FuncPieces:WeaponWithoutAoE()
	return self.AreaOfEffect == 0
end

SetPropMeta("WeaponResource", "DamageToBuildings", "name", "Integrity damage", CurrentModDef)
SetPropMeta("WeaponResource", "DamageToBuildings", "help", "The amount of direct damage dealt to integrity targets (structures, devices and robots) on each succesful hit. The integrity targets are not affected by Health conditions.", CurrentModDef)
SetPropMeta("WeaponResource", "AreaOfEffect", "name", "Area of Effect radius (AoE)", CurrentModDef)
SetPropMeta("WeaponResource", "AreaOfEffect", "help", "Damage radius around the hit pos. The damage is maximal at the center and gradually decreases to zero at the AoE border", CurrentModDef)
SetPropMeta("WeaponResource", "DmgPropagationTime", "name", "Damage propagation time (AoE)", CurrentModDef)
SetPropMeta("WeaponResource", "DmgPropagationTime", "no_edit", FuncPieces.WeaponWithoutAoE, CurrentModDef)
SetPropMeta("WeaponResource", "DamageCone", "name", "Attack Cone", CurrentModDef)

AppendClass.WeaponResource = {
	properties = {
		{ category = "Weapon", id = "BonusDamageFromPhysical", editor = "number", default = 0, scale = "%", template = true, help = "Bonus to damage at max physical skill" },
	},
	attacked_by_weapon = false,
}

----

AppendClass.UnitHealth = {
	properties = {
		{ category = "Combat", id = "damage_reduction", help = "Direct Damage Reduction", editor = "prop_table", default = false, no_edit = true, template = true },
	},
	attacked_by_weapon = false,
}

for _, dtype in ipairs(const.WeaponDamageTypes) do
	local getter = function(self)
		return self.damage_reduction and self.damage_reduction[dtype] or 0
	end
	local setter = function(self, value)
		if value == 0 then
			if self.damage_reduction then
				self.damage_reduction[dtype] = nil
				if next(self.damage_reduction) == nil then
					self.damage_reduction = nil
				end
			end
		else
			self.damage_reduction = table.create_set(self.damage_reduction, dtype, value)
		end
	end
	local prop_id = "DamageReduction_" .. dtype
	table.insert(UnitHealth.properties, {
		category = "Combat", id = prop_id, name = "Damage Reduction: " .. dtype, editor = "number", default = 0,
		min = -100, max = 100, slider = true, scale = "%", template = true, getter = getter, setter = setter,
		dont_save = true, help = "Direct damage reduction from " .. dtype .. " attacks", })
	UnitHealth["Get" .. prop_id] = getter
	UnitHealth["Set" .. prop_id] = setter
end

local origOnAttackReceived = UnitHealth.OnAttackReceived
function UnitHealth:OnAttackReceived(attacker, weapon_def, weapon_obj, aoe_power, ...)
	self.attacked_by_weapon = weapon_def
	self.attacked_aoe_power = aoe_power
	return origOnAttackReceived(self, attacker, weapon_def, weapon_obj, aoe_power, ...)
end

local origAddStatusEffect = UnitHealth.AddStatusEffect or StatusEffectsObject.AddStatusEffect
function UnitHealth:AddStatusEffect(cond)
	cond = origAddStatusEffect(self, cond)
	local damage = cond and cond.HealthLoss or 0
	if damage > 0 and cond.reason == "combat" and cond.Instance then
		local attacker = cond.source
		local weapon_def = self.attacked_by_weapon
		if attacker and weapon_def then
			damage = self:ModifyDamageReceived(damage, weapon_def, attacker, self.attacked_aoe_power, self.damage_reduction) or damage
			local ModifyDamageInflicted = attacker.ModifyDamageInflicted or empty_func
			damage = ModifyDamageInflicted(attacker, damage, weapon_def, self) or damage
			cond.HealthLoss = damage
		end
	end
	return cond
end

function UnitHealth:ModifyDamageReceived(damage, weapon_def, attacker, aoe_power, damage_reduction)
	if damage <= 0 then
		return damage
	end
	if (aoe_power or 100) < 100 then
		damage = damage * Max(aoe_power, 20) / 100
	end
	if damage_reduction then
		local total_reduction, norm = 0, 0
		for dtype in pairs(weapon_def.DamageTypes) do
			total_reduction = total_reduction + (damage_reduction[dtype] or 0)
			norm = norm + 100
		end
		if total_reduction > 0 then
			damage = damage * (norm - total_reduction) / norm
		end
	end
	if weapon_def.ScaleDamageWithUnit and IsValid(attacker) then
		damage = damage * attacker:GetScale() / 100
	end
	return damage
end

----

function UnitSkill:ModifyDamageInflicted(damage, weapon_def, target)
	local bonus = weapon_def.BonusDamageFromPhysical or 0
	if bonus == 0 then return damage end
	return damage + damage * bonus * self:GetSkillLevel("Physical") / 1000
end

function IntegrityProps:OnAttackReceived(attacker, weapon_def, weapon_obj, aoe_power)
	if self.Health == 0 or not weapon_def then return end
	self:MarkAsAttacked()
	if self.invulnerable then return end
	local damage = ResolveDamageToBuildings(weapon_def, attacker, aoe_power, self.damage_reduction)
	if damage <= 0 then return end
	local ModifyDamageInflicted = attacker and attacker.ModifyDamageInflicted
	if ModifyDamageInflicted then
		damage = ModifyDamageInflicted(attacker, damage, weapon_def, self) or damage
	end
	damage = self:ModifyDamageReceived(damage, weapon_def, attacker) or damage
	self:ChangeHealth(-damage, "combat")
	if self:IsDead() then
		self:RegisterDestroyer(attacker)
	end
end

----

-- Make the damage negation chances modifiable, therefore not using a table but separate properties

local first_idx
local dtype_to_prop_id = {}
local prop_id_to_dtype = {}
local attack_props = AttackableObject.properties
for _, dtype in ipairs(const.WeaponDamageTypes) do
	local prop_id = "HitNegationChance_" .. dtype
	dtype_to_prop_id[dtype] = prop_id
	prop_id_to_dtype[prop_id] = dtype
	local getter = function(self)
		local chances = self.HitNegationChance
		local chance = chances and (rawget(self, prop_id) or chances[dtype])
		return chance or self[prop_id]
	end
	local setter = function(self, value)
		local chances = self.HitNegationChance or {}
		self.HitNegationChance = chances
		chances[dtype] = value
		self[prop_id] = value
	end
	local new_prop = {
		category = "Combat", id = prop_id, name = T{622960529683, "Damage resistance <right><em><resistances></em>", resistances = const.WeaponDamageTypeNames[dtype]},
		editor = "number", default = 0, scale = "%",
		template = true, modifiable = true,
		help = "Chance to negate ".. dtype .. " attacks",
		getter = getter, setter = setter,
	}
	local prop, idx = table.find_value(attack_props, "id", prop_id) or {}
	first_idx = first_idx or idx
	if not prop then
		table.insert(attack_props, first_idx or (#attack_props + 1), new_prop)
	else
		table.clear(prop)
		table.append(prop, new_prop)
	end
	AttackableObject["Get" .. prop_id] = nil
	AttackableObject["Set" .. prop_id] = nil
end

function OnMsg.ClassesPreprocess(classdefs)
	-- HitNegationChance table compatibility
	for class, def in pairs(classdefs) do
		local chances = def.HitNegationChance
		if chances then
			local tmp = {}
			for key, chance in pairs(chances) do
				local dtype = dtype_to_prop_id[key] and key or prop_id_to_dtype[key]
				if dtype then
					tmp[dtype] = chance
				end
			end
			table.clear(chances)
			for dtype, chance in pairs(tmp) do
				chances[dtype] = chance
				local prop_id = dtype_to_prop_id[dtype]
				if prop_id and not rawget(def, prop_id) then
					def[prop_id] = chance
				end
			end
		end
	end
end

function AttackableObject:GetHitNegationChanceForType(damage_type)
	local prop_id = dtype_to_prop_id[damage_type]
	return prop_id and self[prop_id] or 0
end

function UnitAnimal:GetIPDamageTypeResistances()
	local resistances
	for _, dtype in ipairs(const.WeaponDamageTypes) do
		local prop_id = dtype_to_prop_id[dtype]
		local resistance = self[prop_id]
		if (resistance or 0) > 0 then
			resistances = table.create_add(resistances, dtype)
		end
	end
	if resistances then
		table.sort(resistances)
		local resistances_texts = {}
		for i, dtype in ipairs(resistances) do
			resistances_texts[#resistances_texts + 1] = const.WeaponDamageTypeNames[dtype]
		end
		resistances_texts = table.concat(resistances_texts, ", ")
		if #resistances == 1 then
			return T{622960529683, "Damage resistance <right><em><resistances></em>", resistances = resistances_texts}
		else
			return T{705697722836, "Damage resistances <right><em><resistances></em>", resistances = resistances_texts}
		end
	end
	return ""
end

----

-- Show all damage negation types as long as they aren't zero
function OnMsg.ModsReloaded()
	UIForEachControl(XTemplates.tabUnitEquipment_Slots, function(control, parent)
		if parent.comment == "damage negation" and control.__condition then
			control.__condition = function (parent, context)
				return (context.negation or 0) > 0
			end
		end
	end)
end

----

