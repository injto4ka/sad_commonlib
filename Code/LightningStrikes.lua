----

local HourDuration = const.HourDuration
local DayDuration = const.DayDuration
local IsValid = IsValid
local max_shield_area = const.Disaster.LightningRodMaxShieldRange

-- Code is almost same but optimized for speed and memory. The only difference is the target redirection to higher more conductive targets.

LightningTargetGetters = { 
	LightningChanceToHitPlant = function (target, dist, seed)
		return MapGetFirst(target, dist, "shuffle", seed, "Plant")
	end,
	LightningChanceToHitDevice = function (target, dist, seed)
		return MapGetFirst("map", "shuffle", seed, "Building", "CombatMech")
	end,
	LightningChanceToHitRod = function (target, dist, seed)
		return MapGetFirst("map", "shuffle", seed, "LightningRodBuilding")
	end,
	LightningChanceToHitAnimal = function (target, dist, seed)
		return MapGetFirst(target, dist, "shuffle", seed, "UnitAnimal")
	end,
	LightningChanceToHitHuman = function (target, dist, seed)
		return target
	end,
}
local LightningTargetGetters = LightningTargetGetters

function DoTargetedLightningStrikes()
	local Region = Region
	local rand = InteractionRandCreate("LightningStrikes")
	local LightningTargets = LightningTargets
	Sleep(Region.LightningStrikeDelay)
	local hits = {}
	while GameState.Thunderstorm do
		Sleep(Region.LightningStrikeInterval)
		if rand(100) < Region.LightningStrikeChance then
			-- select a survivor to be targeted by a lightning
			local survivor = rand(AllSurvivors)
			if IsValid(survivor) and not survivor:IsDead() and survivor:IsValidPos() then
				local distance = Region.LightningStrikeDistance
				local chance = rand(100)
				for _, entry in ipairs(LightningTargets) do
					local target_id = entry.weight
					local weight = Region[target_id] or 0
					assert(weight > 0)
					chance = chance - weight
					if chance < 0 then
						local getter = LightningTargetGetters[target_id] or empty_func
						local obj = getter(survivor, distance, rand())
						if obj and StrikeTargetWithLightning(obj, rand) then
							hits[#hits + 1] = ResolvePos(obj)
							Sleep(Region.LightningStrikeCooldown or 0)
						end
						break
					end
				end
			end
		end
	end
end

function OnMsg.GameStateChanged()
	if not GameState.Thunderstorm then
		DeleteThread(TargetedLightningThread)
	elseif not IsValidThread(TargetedLightningThread) then
		TargetedLightningThread = CreateGameTimeThread(DoTargetedLightningStrikes)
	end
end

function StrikeTargetWithLightning(target, rand, offset, height)
	local st = GetPreciseTicks()
	local orig_target = target
	DbgClear()
	target = target:TryToRedirectLightning() or target
	if not target:CanBeStruckByLightning() then
		return
	end
	local target_pos = target:GetLightningHitPos()
	
	SuspendPassEdits("StrikeTargetWithLightning")
	DbgAddVector(target_pos, 100*guim, yellow)
	DbgAddSegment(target_pos, orig_target)
	if orig_target ~= target then
		print("Redirect", orig_target.class, "-->", target.class)
	else
		print("Direct", target.class)
	end
	
	rand = rand or InteractionRandCreate("LightningStrikes")
	local start_pos = RotateRadius(rand(offset or 20*guim), rand(60*360), target_pos)
	start_pos = start_pos:SetTerrainZ(height or 30*guim)
	PlayFX("LightningStrike", "start", start_pos, target, target_pos)
	target:TakeLightningStrike()
	if not target.absorbs_lightning and not target:IsValidZ() then
		-- strike nearby plants as well
		local dist = const.Disaster.LightningStrikePlantsHitArea
		DbgAddCircle(target, dist, red)
		MapForEach(target, dist, "LightningTarget", function(obj, target, dist)
			if obj ~= target and not obj:IsValidZ() and obj:IsCloser(target, dist) and obj:CanBeStruckByLightning() then
				obj:TakeLightningStrike()
				print("Indirect", obj.class)
				DbgAddVector(obj, 10*guim, red)
			end
		end, target, dist)
	end
	ResumePassEdits("StrikeTargetWithLightning")
	
	print(GetPreciseTicks() - st)
	return true
end

AppendClass.LightningTarget = {
	properties = {
		{ category = "Lightning", id = "lightning_hit_spot",            name = "Lightning Hit Spot",   editor = "combo", items = GetEntitySpotsItems, template = true },
		{ category = "Lightning", id = "absorbs_lightning",             name = "Absorbs Lightning",    editor = "bool", template = true },
		{ category = "Lightning", id = "lightning_attraction",          name = "Lightning Attraction", editor = "number", default = false, template = true, help = "Custom lightning attraction. If not specified, it will be estimated based on material type" },
		{ category = "Lightning", id = "lightning_attraction_modifier", name = "Attraction Modifier",  editor = "number", default = 1, template = true, no_edit = PropGetter("lightning_attraction"), help = "Modifier for the attraction estimation" },
	},
	lightning_hit_spot = false,
}

local gofAny = const.gofPermanent | const.gofSyncObject

function LightningTarget:GetDescription()
	return self.description
end

function LightningTarget:TryToRedirectLightning()
	DbgAddCircle(self, max_shield_area)
	local pos = self:GetVisualPos()
	local redirected = MapGetFirst(pos, max_shield_area, "LightningRodBuilding", nil, nil, nil, gofAny, function(obj, pos)
		if not obj.working then return end
		return obj:IsCloser2D(pos, obj.ShieldArea)
	end, pos)
	if redirected then
		return redirected
	end
	redirected = MapFindMin(pos, max_shield_area, "LightningTarget", nil, nil, nil, gofAny, function(obj, pos)
		local attraction = obj:CanBeStruckByLightning() and obj:GetLightingAttraction(pos)
		if attraction then
			DbgAddText(attraction, obj)
			return -attraction
		end
	end, pos)
	if redirected and redirected ~= self then
		local pos1 = redirected:GetVisualPos()
		redirected = MapGetFirst(pos1, max_shield_area, "LightningRodBuilding", nil, nil, nil, gofAny, function(obj, pos1)
			if not obj.working then return end
			return obj:IsCloser2D(pos1, obj.ShieldArea)
		end, pos1) or redirected
	end
	return redirected
end	

function LightningTarget:GetLightingAttraction(pos)
	local hit = self:GetLightningHitPos()
	local attraction = self.lightning_attraction * (hit:z() - pos:z()) / 2 - self:GetDist2D(pos)
	local count = 1
	for _, support in ipairs(self.supported_by) do
		local GetLightingAttraction = support.GetLightingAttraction or empty_func
		local attraction_i = GetLightingAttraction(support, pos)
		if attraction_i then
			attraction = attraction + attraction_i
			count = count + 1
		end
	end
	return attraction / count
end

function LightningTarget:GetLightningHitPos()
	local spot = self.lightning_hit_spot
	if spot then
		return self:GetSpotLocPos(self:GetSpotBeginIndex(self.lightning_hit_spot))
	end
	if self:HasSpot("Target") then
		local x, y, z
		local i_start, i_end = self:GetSpotRange("Target")
		for i = i_start, i_end do
			local xi, yi, zi = self:GetSpotLocPosXYZ(i, true)
			if not z or z < zi then
				x, y, z = xi, yi, zi
			end
		end
		if x then
			return point(x, y, z)
		end
	end
	local bbox = self:GetObjectBBox()
	return bbox:Center()
end

function OnMsg.ClassesPostprocess()
	ClassDescendantsList("LightningRodBuilding", function(name, def)
		max_shield_area = Max(max_shield_area, def.ShieldArea)
	end)
	local classes = g_Classes
	local attractions = {}
	ClassDescendants("LightningTarget", function(class, def)
		if def.lightning_attraction then return end
		local entity = IsValidEntity(def.entity) and def.entity
		local entity_def = entity and classes[entity]
		local material_type = def:GetMaterialType() or entity_def and entity_def:GetMaterialType() or entity
		local is_metal = material_type and string.find_lower(def.material_type or "", "metal")
		attractions[def] = (is_metal and 10 or 1) * def.lightning_attraction_modifier
	end)
	for def, attraction in pairs(attractions) do
		def.lightning_attraction = attraction
	end
end

VolumeElementBuilding.absorbs_lightning = true

function VolumeElementBuilding:GetLightningHitPos()
	return self:GetVisualPos()
end

function RoomWallPiece:GetLightningHitPos()
	local x, y, z = self:GetVisualPosXYZ()
	return point(x, y, z + const.Gameplay.RoofHeight)
end

function RoofPiece:GetLightningHitPos()
	local x, y, z = self:GetVisualPosXYZ()
	return point(x, y, Max(z, self.end_z or 0))
end

AppendClass.ScavengeableObject = {
	__parents = { "LightningTarget" },
	absorbs_lightning = true,
	lightning_attraction_modifier = 100,
}

AppendClass.MilitaryWonderBuilding = {
	absorbs_lightning = true,
	lightning_attraction = 10000,
}

AppendClass.PowerPoleBuilding = {
	absorbs_lightning = true,
	lightning_attraction_modifier = 10,
	lightning_hit_spot = "Wireeast1",
	CanBeStruckByLightning = ConstructionElement.CanBeStruckByLightning,
}

AppendClass.WindTurbineBuilding = {
	absorbs_lightning = true,
	lightning_attraction_modifier = 10,
	lightning_hit_spot = "Top",
	CanBeStruckByLightning = ConstructionElement.CanBeStruckByLightning,
}

AppendClass.LightningRodBuilding = {
	absorbs_lightning = true,
	lightning_attraction = 10000,
}

AppendClass.Shelter = {
	absorbs_lightning = true,
}

WallOpenCloseObj.CanBeStruckByLightning = empty_func

function SupportObject:CanBeStruckByLightning()
	return not next(self.supported_objs)
end

function UnitEquipmentAutoResolve:CanBeStruckByLightning()
	return not self:HasSurvivalTool("EMUmbrella")
end

local Building_TakeLightningStrike = Building.TakeLightningStrike
function Building:TakeLightningStrike()
	if self.absorbs_lightning then
		return LightningTarget.TakeLightningStrike(self)
	end
	return Building_TakeLightningStrike(self)
end

function OnMsg.StruckByLightning(obj)
	if obj.Malfunction then
		obj:Malfunction()
	end
end

AppendClass.RegionDef = {
	properties = {
		{ category = "Disasters", id = "LightningStrikeCooldown", name = "Lightning strike cooldown after hit", 
			editor = "number", default = 10000, scale = "sec", },
		{ category = "Disasters", id = "LightningStrikeDelay", name = "Lightning strike initial delay", 
			editor = "number", default = 20000, scale = "sec", },
	},
}