local IsAttackable = IsAttackable

local origNestOnAttackReceived = AnimalNestAutoResolve.OnAttackReceived
function AnimalNestAutoResolve:OnAttackReceived(attacker, ...)
	if not IsAttackable(attacker) then
		return
	end
	return origNestOnAttackReceived(self, attacker, ...)
end

local origUnitNestingOnAttackReceived = UnitNesting.OnAttackReceived
function UnitNesting:OnAttackReceived(attacker, ...)
	if not IsAttackable(attacker) then
		return
	end
	return origUnitNestingOnAttackReceived(self, attacker, ...)
end

local origNestRegisterTarget = TerritorialNest.RegisterTarget
function TerritorialNest:RegisterTarget(unit, time)
	if not IsAttackable(unit) then
		return
	end
	return origNestRegisterTarget(self, unit, time)
end

function SavegameFixups.FixNestTargets()
	MapForEach("map", "TerritorialNest", function(self)
		local targets = self.nest_targets
		for i=#(targets or ""),1,-1 do
			local target = targets[i]
			if not IsAttackable(target) then
				targets[target] = nil
				table.remove_rotate(targets, i)
			end
		end
	end)
end