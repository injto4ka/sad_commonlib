local next = next
local pairs = pairs
local ipairs = ipairs
local type = type
local getmetatable = getmetatable
local setmetatable = setmetatable
local tostring = tostring
local pcall = pcall
local IsT = IsT
local TGetID = TGetID
local band, bor = band, bor
local LightUserDataValue = LightUserDataValue
local LightUserData = LightUserData
local options = CurrentModOptions or empty_table

local locId_sig = shift(0xff, 56)
local locId_mask = bnot(locId_sig)

function LocIDToLightUserdata(id)
	return id and LightUserData(bor(id, locId_sig))
end

function LightUserdataToLocId(value)
	value = LightUserDataValue(value)
	if value and band(value, locId_sig) == locId_sig then
		return band(value, locId_mask)
	end
end
local LightUserdataToLocId = LightUserdataToLocId

local function __TExtractText(T)
	if T == "" then
		return ""
	end
	if not IsT(T) then
		return T
	end
	if type(T) ~= "table" --[[shield for non-debug mode]] then
		local id = LightUserdataToLocId(T)
		return id and TranslationTable[id] or "Missing text"
	end
	local ret = type(T[1]) == "number" and T[2] or T[1]
	if type(ret) == "string" then
		return ret
	end
	return __TExtractText(ret)
end
TExtractText = __TExtractText

local function __DuplicateT(v)
	if getmetatable(v) == TConcatMeta then
		local ret = {}
		for i, t in ipairs(v) do
			ret[i] = __DuplicateT(t)
		end
		return setmetatable(ret, TConcatMeta)
	end
	return T{RandomLocId(), __TExtractText(v)}
end

function __DuplicateTs(obj, visited, ignore)
	for key, value in pairs(obj) do
		if not ignore[key] then
			if value ~= "" and IsT(value) then
				obj[key] = __DuplicateT(value)
			elseif type(value) == "table" and not visited[value] and not value.NoLocDuplicate then
				visited[value] = true
				__DuplicateTs(value, visited, ignore)
			end
		end
	end
end
function DuplicateTs(obj, visited, ignore)
	if options.NoLocChangeOnCopy then
		return
	end
	visited = visited or {}
	ignore = ignore or ParentTableCacheIgnoreKeys
	return __DuplicateTs(obj, visited, ignore)
end

ModDef.NoLocDuplicate = true

local origGenerateLocalizationIDs = GenerateLocalizationIDs
function GenerateLocalizationIDs(...)
	if options.NoLocChangeOnCopy then
		return
	end
	return origGenerateLocalizationIDs(...)
end

if Platform.debug then
				
function OnMsg.ClassesGenerate(classdefs)
	for class, def in pairs(classdefs) do
		local props = def.properties
		for i, prop in ripairs(def.properties) do
			if prop.editor == "text" and prop.translate or type(prop.editor) == "function" then
				local prop_id = prop.id
				local function GetLocID(self)
					local text = self:GetProperty(prop_id) or ""
					return text ~= "" and IsT(text) and TGetID(text)
				end
				local no_edit = function(self)
					return not options.ShowPropLocID or not GetLocID(self)
				end
				local name = prop.name or prop_id
				if IsT(name) then
					name = name .. Untranslated(" (ID)")
				else
					name = name .. " (ID)"
				end
				table.insert(props, i + 1, { 
					category = prop.category,
					id = prop_id .. "_loc_id",
					name = name,
					editor = "number",
					default = 0,
					no_edit = no_edit,
					read_only = true,
					dont_save = true,
				})
				def["Get" .. prop_id .. "_loc_id"] = GetLocID
			end
		end
	end
end

end -- Platform.debug

if FirstLoad then
	LocIdIgnoredErrors = {}
	local LocIdToT = {}
	local LocIdToTexts = {}
	local origT = T
	function T(...)
		local T = origT(...)
		local id = TGetID(T)
		if id then
			local prev_T = LocIdToT[id]
			if not prev_T then
				LocIdToT[id] = T
			else
				local text = __TExtractText(T)
				local prev_text = __TExtractText(prev_T)
				if text ~= prev_text then
					local texts = LocIdToTexts[id]
					if not texts then
						texts = {prev_text, text}
						LocIdToTexts[id] = texts
					else
						texts[#texts + 1] = text
					end
				end
			end
		end
		return T
	end
	function OnMsg.ModsReloaded()
		CreateRealTimeThread(function()
			T = origT
			local ignored = LocIdIgnoredErrors
			for id, texts in sorted_pairs(LocIdToTexts) do
				if not ignored[id] then
					local lines = { string_format("\nDuplicated loc ID %d\n", id) }
					local seed = {}
					for _, text in ipairs(texts) do
						if not seed[text] then
							seed[text] = true
							lines[#lines + 1] = string_format('%d, "%s"', id, text)
						end
					end
					ModLogF(true, table.concat(lines, "\n"))
				end
			end
		end)
	end
end