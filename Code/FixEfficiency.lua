DefineConstInt("Human", "MaxEfficiency", 500, "%")

local max_efficiency = const.Human.MaxEfficiency or 500
local max_skill_level = #const.SkillExpAtLevel

-- Removes the efficiency cap of 100 for EfficiencyFromManipulation and EfficiencyFromConsciousness
function Human:GetSkillAndEfficiency_Mastery(activity)
	if type(activity) == "string" then
		activity = ActivityTypes[activity]
	end
	local skill_level = self:GetActivitySkillLevel(activity)
	local efficiency = 100 + activity.EfficiencyFromSkill * skill_level / max_skill_level
	if activity.EfficiencyFromManipulation then
		efficiency = efficiency * self.Manipulation / 100000
	end
	if activity.EfficiencyFromConsciousness then
		efficiency = efficiency * self.Consciousness / 100000
	end
	return skill_level, Clamp(efficiency, 10, max_efficiency) -- efficiency should be greater than 0 to prevent division by zero
end