local TTranslate = TTranslate
local IsKindOf = IsKindOf
local IsPresetWithConstantGroup = IsPresetWithConstantGroup

function PresetsCombo(class, group, additional, filter, format)
	return function(obj, prop_meta)
		local ids = {}
		local encountered = {}
		if type(additional) == "table" then
			for i = #additional, 1, -1 do
				encountered[additional[i]] = true
			end
		elseif additional ~= nil then
			encountered[additional] = true
		end
		local classdef = class and g_Classes[class]
		if classdef then
			local preset_class = classdef.PresetClass or class
			local presets = Presets[preset_class] or empty_table
			local target_list
			if group or not classdef.GlobalMap then
				group = group or IsPresetWithConstantGroup(classdef) and classdef.group or #presets == 1 and 1 or false
				assert(group, "PresetsCombo requires a group when presets are not with unique ids (GlobalMap = true) or with a constant group")
				target_list = presets[group]
			end
			for _, list in ipairs(presets) do
				if not target_list or target_list == list then
					for _, preset in ipairs(list) do
						local id = preset.id
						if id ~= "" and not encountered[id] and not preset.Obsolete and IsKindOf(preset, class) and (not filter or filter(preset, obj, prop_meta)) then
							ids[#ids + 1] = id
							encountered[id] = preset
						end
					end
				end
			end
		end
		table.sort(ids)
		if type(additional) == "table" then
			for i = #additional, 1, -1 do
				table.insert(ids, 1, additional[i])
			end
		elseif additional ~= nil then
			table.insert(ids, 1, additional)
		end
		if format then
			for i, id in ipairs(ids) do
				local preset = encountered[id]
				if preset then
					ids[i] = { value = id, text = TTranslate(format, preset) }
				else
					ids[i] = { value = id, }
				end
			end
		end
		return ids
	end
end

local PresetsCombo = PresetsCombo
local eval = prop_eval

function PresetGroupCombo(class, group, filter, first_entry, format)
	if not first_entry then
		first_entry = ""
	elseif first_entry == "no_empty" then
		first_entry = nil
	end
	return PresetsCombo(class, group, first_entry, filter)
end

function GedGameSocket:rfnGetPresetItems(obj_name, prop_id)
	local obj = self:ResolveObj(obj_name)
	local meta = GedIsValidObject(obj) and obj:GetPropertyMetadata(prop_id)
	if not meta then return empty_table end -- can happen when the selected object in Ged changes and GetPresetItems RPC is sent after that
	
	local preset_class = eval(meta.preset_class, obj, meta)
	local preset_def = preset_class and g_Classes[preset_class]
	if not preset_def then return empty_table end
	
	local extra_item = eval(meta.extra_item, obj, meta) or ""
	local preset_group = eval(meta.preset_group, obj, meta)
	local enumerator = PresetsCombo(preset_class, preset_group, extra_item, meta.preset_filter, preset_def.ComboFormat)
	return eval(enumerator, obj, meta)
end