AppendClass.XTemplate = {
	properties = {
		{ category = "Template", id = "Singleton", editor = "bool", default = false },
	},
}

local OpenGedAppOrig = OpenGedApp
function OpenGedApp(template_id, root, context, ...)
	local template = XTemplates[template_id]
	if template and template.Singleton then
		local app = FindGedApp(template)
		if app then
			app:Call("rfnApp", "Activate", context)
			if app.bound_objects.root ~= root then
				app:BindObj("root", root)
				if app.last_app_state and app.last_app_state.root then
					local sel = app.last_app_state.root.selection
					if sel and type(sel[1]) == "table" then
						app:SetSelection("root", {1}, {1}) -- tree panel
					else
						app:SetSelection("root", 1, {1}) -- list panel
					end
				end
				app:ResetUndoQueue()
				app.last_app_state = false -- the last app state won't make sense for a new root object
			end
			return app
		end
	end
	return OpenGedAppOrig(template_id, root, context, ...)
end