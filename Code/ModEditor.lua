local options = CurrentModOptions or empty_table

-- Add version info when uploading
AppendClass.ModDef = {
	properties = {
		{ category = "Mod",  id = "append_version", name = "Append Version Info", editor = "bool", default = true },
		{ category = "Mod",  id = "restart_on_unload", name = "Restart On Unload", editor = "bool", default = false },
	}
}

local function GetAvailableDscrLen(mod)
	-- description max length cannot exceed 8000!!!
	if not mod.append_version then
		return 8000
	end
	local mod_version_str = mod:GetVersionString()
	local mod_version_header = "\n\n[hr][/hr]\nVersion: "
	return 8000 - #mod_version_str - #mod_version_header
end

local origUploadMod = UploadMod
function UploadMod(ged_socket, mod, params, prepare_fn, upload_fn)
	if mod.steam_id == 0 then
		-- Fix missing Steam id after upload
		local orig_prepare_fn = prepare_fn
		prepare_fn = function(ged_socket, mod, params)
			local success, message = orig_prepare_fn(ged_socket, mod, params)
			if mod.steam_id ~= 0 then
				CreateRealTimeThread(function()
					mod:SaveDef(true)
					mod:MarkClean()
				end)
			end
			return success, message
		end
	end
	if mod.append_version then
		local orig_upload_fn = upload_fn
		upload_fn = function(ged_socket, mod, params)
			local mod_version_str = mod:GetVersionString()
			local mod_description = mod.description
			local mod_version_header = "\n\n[hr][/hr]\nVersion: "
			local max_len = GetAvailableDscrLen(mod)
			local idx = string.find_plain(mod_description, mod_version_header)
			if idx then
				max_len = Min(max_len, idx - 1)
			end
			mod_description = string.sub(mod_description, 1, max_len)
			local instance = {
				description = mod_description .. mod_version_header .. mod_version_str,
				last_changes = "Version " .. mod_version_str .. "\n\n" .. mod.last_changes,
			}
			return orig_upload_fn(ged_socket, mod:CreateInstance(instance), params)
		end
	end
	return origUploadMod(ged_socket, mod, params, prepare_fn, upload_fn)
end

local prop = table.find_value(ModDef.properties, "id", "content_path")
if prop then
	table.insert(prop.buttons, {name = "Reload", func = "ReloadMod"})
end

local origGedOpUnloadMod = GedOpUnloadMod
local origGedOpLoadMod = GedOpLoadMod
local origGedOpEditMod = GedOpEditMod

function SaveModSettingsDelayed()
	return pcall(PlayStationActivities.DbgLaunchOnBoot, {})
end

function GedOpUnloadMod(socket, obj, item_idx)
	origGedOpUnloadMod(socket, obj, item_idx)
	SaveModSettingsDelayed()
end

function GedOpLoadMod(socket, obj, item_idx)
	origGedOpLoadMod(socket, obj, item_idx)
	SaveModSettingsDelayed()
end

function GedOpEditMod(socket, obj, item_idx)
	local mod = ModsList[item_idx]
	local loaded = table.find(ModsLoaded, mod)
	origGedOpEditMod(socket, obj, item_idx)
	if not loaded and table.find(ModsLoaded, mod) then
		SaveModSettingsDelayed()
	end
end

function ModDef:ReloadMod()
	CreateRealTimeThread(function()
		local item_idx = table.find(ModsList, self)
		if not item_idx then return end
		origGedOpUnloadMod(nil, nil, item_idx)
		local item_idx = table.find(ModsList, self)
		if not item_idx then return end
		origGedOpLoadMod(nil, nil, item_idx)
		OpenModEditor(self)
	end)
end

local restarting
function AskToRestartGame(text, delay)
	if restarting then return end
	local start = RealTime()
	while Loading do
		Sleep(100)
	end
	if restarting then
		return
	end
	restarting = true
	local res = WaitQuestion(terminal.desktop, T(824112417429, "Warning"), text, T(6878, "OK"), T(6879, "Cancel"))
	if res ~= "ok" then
		restarting = false
		return
	end
	LocalStorage.AutoStart = nil
	SaveLocalStorage()
	delay = delay or 0
	Sleep(start + delay - RealTime())
	WaitAccountStorageSaving()
	quit("restart")
end

function RestartOnModUnload(mod)
	local id = mod.id
	local title = mod.title
	if restarting or not Mods[id] then return end
	local start = RealTime()
	if not WaitMsg("ModsReloaded", 10000) then
		return
	end
	while Loading do
		Sleep(100)
	end
	mod = Mods[id]
	if restarting or mod and mod:ItemsLoaded() then
		return
	end
	local delay = start + 500 - RealTime()
	local text = Untranslated("Unloading mod " .. title .. " requires a game restart!")
	return AskToRestartGame(text, delay)
end

local origUnloadItems = ModDef.UnloadItems
function ModDef:UnloadItems()
	if not self:ItemsLoaded() then
		return
	end
	if self.restart_on_unload then
		CreateRealTimeThread(RestartOnModUnload, self)
	end
	return origUnloadItems(self)
end

local props = ModDef.properties
local prop, idx = table.find_value(props, "id", "description")
if prop then
	prop.no_edit = function(self) return self.SpellCheck end
	prop.lines = 5
	prop.max_lines = 16
	prop.max_len = GetAvailableDscrLen
	table.insert(props, idx + 1, {
		category = "Mod",  id = "DescriptionT", name = "Description", 
		editor = "text", translate = true, default = "",
		lines = prop.lines, max_lines = prop.max_lines, max_len = prop.max_len, dont_save = true,
		no_edit = function(self) return not self.SpellCheck end
	})
	table.insert(ModDef.properties, idx + 2, {
		category = "Mod",  id = "RemainingSymbols", name = "Remaining Symbols",
		editor = "number", default = 8000,
		read_only = true, dont_save = true
	})
end
local prop, idx = table.find_value(props, "id", "last_changes")
if prop then
	prop.no_edit = function(self) return self.SpellCheck end
	prop.lines = 3
	prop.max_lines = 8
	prop.max_len = 4000
	table.insert(props, idx + 1, {
		category = "Mod",  id = "LastChangesT", name = "Last changes",
		editor = "text", translate = true, default = "",
		lines = prop.lines, max_lines = prop.max_lines, max_len = prop.max_len, dont_save = true,
		no_edit = function(self) return not self.SpellCheck end
	})
end
local prop, idx = table.find_value(props, "id", "ignore_files")
if prop then
	table.insert(props, idx, { category = "Mod",  id = "SpellCheck", name = "Spell Check",  editor = "bool", default = false })
end

function ModDef:GetDescriptionT()
	return Untranslated(self.description)
end
function ModDef:SetDescriptionT(text)
	self.description = TTranslate(text or "")
	ObjModified(self)
end

function ModDef:GetModLabel(plainText)
	local text = string_format("%s ('%s' %s)", self.title, self.id, self:GetVersionString())
	return plainText and text or Untranslated(text)
end

function ModDef:GetLastChangesT()
	return Untranslated(self.last_changes)
end
function ModDef:SetLastChangesT(text)
	self.last_changes = TTranslate(text)
end
function ModDef:GetRemainingSymbols()
	local dscr = self:GetProperty("description")
	return GetAvailableDscrLen(self) - #dscr
end

----
-- Fix missing UI update after mod saving
local ModDef_SaveDef = ModDef.SaveDef
function ModDef:SaveDef(serialize_only)
	local err, code_dirty = ModDef_SaveDef(self, serialize_only)
	ObjModifiedMod(self)
	return err, code_dirty
end

-- Utility function for updating the Mod Editor tree panel for a given mod that changed
function ObjModifiedMod(mod)
	if not mod then return end
	for obj in pairs(GedObjects) do
		if type(obj) == "table" and (obj == mod or obj[1] == mod) then
			ObjModified(obj)
		end
	end
end

----

function ModDef:GetSortedCodeItems()
	local items
	self:ForEachItem(function(item)
		local name = item:GetCodeFileName() or ""
		if name ~= "" then
			items = items or {}
			items[#items + 1] = item
		end
	end)
	if not items then return end
	table.stable_sort(items, function(a, b)
		return (a.CodePriority or 0) > (b.CodePriority or 0)
	end)
	return items
end

local ModDef_UpdateCode = ModDef.UpdateCode
function ModDef:UpdateCode()
	local ok, items = procall(self.GetSortedCodeItems, self)
	if not ok then
		return ModDef_UpdateCode(self)
	end
	if not items and not self.code then
		return
	end
	local orig_items = self.items or nil
	self.items = items
	local ok, dirty = procall(ModDef_UpdateCode, self)
	self.items = orig_items
	return ok and dirty
end

----

function VersionEncodedModsCombo(self, prop)
	local result = {}
	local seen = {}
	for id, mod in pairs(Mods) do
		local encoded = string.format("%s %d %d %d", id, mod.version_major, mod.version_minor, mod.version)
		seen[encoded] = true
		table.insert(result, { value = encoded, text = string.format("%s - %s %s", mod.title, id, mod:GetVersionString()) })
	end
	for _, encoded in ipairs(self.value) do
		if not seen[encoded] then
			table.insert(result, { value = encoded, text = encoded })
		end
	end
	table.sort(result, function(e1, e2) return CmpLower(e1.text, e2.text) end)
	return result
end

function FindEncodedModVersion(id, list, cache)
	local info = cache and cache[id]
	if info ~= nil then
		return info
	end
	for _, encoded in ipairs(list) do
		if not cache or not cache[encoded] then
			local entries = string.split(encoded, ' ', true)
			local id_i = entries[1]
			local info_i = {
				id = id_i,
				vmaj = tonumber(entries[2]) or max_int,
				vmin = tonumber(entries[3]) or max_int,
				rev = tonumber(entries[4]) or max_int,
			}
			if cache then
				cache[encoded] = true
				cache[id_i] = info_i
			end
			if id == id_i then
				return info_i
			end
		end
	end
	if cache then
		cache[id] = false
	end
end

function ModDef:IsVersionNewer(vmaj, vmin, rev)
	if vmaj < self.version_major then
		return true
	elseif vmaj > self.version_major then
		return
	end
	if vmin < self.version_minor then
		return true
	elseif vmin > self.version_minor then
		return
	end
	return rev < self.version
end

local obsolete_cache = {}
function ModDef:IsModObsolete()
	local info = FindEncodedModVersion(self.id, const.ObsoleteMods, obsolete_cache)
	return info and not self:IsVersionNewer(info.vmaj, info.vmin, info.rev)
end

local ModDef_IsTooOld = ModDef.IsTooOld
function ModDef:IsTooOld()
	if options.AllowOutdatedMods then
		return
	end
	if self:IsModObsolete() then
		return true
	end
	return ModDef_IsTooOld(self)
end

----

local origOpenModEditor = OpenModEditor
function OpenModEditor(mod)
	if mod then
		local path = mod:GetModContentPath()
		for ged_id, ged in pairs(GedConnections) do
			if ged.app_template == "ModEditor" then
				local context = ged.context
				if type(context) == "table" and path == context.mod_content_path then
					ged:Call("rfnApp", "Activate", context)
					return ged
				end
			end
		end
	end
	local editor = origOpenModEditor(mod)
	if editor then
		editor:Send("rfnApp", "SetTitle", mod.title)
	end
	return editor
end

function WaitOpenModManager()
	if IsModManagerOpened() then return end
	SortModsList()
	local context = { dlcs = g_AvailableDlc or { } }
	local ged = OpenGedApp("ModManager", ModsList, context)
	if ged then ged:BindObj("log", ModMessageLog) end
	return ged
end

local origModEditorOpen = ModEditorOpen
function ModEditorOpen(mod)
	if mod then
		CreateRealTimeThread(OpenModEditor, mod)
	end
	return origModEditorOpen()
end

----
---
if Platform.debug then
	-- start GED always in release mode
	local origOpenGed = OpenGed
	function OpenGed(id, in_game)
		if in_game or not options.NoDebugForEditors then
			return origOpenGed(id, in_game)
		end
		local origGetExecName = GetExecName
		GetExecName = function ()
			local exe = origGetExecName()
			exe = string.remove(exe, "Debug")
			return exe
		end
		local ged = origOpenGed(id)
		GetExecName = origGetExecName
		return ged
	end
end

----

-- No version bump on save, only on uppload
local origGedOpSaveMod = GedOpSaveMod
function GedOpSaveMod(socket, root)
	local mod = root[1] or {}
	mod.SaveDef = function(self, ...)
		local version = self.version
		local err, code_dirty = ModDef.SaveDef(self, ...)
		self.version = version
		return err, code_dirty
	end
	sprocall(origGedOpSaveMod, socket, root)
	mod.SaveDef = nil
end

----

if FirstLoad then
	ModWorkingThread = false
	ModIgnoredQuestions = 0
end

local warnings_to_ignore = {
	[4164] = true, -- Mods are player created software packages...
}

local origWaitModsQuestion = WaitModsQuestion
function WaitModsQuestion(parent, caption, text, ...)
	local loc_id = TGetID(text) or 0
	if warnings_to_ignore[loc_id] then
		ModIgnoredQuestions = ModIgnoredQuestions + 1
		return "ok"
	end
	
	return origWaitModsQuestion(parent, caption, text, ...)
end

function ForceModReload()
	return ModsUIDialogEnd{
		DeleteThread = empty_func,
		CreateThread = function(self, name, callback, ...)
			local origWaitModsQuestion = WaitModsQuestion
			WaitModsQuestion = function() return "ok" end
			callback(...)
			WaitModsQuestion = origWaitModsQuestion
		end,
		SetMode = empty_func,
	}
end

function SetEnabledMods(mods, enable_only)
	if enable_only and not next(mods) then
		return
	end
	local loaded_list = GetEnabledMods()
	if table.iequal(loaded_list, mods) then
		return
	end
	if not IsRealTimeThread() then
		return CreateRealTimeThread(SetEnabledMods, mods, enable_only)
	end
	local thread = CurrentThread()
	if thread ~= ModWorkingThread and IsValidThread(ModWorkingThread) then
		print("Mod work in progress!")
		return
	end
	ModWorkingThread = thread

	local to_load = table.invert(mods)
	local loaded = table.invert(loaded_list)
	local enabled, disabled = {}, {}
	for _, mod in ipairs(ModsList) do
		local id = mod.id
		if not to_load[id] and loaded[id] then
			if not enable_only then
				ModSmartLogF(CurrentModDef, "Turning off mod %s '%s'", id, mod.title)
				TurnModOff(id)
				for _, ged in pairs(GedConnections) do
					if ged.app_template == "ModEditor" then
						local root = ged:ResolveObj("root")
						if root and root[1] == mod then
							ged:Close()
						end 
					end
				end
				enabled[#enabled + 1] = mod
			end
		elseif to_load[id] and not loaded[id] then
			ModSmartLogF(CurrentModDef, "Turning on mod %s '%s'", id, mod.title)
			TurnModOn(id)
			disabled[#disabled + 1] = mod
		end
	end
	if #enabled == 0 and #disabled == 0 then
		return
	end
	ObjModified(ModsList)
	ForceModReload()
	return enabled, disabled
end

function GetEnabledMods()
	return table.map(ModsLoaded, "id")
end

function GetModsToLoad()
	local tmp = g_InitialMods
	ModsUIDialogStart()
	local mods = g_InitialMods
	g_InitialMods = tmp
	return mods
end

-- auto enable mods and notify about updated ones
function CheckUpatedMods()
	local loaded = table.invert(GetEnabledMods())
	local to_enable
	local prev_versions = LocalStorage.ModVersions
	local versions = {}
	local logs = 0
	for id, mod in sorted_pairs(Mods) do
		local version = mod:GetVersionString()
		versions[id] = version
		if prev_versions and mod.packed then
			local prev_version = prev_versions[id]
			if prev_version then
				if prev_version ~= version then
					if logs == 0 then ModLogF("") end
					logs = logs + 1
					ModSmartLogF(CurrentModDef, "Mod '%s' (id %s) updated from %s to %s", mod.title, mod.id, prev_version, version)
				end
			elseif not loaded[id] then
				to_enable = table.create_add(to_enable, id)
				if logs == 0 then ModLogF("") end
				logs = logs + 1
				ModSmartLogF(CurrentModDef, "New mod found '%s' (id %s)", mod.title, mod.id)
			end
		end
	end
	if logs > 0 then ModLogF("") end
	if not prev_versions or not table.equal_values(versions, prev_versions) then
		print("SaveLocalStorageDelayed")
		LocalStorage.ModVersions = versions
		SaveLocalStorageDelayed()
	end
	return to_enable
end

if FirstLoad and not DataLoaded then
	local _, to_enable = procall(CheckUpatedMods)
	function OnMsg.ModsReloaded()
		if to_enable and options.AutoEnableNewMods then
			CreateRealTimeThread(SetEnabledMods, to_enable, true)
		end
		to_enable = false
	end
end

function OnMsg.ModUnloadLua(id)
	if id == CurrentModId then
		LocalStorage.ModVersions = nil
		SaveLocalStorageDelayed()
	end
end

----

local origModsUIToggleEnabled = ModsUIToggleEnabled
function ModsUIToggleEnabled(mod, win, obj_table, ...)
	mod = mod or g_ModsUIContextObj:GetSelectedMod(obj_table)
	local dependency = mod and ModDependencyGraph[mod.ModID]
	if dependency and options.AutoEnableModDependency and not g_ModsUIContextObj.enabled[mod.ModID] then
		if mod.Warning_id == "dependencies_disabled" then
			mod.Warning_id = nil
		end
		for _, dep in ipairs(dependency.outgoing) do
			if Mods[dep.id] then
				g_ModsUIContextObj.enabled[dep.id] = true
				TurnModOn(dep.id)
				ObjModified(dep)
			end
		end
	end
	return origModsUIToggleEnabled(mod, win, obj_table, ...)
end

----

-- Fixed deleting the current thread:
local origInitializeWarningsForGedEditor = InitializeWarningsForGedEditor
function InitializeWarningsForGedEditor(...)
	local thread = DiagnosticMessageActivateGedThread
	if not IsValidThread(thread) then
		return origInitializeWarningsForGedEditor(...)
	end
	DiagnosticMessageActivateGedThread = false
	origInitializeWarningsForGedEditor(...)
	CreateRealTimeThread(DeleteThread, thread)
end

----

-- Undo includes dont-save props
function GedPropCapture(obj)
	if not IsKindOf(obj, "PropertyObject") then
		return
	end
	local prop_eval = prop_eval
	local prop_capture = {}
	for _, prop_meta in ipairs(obj:GetProperties()) do
		local no_edit = prop_eval(prop_meta.no_edit, obj, prop_meta)
		local read_only = prop_eval(prop_meta.read_only, obj, prop_meta)
		if not no_edit and not read_only then
			local editor = prop_meta.editor
			if editor == "object" or editor == "objects" then -- can't really undo nested game objects that get changed via a Ged button
				return
			end
			
			local id = prop_meta.id
			local value = obj:GetProperty(id)
			if not obj:IsDefaultPropertyValue(id, prop_meta, value) then
				prop_capture[id] = obj:ClonePropertyValue(value, prop_meta)
			end
		end
	end
	return prop_capture
end

