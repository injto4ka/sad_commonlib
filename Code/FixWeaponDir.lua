UnitCombat.weapon_dummy = false

function UnitCombat:GetWeaponDymmy(weapon_obj, target_pos)
	local dummy = self.weapon_dummy
	if not IsValid(dummy) or dummy.class ~= weapon_obj.class then
		DoneObject(dummy)
		dummy = PlaceObject(weapon_obj.class)
		dummy:SetVisible(false)
		dummy:SetOpacity(0)
		self.weapon_dummy = dummy
	end
	dummy.fx_actor_class = weapon_obj.fx_actor_class
	dummy:ChangeEntity(weapon_obj:GetEntity())
	dummy:SetMirrored(weapon_obj:GetMirrored())
	dummy:SetScale(weapon_obj:GetVisualScale())
	dummy:SetPos(weapon_obj:GetVisualPos())
	dummy:Face3D(target_pos)
	dummy:SetVisible(true)
	return dummy
end

UnitCombat.OrigOnTargetAttacked = UnitCombat.OnTargetAttacked

function UnitCombat:OnTargetAttacked(moment, weapon_def, weapon_obj, target, target_pos, hit_chance, parabola_angle, ...)
	if (weapon_obj or self) == self or weapon_def.DamageCone == 0 or (parabola_angle or 0) ~= 0 then
		return self:OrigOnTargetAttacked(moment, weapon_def, weapon_obj, target, target_pos, hit_chance, parabola_angle, ...)
	end
	local restore_spot
	if not weapon_def.ProjectileSpot then
		weapon_def.ProjectileSpot = "Origin"
		restore_spot = true
	end
	local restore_range = weapon_def.MinAttackRange
	if restore_range ~= 0 then
		weapon_def.MinAttackRange = 0
	end
	local weapon_dummy = self:GetWeaponDymmy(weapon_obj, target_pos)
	self:OrigOnTargetAttacked(moment, weapon_def, weapon_dummy, target, target_pos, hit_chance, parabola_angle, ...)
	weapon_dummy:SetVisible(false)
	if restore_spot then
		weapon_def.ProjectileSpot = nil
	end
	if restore_range ~= 0 then
		weapon_def.MinAttackRange = restore_range
	end
end

local UnitCombat_Done = UnitCombat.Done
function UnitCombat:Done()
	DoneObject(self.weapon_dummy)
	self.weapon_dummy = nil
	return UnitCombat_Done(self)
end

SetPropMeta("WeaponResource", "ProjectileSpot", "no_edit", nil, CurrentModDef)