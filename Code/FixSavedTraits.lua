-- Fixed in game version 1.22.240321
if LuaRevision >= 352127 then
	return
end
function SavegameFixups.RenameBreakdownThreshold2()
	local traits = {"Liberated", "CognitiveStabilizers"}
	MapForEach(true, "Human", function(unit)
		unit.StressedThreshold = nil
		unit.BreakdownThreshold = nil
		for _, trait in ipairs(traits) do
			if unit:HasTrait(trait) then
				-- reset trait
				unit:SetTrait(trait, false)
				unit:SetTrait(trait, true, "forced")
				unit:InterruptBreakdown()
			end
		end
	end)
end