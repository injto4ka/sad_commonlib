local enum_radius = const.SlabSizeX
local enum_flags = const.efVisible | const.efBuilding | const.efSelectable
local game_flags = const.gofPermanent
local game_flags_ignore = const.gofSolidShadow
local surf_flags = EntitySurfaces.Selection | EntitySurfaces.Collision
local IsValidPos = IsValidPos
local IntersectSegmentWithClosestObj = IntersectSegmentWithClosestObj
local GetTerrainGamepadCursor = GetTerrainGamepadCursor
local GetTerrainCursor = GetTerrainCursor
local camera_GetEye = camera.GetEye
local IsCloser = IsCloser
local GetPreciseSelection = GetPreciseSelection
local GetUIStyleGamepad = GetUIStyleGamepad
local next = next
local IsValid = IsValid
local PlaceObject = PlaceObject
local FillPathMeshPstr = FillPathMeshPstr
local pstr = pstr
local iclear = table.iclear
local DoneObject = DoneObject
local DoneObjects = DoneObjects

function GetPreciseSelectionWithSelectionSurfaces()
	local eye = camera_GetEye()
	if not IsValidPos(eye) then return end
	local lookat = GetUIStyleGamepad() and GetTerrainGamepadCursor() or GetTerrainCursor()
	if not IsValidPos(lookat) then return end
	local sobj, spos = IntersectSegmentWithClosestObj(eye, lookat, "CObject", enum_radius, enum_flags, game_flags, 0, game_flags_ignore, surf_flags)
	local pobj = GetPreciseSelection()
	if not sobj or not pobj then
		return sobj or pobj
	end
	local ppos = pobj:IntersectSegment(eye, lookat) or pobj:GetBSphere()
	return IsCloser(eye, spos, ppos) and sobj or pobj
end

----
-- remove some memory allocs

local width          = 25*guic
local z_offset       = 10*guic
local turn_step      = 10*60
local turn_step_size = 5*guic

function InplaceUnitPath(path, color, line)
	if next(path) then
		if not IsValid(line) then
			line = PlaceObject("Mesh")
			line:SetShader(ProceduralMeshShaders.soft_mesh)
			line:SetDepthTest(false)
			line:SetMeshFlags(const.mfWorldSpace)
			line:SetUniforms(
				const.Human.DepthFadeoutDistance or -guim/3
			)
		end
		local mesh_pstr = line.vertices_pstr_prev
		line.vertices_pstr_prev = line.vertices_pstr
		mesh_pstr = mesh_pstr or pstr("", 1024, const.pstrfSaveEmpty | const.pstrfBinary)
		mesh_pstr:clear()
		FillPathMeshPstr(mesh_pstr, point30, path, color, width, z_offset, turn_step, turn_step_size)
		line:SetMesh(mesh_pstr)
		line:SetPos(path[1])
	end
	return line
end

Unit.ShowAdditionalPaths = empty_func
local PathColor = SetA(cyan, 128)
function Unit:ShowPath()
	if not self:ShouldShowPath() then
		self:ClearPathDisplay()
		return
	end
	
	local line
	local path = GetVisualPathPoints(self)
	if not next(path) then
		DoneObject(self.path_line)
	else
		line = InplaceUnitPath(path, PathColor, self.path_line)
	end
	self.path_line = line
	
	local line_and_numbers = self.additional_path_line or {}
	self.additional_path_line = line_and_numbers
	DoneObjects(line_and_numbers)
	iclear(line_and_numbers)
	self:GatherAdditionalPathLines(line_and_numbers)
	
	ShownPaths[self] = (line or next(line_and_numbers)) and true or nil 
end

function Unit:UpdatePathDisplay()
	if not self:ShouldShowPath() then
		self:ClearPathDisplay()
		return
	end
	local list = UpdatePathDisplayList
	if not list then
		list = {}
		UpdatePathDisplayList = list
	elseif list[self] then
		return
	end
	list[self] = true
	list[#list + 1] = self
	if IsValidThread(UpdatePathDisplayThread) then
		Wakeup(UpdatePathDisplayThread)
		return
	end
	UpdatePathDisplayThread = CreateMapRealTimeThread(function()
		local clear = table.clear
		local WaitWakeup = WaitWakeup
		local ipairs = ipairs
		while true do
			local list = UpdatePathDisplayList
			for _, obj in ipairs(list) do
				obj:ShowPath()
			end
			clear(list, true)
			WaitWakeup()
		end
	end)
end

local lead_path_line
local path_line_thread
local LeadAnimalPathColor = RGBA(214, 76, 150, 200)

function EnterLeadAnimalUIMode(animal)
	local function GetLeadPos(animal)
		return terrain.FindPassable(GetCursorPos(GetUIStyleGamepad()), animal.pfclass_tamed_lead, -1, animal)
	end
	DeleteThread(path_line_thread)
	path_line_thread = CreateRealTimeThread(function(animal)
		local cursor_pos, animal_pos
		while IsValid(animal) do
			local new_cursor_pos = GetLeadPos(animal)
			local new_animal_pos = GetPosHash(animal)
			if new_cursor_pos and (new_cursor_pos ~= cursor_pos or new_animal_pos ~= animal_pos) then
				cursor_pos, animal_pos = new_cursor_pos, new_animal_pos
				local restrict_area = animal:GetLeadRestrictArea(cursor_pos)
				local reachable, obj = animal:CanReach(cursor_pos, restrict_area, nil, animal.pfclass_tamed_lead)
				local path = GetVisualPathPoints(obj)
				lead_path_line = InplaceUnitPath(path, LeadAnimalPathColor, lead_path_line)
			end
			Sleep(50)
		end
	end, animal)
	EnterTargetPosMode(function()
		NetSyncEvent("ObjFunc", animal, "rfnToggleLeadAnimalActivity", UIPlayer, GetLeadPos(animal))
	end, T(881252027367, "Lead to"))
end

function OnMsg.IGIModeChanged(mode)
	if mode ~= "IModePickTargetPos" and path_line_thread then
		DeleteThread(path_line_thread)
		path_line_thread = nil
		DoneObject(lead_path_line)
		lead_path_line = nil
	end
end

function IModePickTargetPos:OnMousePos(pt, ...)
	local func = self.context and self.context.OnMousePos
	if func then
		func(pt)
	end
	return InterfaceModeDialog.OnMousePos(self, pt, ...)
end

IModePickTargetPos.ZOrder = -1000