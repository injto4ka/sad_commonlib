if not Libs.Dev then
	FunctionProfilerStop = empty_func
	FunctionProfilerStart = empty_func
	OS_LocateFile = empty_func
	OS_OpenFile = empty_func
	bp = empty_func
	GetEntityFiles = function() return {} end
	SaveCollapsedPresetGroups = empty_func
	LoadCollapsedPresetGroups = empty_func
end
ModDef.SortItems = empty_func

 -- avoid computing debug map hash for saved games
TerrainHashPassability = terrain.HashPassability
terrain.HashPassability = empty_func

if not config.EnableHaerald then
	StopDebugger = empty_func
end

if FirstLoad then
	RecipeTweaks = RawGetGlobal("RecipeTweaks") or {}
	shallowcopy = RawGetGlobal("shallowcopy") or empty_func
	DefineModMapData = RawGetGlobal("DefineModMapData") or empty_func
end

--[[ uncomment to allow debugging property problems
UpdateDiagnosticMessage = empty_func
GetDiagnosticMessage = empty_func
--]]

CObject.GetAllRequests = empty_func
CObject.OnObjUpdate = empty_func

UnpersistedMissingClass.CanBeDamaged = empty_func

-- Stubs for missing DLC:
TryDefineClass("ActivityInvaderRobotBase", "UnitInvader")
TryDefineClass("HumanoidRobot", "ActivityInvaderRobotBase")
TryDefineClass("HumanoidRobotStation", "RobotStationBase")