local GameTime = GameTime
local GetSunAltAzi = GetSunAltAzi
local Clamp = Clamp
local origObjModified = ObjModified

SetPropMeta("Lightmodel", "lightning_enable", "blend", 100, CurrentModDef)
SetPropMeta("Lightmodel", "rain_enable", "blend", 100, CurrentModDef)
SetPropMeta("Lightmodel", "snow_enable", "blend", 100, CurrentModDef)
SetPropMeta("Lightmodel", "stars_pole_alt", "min", -180*60, CurrentModDef)

local blend_props = {}
function OnMsg.ClassesBuilt()
	local preset = LightmodelPreset
	for _, prop_meta in ipairs(preset:GetProperties()) do
		if prop_meta.category ~= "Preset"
		and not prop_eval(prop_meta.no_edit, preset, prop_meta)
		and not prop_eval(prop_meta.read_only, preset, prop_meta)
		then
			blend_props[#blend_props + 1] = prop_meta
		end
	end
end

function BlendLightmodels(result, lm1, lm2, num, denom, props)
	props = props or blend_props
	local firstHalf = 2*num < denom
	local type = type
	local InterpolateRGB = InterpolateRGB
	local Lerp = Lerp
	local GetProperty = result.GetProperty
	local SetProperty = result.SetProperty
	
	--[[
	result.id = "{ [1] " .. (lm1.id or "?") .. " >> " .. 100 * num / denom .. "% of " .. denom .. ") >> [2] " .. (lm2.id or "?") .. "}"
	print("BlendLightmodels", result.id)
	--]]
	
	result.id = (firstHalf or not lm2.id) and lm1.id or lm2.id
	for _, prop_meta in ipairs(props) do
		local prop_id = prop_meta.id
		local v1 = GetProperty(lm1, prop_id)
		local v2 = GetProperty(lm2, prop_id)
		local value
		local prop_blend = prop_meta.blend
		if num >= denom or v1 == v2 or prop_blend == "set" then
			value = v2
		elseif num <= 0 or prop_blend == "suppress" then
			value = v1
		else
			value = v2
			local prop_editor = prop_meta.editor
			if prop_editor == "number" or prop_editor == "point" then
				value = Lerp(v1, v2, num, denom)
			elseif prop_editor == "color" then
				value = InterpolateRGB(v1, v2, num, denom)
			elseif type(prop_blend) == "number" and prop_blend ~= 50 then
				-- "blend" here is a number indicating the descrete value change threshold
				if prop_editor == "bool" then
					value = v1 and 100 * num < (100 - prop_blend) * denom or v2 and 100 * num >= prop_blend * denom
				elseif 100 * num < prop_blend * denom then
					value = v1
				end
			elseif firstHalf then
				value = v1
			end
		end
		SetProperty(result, prop_id, value)
	end
end

MapVar("BlendedLightmodel", false)
MapVar("BlendedForcedLightmodel", false)

function UpdateLightModel(dt)
	dt = dt or 0
	local sim_time = (ForceTimeOfDay or GameTime()) + dt
	local forced_light_models = ForcedLightModels
	if #forced_light_models > 0 then 
		local last_forced_lm_group = forced_light_models[#forced_light_models]
		last_forced_lm_group.time = Clamp(last_forced_lm_group.time + dt, 0, last_forced_lm_group.total_time)
		if last_forced_lm_group.time == last_forced_lm_group.total_time then
			table.clear(forced_light_models)
			if last_forced_lm_group.group then
				forced_light_models[1] = last_forced_lm_group
			end
		end
	end
	
	origObjModified = origObjModified or ObjModified
	ObjModified = empty_func
	
	local BlendLightModelGroup = BlendLightModelGroup
	local lm_current = BlendedLightmodel or Lightmodel:new()
	local blended_forced_lm = BlendedForcedLightmodel or Lightmodel:new()
	local light_model_group = LastLightModelGroup or SelectLMGroup()
	
	local alt, azi = GetSunAltAzi(sim_time)
	NetUpdateHashNoStack("SunPos", alt, azi)
	BlendLightModelGroup(lm_current, light_model_group, alt, azi, sim_time)
	for _, forced_lm in ipairs(forced_light_models) do
		BlendLightModelGroup(blended_forced_lm, forced_lm.group or light_model_group, alt, azi, sim_time)
		BlendLightmodels(lm_current, lm_current, blended_forced_lm, forced_lm.time, forced_lm.total_time)
	end
	
	ObjModified = origObjModified
	LastLightModelGroup = light_model_group
	BlendedLightmodel = lm_current
	BlendedForcedLightmodel = blended_forced_lm
	
	Msg("UpdateLightModel", lm_current, dt)
	SetLightmodel(1, lm_current, dt)
end