local iclear = table.iclear
local IsValidThread = IsValidThread
local Wakeup = Wakeup
local next = next
local pairs = pairs
local ipairs = ipairs
local type = type
local select = select
local IsValid = IsValid
local g_Classes = g_Classes
local procall = procall
local Min = Min

local options = CurrentModOptions or empty_table

function OnMsg.PersistGatherPermanents(permanents, direction)
	permanents["__next__"] = next
	permanents["__rawget__"] = rawget
	permanents["table.iclear"] = table.iclear
	permanents["cthread.IsValidThread"] = IsValidThread
	permanents["cthread.Wakeup"] = Wakeup
	permanents["__select__"] = select
	permanents["__procall__"] = procall
end

MessageText["Path Not Found"] = T(950959678764, "File not found.")

----

const.DamageTypeNegationHidden.energy = false

----

-- Fix CombatQuadcopters weapon property preset group
OnMsg.ClassesGenerate = function(classdefs)
	local CombatQuadcopterBase = classdefs["CombatQuadcopterBase"]
	if CombatQuadcopterBase then
		local prop = table.find_value(CombatQuadcopterBase.properties, "preset_group", "Weapons_Robot")
		if prop then
			prop.preset_group = "Weapons_Robots"
		end
	end
end

----
-- Fix missing duration in the rollover of health condition effects
local origGetEffectsDescription = HealthCondition.GetEffectsDescription
function HealthCondition:GetEffectsDescription(obj)
	local dscr = origGetEffectsDescription(self, obj)
	if self.Expiration then
		self.__expiration_time = 0
		obj:ForEachEffectByClass("HealthCondition", function(effect, self)
			if effect.id == self.id and effect.expiration_time then
				self.__expiration_time = Max(self.__expiration_time, effect.expiration_time)
			end
		end, self)
		if self.__expiration_time >= GameTime() then
			local tmp = { dscr, T{976879128263, "<newline>Time remaining<right><time_icon><em><duration(time_left)></em>",
				time_left = self.__expiration_time - GameTime(),
			}}
			dscr = table.concat(tmp, "\n")
		end
	end
	return dscr
end

SetPropMeta("HealthCondition", "HealerTags", "default", false, CurrentModDef)

----

function UnitHealth:GetStatusEffectsAffectingBodyPartGrouped(body_part)
	local HealthConditions = HealthConditions
	local counters = {}
	local effects = {}
	self:ForEachEffectByClass("HealthCondition", function(status_effect, body_part, counters)
		if status_effect.body_part == body_part then
			local id = status_effect.id
			counters[id] = (counters[id] or 0) + 1
			effects[id] = effects[id] or HealthConditions[id] or status_effect
		end
	end, body_part, counters)
	local setmetatable = setmetatable
	local is_android = self:HasUnitTag("Android")
	local entries = {}
	for id, count in pairs(counters) do
		local status_effect = effects[id]
		local text
		local display_name
		if is_android and (status_effect.DisplayNameAndroid or "") ~= "" then
			display_name = status_effect.DisplayNameAndroid
		else
			display_name = status_effect.DisplayName
		end
		if count > 1 then
			text = T{813947885121, "<name> x<count>", name = display_name, count = count }
		else
			text = display_name
		end
		local entry = {
			count = count,
			body_part = body_part,
			DisplayText = text,
		}
		entry = setmetatable(entry, {
			__index = status_effect
		})
		entries[#entries + 1] = entry
	end
	table.sortby_field(entries, "id")
	return entries
end

----

-- Include additional savegame information in the log files in order to make possible associating a log file with a specific savegame
function OnMsg.GameMetadataLoaded(meta)
	local info = {
		"\n\tDisplay name  ", meta.displayname or "",
		"\n\tFile name     ", meta.savename or "",
		"\n\tScenario      ", meta.scenario or "",
		"\n\tRegion        ", meta.region or "",
		"\n\tMap Seed      ", meta.map_seed or "",
		"\n\tPlaytime      ", string_format("%d:%02d:%02d", FormatElapsedTime(meta.playtime or 0, "hms")),
	}
	DebugPrint("Additional savegame information:", table.concat(info), "\n")
end

----

local origGatherCannotUpgradeReasons = Building.GatherCannotUpgradeReasons
function Building:GatherCannotUpgradeReasons(upgrade_target, reasons)
	if upgrade_target.SupportStrengthRequirement and not next(self.supported_by) then
		table.insert(reasons, T(959997424319, "Destroyed support"))
		return
	end
	return origGatherCannotUpgradeReasons(self, upgrade_target, reasons)
end

----

function MatchTags(tags_to_check, filter_tags, no_filter_default)
	if not filter_tags or not next(filter_tags) then
		return no_filter_default
	end
	if not tags_to_check or not next(tags_to_check) then
		return
	end
	local found_set, found_any
	for tag, set in pairs(filter_tags) do
		found_any = found_any or set
		if tags_to_check[tag] then
			if set then
				found_set = true
			else
				return
			end
		end
	end
	return not found_any or found_set
end

local MatchTags = MatchTags

function MatchGameScenarioTags(tags)
	local scenario = Scenario or GetGameScenario()
	return MatchTags(scenario and scenario.ScenarioTags, tags, true)
end

SetPropMeta("MentalBreakdown", "UnitTags", "three_state", true, CurrentModDef)
SetPropMeta("RelaxationRoutine", "UnitTags", "three_state", true, CurrentModDef)
SetPropMeta("HappinessFactor", "UnitTags", "three_state", true, CurrentModDef)
SetPropMeta("HealthCondition", "UnitTags", "three_state", true, CurrentModDef)

AutoResolveMethods.IsAdult = "and"
UnitSocial.IsAdult = return_true
UnitEnergy.IsAdult = return_true

function Human:IsAdult()
	return self.Age >= 18
end

local origIsResAccepted = UnitEnergy.IsResAccepted

function UnitEnergy:IsResAccepted(res, ...)
	if (res or "") ~= "" and not self:IsAdult() then
		local Resources = Resources
		local res_def = res
		if type(res_def) == "string" then
			res_def = Resources[res_def]
		end
		if not res_def then
			assert(false, "No such resource definition: " .. tostring(res))
		elseif res_def.adult_only then
			return
		elseif not res_def.is_group then
			for _, group in ipairs(res_def.in_groups) do
				local group_def = Resources[group]
				if group_def and group_def.adult_only then
					return
				end
			end
		end
	end
	return origIsResAccepted(self, res, ...)
end

AppendClass.Resource = {
	properties = {
		{ category = "Consumable", id = "adult_only", name = "Adult Only", editor = "bool", default = false, },
	},
}
			
----
-- Fixup for hidden unlocked resources
function OnMsg.LoadGame()
	local Resources = Resources
	for _, player in ipairs(Players) do
		for res in pairs(player.unlocked_resources) do
			local def = Resources[res]
			local state = GetPresetLockStateAndText(def, player)
			if state == "hidden" then
				RemovePresetLockStateReason(def.class, def.group, def.id, "hidden", false, player)
				state = GetPresetLockStateAndText(def, player)
				if state ~= "hidden" then
					ModSmartLogF(CurrentModDef, "Fixed wrong hidden state of resource '%s'", res)
				end
			end
		end
	end
end

----

function FuncPieces:EntitySpecInheritItems()
	if not table.get(EntitySpecPresets, "HumanMale") then
		return GetAllEntitiesComboItems()
	end
	local items = {}
	for entity, preset in pairs(EntitySpecPresets) do
		if preset.can_be_inherited then
			items[#items + 1] = entity
		end
	end
	table.sort(items)
	table.insert(items, 1, "")
	return items
end

SetPropMeta("EntitySpecProperties", "inherit_entity", "editor", "choice", CurrentModDef)
SetPropMeta("EntitySpecProperties", "inherit_entity", "items", FuncPieces.EntitySpecInheritItems, CurrentModDef)
SetPropMeta("SpawnObject", "EventProgressValue", "max", nil, CurrentModDef)

----

-- Fixed disappearing ceiling lamps after being moved

DefineClass("CeilingLampFixes")
AppendClass.CeilingLampBuilding = {
	__parents = { "CeilingLampFixes" }
}

local LampsToUpdate
function CeilingLampFixes:OnMoved()
	if not LampsToUpdate then
		if not GetOverlay("RoomsOverlay") then
			return
		end
		CreateRealTimeThread(function()
			for obj in pairs(LampsToUpdate) do
				local visibility_level = table.get(obj.room, "visibility_level")
				if visibility_level and visibility_level <= RoomVisibilityLevel then
					obj:HideRoomObj(false, 0, CeilingHideReason)
				end
			end
			LampsToUpdate = false
		end)
		LampsToUpdate = {}
	end
	LampsToUpdate[self] = true
end

----

local prop = table.find_value(RegionDef.properties, "id", "Temperature")
table.set(prop, "prop_meta", "scale", "C")

----

SetPropMeta("XAction", "BindingsMenuCategory", "editor", "preset_id", CurrentModDef)
SetPropMeta("XAction", "BindingsMenuCategory", "preset_class", "BindingsMenuCategory", CurrentModDef)
SetPropMeta("XAction", "BindingsMenuCategory", "name", "Bindings Category", CurrentModDef)

----

local UnitEquipmentEquip = UnitEquipment.Equip
function UnitEquipment:Equip(equipment_id, ...)
	local amounts = self.res_amounts
	if not amounts or not amounts[equipment_id] then
		assert(false, string_format("Trying to equip '%s' to '%s'", equipment_id, self.class))
		return false
	end
	return UnitEquipmentEquip(self, equipment_id, ...)
end

----

SetPropMeta("CharacterDef", "ActorFXClass", "editor", "combo", CurrentModDef)
SetPropMeta("CharacterDef", "ActorFXClass", "items", ActorFXClassCombo, CurrentModDef)
SetPropMeta("Robot", "ActorFXClass", "editor", "combo", CurrentModDef)
SetPropMeta("Robot", "ActorFXClass", "items", ActorFXClassCombo, CurrentModDef)

function OnMsg.GatherFXActors(list)
	for _, def in ipairs{"CharacterDef", "RobotCompositeDef"} do
		ForEachPreset(def, function(preset, group, list)
			local class = preset.ActorFXClass or ""
			if class ~= "" then
				list[#list + 1] = class
			end
		end, list)
	end
end
	
----

function CleanupInvalidObjects()
	local st = GetPreciseTicks()
	local objs = table.invert(__cobjectToCObject)
	MapForEach(true, function(obj)
		objs[obj] = nil
	end)
	local count = 0
	for lua_obj, C_obj in pairs(objs) do
		print("Invalid object deleted:", ValueToStr(lua_obj))
		pcall(lua_obj.delete, lua_obj, true)
		__cobjectToCObject[C_obj] = nil
		table.clear(lua_obj)
		count = count + 1
	end
	print(count, "invalid objects removed in", GetPreciseTicks() - st, "ms")
end

-- Potential fix for "Trying to persist an invalid object" error
local origPersistGame = PersistGame
function PersistGame(...)
	local err = origPersistGame(...)
	if err == "Trying to persist an invalid object" then
		pcall(CleanupInvalidObjects)
		err = origPersistGame(...)
		if err ~= "Trying to persist an invalid object" then
			print("A savegame error has been detected and fixed! Please report this game session.")
		end
	end
	return err
end

----

function FuncPieces:ShowProgressInDebug()
	return Platform.cheats or Platform.developer or Platform.debug
end

UIChangeControl("GameShortcuts", "actionConstructionChangeSnapping", 'ActionBindable', true, CurrentModDef)
UIChangeControl("InGameMenuMain", { comment = "progress debug"}, '__condition', FuncPieces.ShowProgressInDebug, CurrentModDef)
UIChangeControl("IPActionsFields", "idExtendField", 'RolloverHint', Untranslated("<em><ShortcutName('actionExtendField')></em>"), CurrentModDef)
UIChangeControl("IPActionsFields", "idReduceField", 'RolloverHint', Untranslated("<em><ShortcutName('actionReduceField')></em>"), CurrentModDef)

local version_str = TTranslate(T(423675131193, "Version <u(ver)><opt(build, ' - ', '')>"), { ver = BuildVersion or LuaRevision, build = rawget(_G, "BuildName") })
version_str = version_str .. "\nCommon Lib " .. CurrentModDef:GetVersionString() 
local match = {
	__class = "XText",
	HAlign = "right",
	VAlign = "bottom",
	DrawOnTop = true,
	HandleMouse = false,
	TextStyle = "ConsoleLog",
	Translate = true,
}
UIChangeControl("InGameMenu", match, 'Text', Untranslated(version_str), CurrentModDef)
UIChangeControl("PreGameMenu", match, 'Text', Untranslated(version_str), CurrentModDef)

----

function Movable:OnTunnelMissingFallback()
	self:ClearPath()
	if self:HasState("idle") then
		self:SetState("idle") -- avoid walking in place
	end
	Sleep(100) -- avoid infinite loop
end

----

AppendClass.CompositeBodyPart = {
	__parents = { "ComponentCustomData" },
	CustomDataType = "outline",
}

Building.CustomDataType = "outline"
Unit.CustomDataType = "outline"
MedicalBed.CustomDataType = "outline+animated"
AnimatedTextureBuilding.CustomDataType = "outline+animated"

local idx = table.find(CompositeBodyPreset.properties, "id", "Colors") or #CompositeBodyPreset.properties
table.insert(CompositeBodyPreset.properties, idx + 1, { id = "ColorModifier", name = "Color Modifier", editor = "rgbrm", default = RGB(100, 100, 100), no_edit = function(self) return self.ColorInherit ~= "" end })

local origColorizeBodyPart = CompositeBody.ColorizeBodyPart
function CompositeBody:ColorizeBodyPart(part, preset, ...)
	local inherit_from = preset.ColorInherit
	local inherit_part = inherit_from ~= "" and table.get(self.attached_parts, inherit_from)
	local modifier = inherit_part and inherit_part:GetColorModifier() or preset.ColorModifier
	part:SetColorModifier(modifier)
	return origColorizeBodyPart(self, part, preset, ...)
end

CompositeBody.composite_part_spots = {}
QuadcopterBase.composite_part_spots = {
	Carry = "Carrying",
}

----

function MineableRock:Mine(unit, skill_level, efficiency)
	local params = { delay_distribution = true, jump_from = unit }
	local modifiers = unit and unit:CallReactions_Modify("GatherSiteLootModifiers", nil, self.class, "Mining")

	local resource = self.MineResource
	local modifier = modifiers and modifiers[resource] or 1000
	local amount = MulDivRound(self.MineAmount, modifier, 1000)
	ProduceResource(unit, self, resource, amount, nil, params)

	local resource2 = self.SecondaryResource
	if resource2 and self.SecondaryResourceAmount > 0 then
		local chance = self.SecondaryResourceChance + (unit.RockProspectingChance or 0)
		if chance > 0 and InteractionRand(100, "drop_secondary_resource", self) < chance then
			local modifier2 = modifiers and modifiers[resource2] or 1000
			local amount2 = MulDivRound(self.SecondaryResourceAmount, modifier2, 1000)
			ProduceResource(unit, self, resource2, amount2, nil, params)
		end
	 end

	CreateGameTimeThread(function (self)
		self:SetOpacity(0, 1000)
		Sleep(1000)
		DoneObject(self)
 	end, self)
end

-- Code to add GatherSiteLootModifiers to Butchering from Powermogri
function UnitCorpse:ProduceButcherResources(unit, skill_level, efficiency)
	local work_action = GetWorkActionInstance(self, "Butcher", unit.player)
	local params = { delay_distribution = true, jump_from = unit }
	local groups = GroupResourceIds
	local modifiers = unit and unit:CallReactions_Modify("GatherSiteLootModifiers", nil, self.class, "Butchering")
	
	for _, res_amount in ipairs(self:GetButcherResources()) do
		local resource = res_amount.resource
		local amount = res_amount.amount
		local modifier = modifiers and modifiers[resource] or 1000
		local group = groups[resource]
		if group then
			resource = group[1]
		end
		amount = self:ScaleByDecay(amount)
		amount = MulDivRound(amount, modifier, 1000)
		amount = RoundResourceAmount(amount)
		local amount_placed, used_piles = ProduceResource(unit, self, resource, amount, nil, params)
		if work_action then
			AttachDeliveryObjectsToPiles(used_piles, unit, work_action.activity_id)
		end
	end
end

----

local origResCanBeDismantled = ResCanBeDismantled
ResCanBeDismantled = return_true

local origGetDismantleResourcesText = GetDismantleResourcesText
function GetDismantleResourcesText(res, amount, res_info)
	local text = origGetDismantleResourcesText(res, amount, res_info)
	if text == "" then
		text = T(776057477288, "Nothing")
	end
	return text
end

function ResourcePile:CanBeDismantled()
	local def = self:GetIPResourceDef()
	if not def or def:IsHumanEdible() then return end
	return next(def.DismantleResources) or (def.Uses or 0) > 0 and next(def.DismantleResourcesPerUseLeft)
end

function ResourcePile:GetDismantleActionText()
	if self:CanBeDismantled() then
		return T(746563020144, "Dismantle")
	end
	return T(890121443768, "Throw away")
end

function ResourcePile:GetDismantleActionIcon(dismantling)
	if self:CanBeDismantled() then
		return dismantling and "UI/Icons/Infopanels/common_dismantle_cancel" or "UI/Icons/Infopanels/common_dismantle"
	end
	return dismantling and "UI/Icons/Infopanels/common_throw_away_cancel" or "UI/Icons/Infopanels/common_throw_away"
end

----

local origTryLaunchFromExplosion = UnitFall.TryLaunchFromExplosion
function UnitFall:TryLaunchFromExplosion(...)
	if not self:CheckPassable() then
		return
	end
	return origTryLaunchFromExplosion(self, ...)
end

----

SetPropMeta("Recipe", "Unfinished", "no_edit", true, CurrentModDef)

----

local AppendRequiredObjClasses = {
	CheckIsConscious = "Unit",
	CheckFurniture = "Room",
	CheckHappinessFactor = "UnitHappiness",
	CheckHealthCondition = "UnitHealth",
	CheckIsAlive = "DamageableObject",
	CheckIsAndroid = "Unit",
	CheckIsAwake = "Unit",
	CheckIsOnExpedition = "Unit",
	CheckOpinion = "UnitSocial",
	CheckInRelationship = "UnitSocial",
	CheckSkillInclination = "UnitSkill",
	CheckSkillLevel = "UnitSkill",
	CheckTrait = "Unit",
	CheckUnitTag = "Unit",
}

for class, req_class in pairs(AppendRequiredObjClasses) do
	local classdef = _G[class]
	if classdef then
		local list = classdef.RequiredObjClasses or {}
		table.insert_unique(list, req_class)
		classdef.RequiredObjClasses = list
	end
end

Unit.IsOnExpedition = empty_func
Unit.HasTrait = empty_func
Unit.IsAwake = return_true
AutoResolveMethods.IsAwake = "and"

function FunctionObject:GetRequiredClassesFormatted()
	if not self.RequiredObjClasses then return end
	local classes = {}
	for _, id in ipairs(self.RequiredObjClasses) do
		classes[#classes + 1] = id
	end
	table.sort(classes)
	return Untranslated("(" .. table.concat(classes, ", ") .. ")")
end

----

function BuildingCompositeDef.OnLockStateChanged(group, preset, old_state, state, player)
	if GameTime() ~= 0 and old_state == "hidden" then
		local locked = LockedBuildMenuCategories[player] or {}
		LockedBuildMenuCategories[player] = locked
		local category = preset.BuildMenuCategory or ""
		while category ~= "" do
			locked[category] = false
			category = table.get(BuildMenuCategories, category, "BuildMenuCategory") or ""
		end
		AddNewlyUnlockedPreset(preset, player)
	end
	if player == UIPlayer then
		RefreshBuildMenu()
	end
end

----

local origGetEntryRolloverDescription = BuildMenu.GetEntryRolloverDescription
function BuildMenu:GetEntryRolloverDescription(entry, ...)
	if entry.class ~= "BuildingCompositeDef" and entry.class ~= "ModItemBuildingCompositeDef" then
		return entry:GetProperty("menu_description") or entry:GetProperty("description") or Untranslated("Missing description.")
	end
	if not g_BuildingProtos[entry.id] then
		return Untranslated("Missing description.")
	end
	return origGetEntryRolloverDescription(self, entry, ...)
end

----

function IModeConstruction:Close(...)
	if self.controller then
		local local_data = ConstructionLocalData or {}
		self.controller:PersistLocalData(local_data)
		ConstructionLocalData = local_data
		self.controller:delete()
		self.controller = nil
	end
	self:DeleteThread("UpdateConstruction")
	return InterfaceModeDialog.Close(self, ...)
end

----

local delayed_place_objs = {}
local delayed_place_count = 0
local origPlaceObj = PlaceObj

function PlaceObj(class_name, tbl, arr, ...)
	local class = g_Classes[class_name]
	if not class then
		if not delayed_place_objs then
			assert(false, "Trying to create object of missing class " .. class_name)
			return
		elseif tbl and type(tbl[1]) == "string" or arr then -- StoreAsTable == false
			arr = arr or {}
			arr.class = class_name
			arr.__props__ = tbl
			delayed_place_count = delayed_place_count + 1
			delayed_place_objs[delayed_place_count] = arr
			return arr
		else -- StoreAsTable == true
			tbl = tbl or {}
			tbl.class = class_name
			delayed_place_count = delayed_place_count + 1
			delayed_place_objs[delayed_place_count] = tbl
			return tbl
		end
	end
	return class:__fromluacode(tbl, arr, ...)
end

function OnMsg.ClassesPostprocess()
	PlaceObj = origPlaceObj
	local SetObjPropertyList = SetObjPropertyList
	local objs = delayed_place_objs
	delayed_place_objs = nil
	for i=1,delayed_place_count do
		local obj = objs[i]
		local class_def = g_Classes[obj.class]
		if not class_def then
			assert(false, "Trying to place non-existent class " .. obj.class)
		elseif obj.__props__ then
			local props = obj.__props__
			obj.__props__ = nil
			class_def:new(obj)
			SetObjPropertyList(obj, props)
		else
			class_def:new(obj)
		end
		obj.class = nil
	end
end

----

function RecalcPreemptiveActivities()
	table.clear(g_PreemptiveActivities)
	ActivityIds = {}
	ForEachPreset("ActivityType", function(preset, group, ...)
		if preset.CanPreemptDefaultActivities then
			g_PreemptiveActivities = table.create_add(g_PreemptiveActivities, preset)
			g_PreemptiveActivities[preset.id] = preset
		end
		ActivityIds[#ActivityIds + 1] = preset.id
	end)
	table.stable_sort(g_PreemptiveActivities, function(a, b)
		return a.PreemptionPriority < b.PreemptionPriority
	end)
	table.sort(ActivityIds)
end

OnMsg.ModsReloaded = RecalcPreemptiveActivities

----

SetPropMeta("VisualStorageBase", "placement_spots", "items", GetEntitySpotsItems, CurrentModDef)
SetPropMeta("VisualStorageBase", "placement_spots", "arbitrary_value", true, CurrentModDef)

local origCreateCustomApproachAttach = Unit.CreateCustomApproachAttach
function Unit:CreateCustomApproachAttach(target, attach_class, spot_name, best_dist)
	if not IsValid(target) or not IsValid(self) then return end
	if spot_name and not target:HasSpot(spot_name) then
		assert(false, "Trying to reach non-existing spot "..spot_name)
		spot_name = "Origin"
	end
	return origCreateCustomApproachAttach(self, target, attach_class, spot_name, best_dist)
end

----

local origIsActivityAllowed = UnitActivity.IsActivityAllowed
function UnitActivity:IsActivityAllowed(activity_id)
	if not activity_id then
		return
	end
	return origIsActivityAllowed(self, activity_id)
end

----

local origSaveLocalStorage = SaveLocalStorage
function SaveLocalStorage()
	Msg("OnSaveLocalStorage")
	return origSaveLocalStorage()
end

function OnMsg.OnSaveLocalStorage()
	EngineOptions.FullscreenMode = EngineOptions.FullscreenMode
end

----

AppendClass.EquipResource = {
	__parents = { "UnitReactionsPreset", },
	properties = {
		{ category = "Effect", id = "OnEquip", help = "Called right after equiping", 
			editor = "script", default = false, params = "self, unit" },
		{ category = "Effect", id = "OnUnequip", help = "Called right after unequiping", 
			editor = "script", default = false, params = "self, unit" },
	},
}

SetPropMeta("ToolResource", "OnEquip", "category", "Effect", CurrentModDef)
SetPropMeta("ToolResource", "OnUnequip", "category", "Effect", CurrentModDef)

----

function OnMsg.ModsReloaded()
	local reactions_Tamed = HappinessFactors.AnimalWasTamed.msg_reactions
	local reaction_Born, idx_Born = table.find_value(reactions_Tamed, "Event", "AnimalWasBorn")
	if idx_Born then
		table.remove(reactions_Tamed, idx_Born)
	end
	local reactions_Born = HappinessFactors.AnimalWasBorn.msg_reactions
	local reaction_Tamed, idx_Tamed = table.find_value(reactions_Born, "Event", "AnimalTamed")
	if idx_Tamed then
		table.remove(reactions_Born, idx_Tamed)
	end
	if reaction_Born then
		table.insert(reactions_Born, reaction_Born)
	end
	if reaction_Tamed then
		table.insert(reactions_Tamed, reaction_Tamed)
	end
end

----

-- Fix mod shortcuts not being active if a saved game is loaded first
function OnMsg.LoadGame()
	DelayedCall(0, ReloadShortcuts)
end

----

function OnMsg.ChangeMap()
	SelectObj()
end

----

local offset_pt = point(0,0,guim)
local line_pts = {}
local line_color = RGB(205,125,0)

function UnitActivity:GatherAdditionalPathLines(line_and_numbers)
	if not options.ShowAdditionalTasks then
		return
	end
	
	iclear(line_pts)
	
	local first_path_point = self:GetPathPoint(1) or false
	if not first_path_point and #self.activity_queue == 0 then return false end
	if first_path_point then 
		local pos = first_path_point:SetZ(terrain.GetHeight(first_path_point)):InplaceAdd(offset_pt)
		line_pts[#line_pts + 1] = pos
	end
	local previous_object -- for cases where multiple objects have the same parent (fields, etc.)
	for i, activity in ipairs(self.activity_queue) do
		local activity_id, target, param1, param2 = unpack_params(activity)
		if IsValid(target) then
			local obj = GetTopmostParent(target)
			if obj ~= previous_object then
				local pos = obj:GetVisualPos()
				if IsValidPos(pos) then
					pos:InplaceAdd(offset_pt)
					line_pts[#line_pts + 1] = pos
					local text = PlaceText(tostring(i+(self.current_activity and 1 or 0)), pos)
					text:SetTextStyle("NextActivities")
					text:SetDepthTest(false)
					line_and_numbers[#line_and_numbers + 1] = text
				end
			end
			previous_object = obj
		end
	end
	
	local line = PlacePolyLine(line_pts, line_color, false)
	line_and_numbers[#line_and_numbers + 1] = line
end

----

DefineClass.AnimRemapEntry = {
	__parents = { "PropertyObject" },
	properties = {
		{ category = "Animations", id = "remap_from", name = "Remap From", editor = "choice", template = true, default = "", items = GetAllStatesCombo },
		{ category = "Animations", id = "remap_to",   name = "Remap To",   editor = "choice", template = true, default = "", items = function(self) return GetEntityStateItems(GetParentTableOfKindNoCheck(self, "PropertyObject")) end },
	},
	EditorView = Untranslated("<em><remap_from></em> = <positive><remap_to></positive>"),
	StoreAsTable = true,
}

CObject.missing_anim = "idle"
CObject.ResolveAnimName = return_second

AppendClass.Unit = {
	properties = {
		{ category = "Animations", id = "anim_remap_list",  name = "Anim Remapping", editor = "nested_list", template = true, base_class = "AnimRemapEntry", default = false, read_only = IsValid },
	},
	anim_remap = false,
}

function Unit:GetAnimRemapping()
	local anim_remap = self.anim_remap
	if not anim_remap then
		anim_remap = {}
		for _, remap in ipairs(self.anim_remap_list) do
			anim_remap[remap.remap_from] = remap.remap_to
			anim_remap[GetStateIdx(remap.remap_from)] = remap.remap_to
		end
		rawset(g_Classes[self.class], "anim_remap", anim_remap)
	end
	return anim_remap
end

function Unit:ResolveAnimName(state)
	if (state or "") == "" or self:HasState(state) then
		return state
	end
	local remap = self:GetAnimRemapping()
	local alt_state = remap[state]
	if alt_state and (alt_state == "" or self:HasState(alt_state)) then
		return alt_state
	end
end

local function ReportMissingState(self, state)
	local entity = self:GetEntity() or ""
	if entity == "" then
		GameTestsError("once", "Object without entity:", self.class)
	else
		GameTestsError("once", "Missing animation:", entity, '-', state)
	end
end

local origWaitMomentTrackedAnim = CObject.WaitMomentTrackedAnim
function Unit:WaitMomentTrackedAnim(state, wait_func, wait_param, count, flags, crossfade, duration, ...)
	if (state or "") ~= "" then
		local resolved_state = self:ResolveAnimName(state)
		if not resolved_state then
			resolved_state = self.missing_anim
			duration = Min(duration or 1000, 1000)
			ReportMissingState(self, state)
		end
		state = resolved_state
	end
	return origWaitMomentTrackedAnim(self, state, wait_func, wait_param, count, flags, crossfade, duration, ...)
end

local origSetState = CObject.SetState
function Unit:SetState(state, ...)
	local resolved_state = self:ResolveAnimName(state)
	if not resolved_state then
		resolved_state = state
		ReportMissingState(self, state)
	end
	return origSetState(self, resolved_state, ...)
end

local origGetAnimDuration = CObject.GetAnimDuration
function Unit:GetAnimDuration(anim, ...)
	if not anim then
		return origGetAnimDuration(self)
	end
	local resolved_state = self:ResolveAnimName(anim) or ""
	if resolved_state == "" then
		return 1000
	end
	return origGetAnimDuration(self, resolved_state, ...)
end

local origIterateMoments = CObject.IterateMoments
function CObject:IterateMoments(anim, phase, moment_index, moment_type, reversed, looping, moments, duration)
	return origIterateMoments(self, anim, phase, moment_index, moment_type, reversed, looping, moments)
end

----

-- typo fixed ModifyCombatEfficiencOnDiey
function UnitAnimal:GetCombatEfficiency()
	local efficiency = self.CombatSkill * self.Manipulation / 10000
	return self:CallReactions_Modify("ModifyCombatEfficiency", efficiency)
end

-- make the animals use the standard combat reactions
function UnitAnimal:GetHitEffects(target, weapon_def)
	return self:CallReactions_Modify("GatherHitEffects", nil, target, weapon_def)
end
function UnitAnimal:ModifyHitChance(hit_chance, target, weapon_def, target_dist)
	return self:CallReactions_Modify("ModifyHitChance", hit_chance, target, weapon_def, target_dist)
end
function UnitAnimal:AvoidAttackModify(hit_chance, attacker, weapon_def, attacker_dist)
	return self:CallReactions_Modify("AvoidAttackModify", hit_chance, attacker, weapon_def, attacker_dist)
end

----

AutoResolveMethods.WeaponRangeModify = "modify"
CombatObject.WeaponRangeModify = return_second

local origGetAttackRange = CombatObject.GetAttackRange
function UnitCombat:GetAttackRange(weapon_def)
	local max_range, min_range, weapon_def = origGetAttackRange(self, weapon_def)
	if not max_range then
		return
	end
	max_range = self:WeaponRangeModify(max_range, weapon_def) or max_range
	return max_range, min_range, weapon_def
end

function UnitCombat:WeaponRangeModify(range, weapon_def)
	return self:CallReactions_Modify("ModifyWeaponRange", range, weapon_def)
end

----

UnitActivity.SetMovingBuilding = Human.SetMovingBuilding

UnitActivity.UnequipCorpse = HumanoidCompositeBody.UnequipCorpse
UnitActivity.CmdUnequipCorpse = HumanoidCompositeBody.CmdUnequipCorpse

AutoResolveMethods.UnequipTool = true
Unit.UnequipTool = empty_func

AutoResolveMethods.GetEquippedTool = "or"
Unit.GetEquippedTool = empty_func

----

AppendClass.StoryBit = {
	properties = {
		{ category = "Popup", id = "LargePopup", name = "Large Popup", editor = "bool", default = false },
	}
}

local origOpenStoryBitsPopup = OpenStoryBitsPopup
function OpenStoryBitsPopup(id, ...)
	local storybit = StoryBits[id]
	if not storybit or not storybit.LargePopup then
		return origOpenStoryBitsPopup(id, ...)
	end
	local origOpenDialog = OpenDialog
	local modOpenDialog = function(template, ...)
		if template == "StoryBitPopup" then
			template = "StoryBitPopupLarge"
			OpenDialog = origOpenDialog
		end
		return origOpenDialog(template, ...)
	end
	OpenDialog = modOpenDialog
	procall(origOpenStoryBitsPopup, id, ...)
	if OpenDialog == modOpenDialog then
		OpenDialog = origOpenDialog
	end
end

function TryActivateStoryBit(id, object, immediate, activated_by, no_cooldown)
	local state = g_StoryBitStates[id] or StoryBits[id] and StoryBitState:new{ id = id }
	if not state or not state:CheckPrerequisites(object) then
		return
	end
	if activated_by then
		state.inherited_title = activated_by:GetTitle()
		state.inherited_image = activated_by:GetImage()
		state.player = activated_by.player
	end
	state:ActivateStoryBit(object, immediate)
	GetStoryBitCategoryState(id):StorybitActivated(state, no_cooldown)
	return true
end

----

AppendClass.DisplayPreset = {
	properties = {
		{ category = "Mod", id = "mod_version_major", name = "New In Version Major", editor = "number", default = 0, no_edit = function(self) return not self.mod end },
		{ category = "Mod", id = "mod_version_minor", name = "New In Version Minor", editor = "number", default = 0, no_edit = function(self) return not self.mod end },
	},
}

function DisplayPreset:GetNewInModTag()
	return self.mod and self.mod.version_major == self.mod_version_major and self.mod.version_minor == self.mod_version_minor and self.NewFeatureTag
end

local origGetDisplayName = DisplayPreset.GetDisplayName
function DisplayPreset:GetDisplayName()
	local tag = self:GetNewInModTag()
	if tag then
		return self.NewFeatureTag .. self.display_name
	end
	return origGetDisplayName(self)
end

local origGetDisplayNameCaps = DisplayPreset.GetDisplayNameCaps
function DisplayPreset:GetDisplayNameCaps()
	local tag = self:GetNewInModTag()
	if tag then
		return self.NewFeatureTag .. self.display_name
	end
	return origGetDisplayNameCaps(self)
end

local origOnEditorNew = DisplayPreset.OnEditorNew
function DisplayPreset:OnEditorNew(...)
	if self.mod then
		self.mod_version_major = self.mod.version_major
		self.mod_version_minor = self.mod.version_minor
	end
	return origOnEditorNew(self, ...)
end

----

function ReactionObject:GetRegisteredInstance(instance)
	for _, handlers in pairs(self.reaction_handlers) do
		for i = #handlers - 1, 1, -2 do
			local instance_i = handlers[i]
			if instance == instance_i or instance_i.__index == instance then
				return instance_i
			end
		end
	end
end

function UnitReactionsPreset:ReapplyReactions()
	if GetMap() == "" then return end
	MapForEach("map", "Unit", function(obj, self)
		local instance = obj:GetRegisteredInstance(self)
		if instance then
			obj:ReloadReactions(instance, self.unit_reactions)
		end
	end, self)
end

local origOnEditorSetProperty = Reaction.OnEditorSetProperty
function UnitReaction:OnEditorSetProperty(prop_id, old_value, ged)
	local parent = GetParentTableOfKindNoCheck(self, "UnitReactionsPreset")
	if parent then
		parent:ReapplyReactions()
	end
	return origOnEditorSetProperty(self, prop_id, old_value, ged)
end

----

-- Avoid saving empty prop values:
local origIsDefaultPropertyValue = PropertyObject.IsDefaultPropertyValue
function PropertyObject:IsDefaultPropertyValue(prop, prop_meta, value)
	if origIsDefaultPropertyValue(self, prop, prop_meta, value) then
		return true
	end
	local vtype = type(value)
	if vtype == "function" then
		prop_meta = prop_meta or self:GetPropertyMetadata(prop)
		local default_value = self:GetDefaultPropertyValue(prop, prop_meta)
		return type(default_value) == "function" and GetFuncBody(value) == GetFuncBody(default_value)
	end
	if vtype == "table" and next(value) == nil and not value.class then
		prop_meta = prop_meta or self:GetPropertyMetadata(prop)
		local default_value = self:GetDefaultPropertyValue(prop, prop_meta)
		if not default_value then
			return true
		end
	end
	return false
end

----

local efAttackable = const.efAttackable
function IsAttackable(target)
	return target and IsValid(target) and target:GetEnumFlags(efAttackable) ~= 0
end

----

-- cleanup invalid activities
function OnMsg.ModsReloaded()
	local valid = ActivityTypes
	for group_id, activity_group in sorted_pairs(ActivityGroups) do
		local activities = activity_group.Activities
		for i, activity_id in ripairs(activities) do
			if not valid[activity_id] then
				table.remove(activities, i)
				ModSmartLogF(CurrentModDef, "Removed invalid activity '%s'", activity_id)
			end
		end
	end
end

----

function Robot:ProduceButcherResources(unit, skill_level, efficiency)
	local spawned_loot
	local loot_def = LootDefs[self.SalvageLootTable]
	if loot_def then
		spawned_loot = {}
		local modifiers = unit and unit:CallReactions_Modify("GatherSiteLootModifiers", nil, self.SalvageLootTable, "Scavenging")
		loot_def:GenerateLoot(unit, self, false, spawned_loot, modifiers)
	end
	local work_action = spawned_loot and GetWorkActionInstance(self, "DisassembleRobot", unit.player)
	if work_action then
		for _, loot in ipairs(spawned_loot) do
			AttachDeliveryObjectsToPiles(loot[3], unit, work_action.activity_id)
		end
	end
	if next(spawned_loot) then
		PostMsg("ObjectScavenged", unit, self, spawned_loot)
	end
end

HumanoidRobotStation.CustomDataType = "outline+animated"

local origInitEquipement = HumanoidRobot.InitEquipement or empty_table
function HumanoidRobot:InitEquipement(...)
	local equipment = self.Equipment
	if equipment and #equipment == 0 then -- the vanilla method would break in that case
		for _,slot in ipairs(EquipSlots) do
			self:Unequip(slot)
		end
		return
	end
	return origInitEquipement(self, ...)
end

----

-- Shield crash from division by zero
if FirstLoad then
	__origIsInsideObj = IsInsideObj
end
local __origIsInsideObj = __origIsInsideObj
function IsInsideObj(obj, ...)
	if obj:GetRadius() <= 0 or obj:GetWorldScale() <= 0 then
		return
	end
	return __origIsInsideObj(obj, ...)
end

----

-- Shield terrain.FindReachable tfrDir option with object provided
if FirstLoad then
	__terrain_FindReachable = terrain.FindReachable
end
local __tfrDir = const.tfrDir
local __tfrPassClass = const.tfrPassClass
local __tfrCanDestlock = const.tfrCanDestlock
local __tfrLimitDist = const.tfrLimitDist
local __terrain_FindReachable = __terrain_FindReachable
local origGetFormationApproachTarget = UnitInvader.GetFormationApproachTarget
function UnitInvader:GetFormationApproachTarget(target, ...)
	terrain.FindReachable = function(approach_target, tfrPassClass, pfclass, tfrCanDestlock, owner, tfrLimitDist, formation_radius, min_spread, tfrDir, formation_dir, ...)
		if tfrPassClass == __tfrPassClass and tfrCanDestlock == __tfrCanDestlock and tfrLimitDist == __tfrLimitDist and tfrDir == __tfrDir
		and IsValid(formation_dir) then
			formation_dir = formation_dir:GetAngle()
		end
		return __terrain_FindReachable(approach_target, tfrPassClass, pfclass, tfrLimitDist, formation_radius, min_spread, tfrDir, formation_dir, ...)
	end
	
	local ok, approach_pos, formation_radius, leader, keep_formation, group_size = procall(origGetFormationApproachTarget, self, target, ...)
	
	terrain.FindReachable = __terrain_FindReachable
	if not ok then
		return
	end
	return approach_pos, formation_radius, leader, keep_formation, group_size
end

----

CObject.SetColorModifierReasonRecursive = SetColorModifierReason
function SetColorModifierReason(obj, reason, color, ...)
	return obj:SetColorModifierReasonRecursive(reason, color, ...)
end

DefineClass.NoVisualsObject = {
	__parents = { "CObject" },
	flags = { efSunShadow = false, efShadow = false, efVisible = false, efSelectable = false, efWalkable = false, efCollision = false, efApplyToGrids = false },
	
	SetContourOuterID = empty_func,
	SetContourRecursive = empty_func,
	SetOpacity = empty_func,
	SetColorModifier = empty_func,
	SetColorModifierReasonRecursive = empty_func,
	SetUnderConstruction = empty_func,
	SetUnderConstructionRecursive = empty_func,
}

AppendClass.ActivityObject = {
	__parents = { "NoVisualsObject" },
}

AppendClass.ConstructionElement = {
	flags =  { cofComponentColorizationMaterial = true },
}

----

function OnMsg.ApplicationQuitting()
	ClearGameThreads()
end

---- UnitRestrictions for all

AutoResolveMethods.UIShowRestrictions = "and"
AutoResolveMethods.GetRestrictionResTextStyle = "or"
AutoResolveMethods.GetAllowedCategoryResourcesText = "or"

Unit.GetRestrictionResTextStyle = empty_func

function Unit:GetAllowedCategoryResourcesText(res)
	local allowed, total = self:GetAllowedCategoryResources(res)
	if total == 0 and allowed == 0 then return "" end
	return T{596326548073, "<em><allowed> / <total></em>", allowed = allowed, total = total}, allowed ~= total
end

function Unit:UIShowRestrictions()
	return next(self.UnitRestrictions)
end

function UnitAnimalAutoResolve:UIShowRestrictions()
	return self:IsTamed()
end

local match = {
	class = "XTemplateTemplate",
	__context_of_kind = "Human",
	__template = "InfopanelTab",
}
local change = {
	__context_of_kind = "Unit",
	__condition = function (parent, context) return context and context.UIShowRestrictions and context:UIShowRestrictions() end,
}
UIChange("tabHumanRestrictions", match, change, CurrentModDef)

local match = {
	class = "XTemplateTemplate",
	__template = "ResourceRestrictionsSection",
}
local change = {
	__context_of_kind = "Human",
}
UIChange("tabHumanRestrictions_Restrictions", match, change, CurrentModDef)

AppendClassMembers.UnitRestrictions = AppendClassMembers.properties

----

if not table.find(TamingUIToggleActions, "id", "Reproduce") then
	TamingUIToggleActions[#TamingUIToggleActions + 1] = {
		id = "Reproduce",
		display_name = T(788701392089, "Reproduce"),
		caps_name = T(788701392089, "Reproduce"),
		rollover_text = T(788701392090, "Allow/Forbid this animal to reproduce"),
		IsActionEnabled = function(self, obj) return not obj:IsDead() and obj.Gender ~= "infertile" end,
		IsActionToggled = function(self, obj) return not obj.disable_reproduce end,
		ToggleAction = function(self, obj) if IsValid(obj) then obj:rfnToggleAllowedReproduce() end end	,
		NetSyncToggleAction = function(self, obj) if IsValid(obj) then NetSyncEvent("ObjFunc", obj, "rfnToggleAllowedReproduce") end end	
	}
end

function UnitAnimal:rfnToggleAllowedReproduce()
	self.disable_reproduce = not self.disable_reproduce
	if self.disable_reproduce then
		self.DailyPregnancyChanceOrig = self:GetClassValue("DailyPregnancyChance") ~= self.DailyPregnancyChance and self.DailyPregnancyChance or nil
		self.DailyPregnancyChance = 0
	else
		self.DailyPregnancyChance = self.DailyPregnancyChanceOrig
		self.DailyPregnancyChanceOrig = nil
	end
	ObjModified(self)
end

if not Platform.debug then
	-- suppress log spam
	if FirstLoad then
		origGetRandomSpot = CObject.GetRandomSpot
	end
	local MarkHash = MarkHash
	local origGetRandomSpot = origGetRandomSpot
	function Building:GetRandomSpot(...)
		if select("#", ...) == 1 and not self:HasSpot(...) then
			if MarkHash("GetRandomSpot", self:GetEntity(), ...) then
				DebugPrint("[Entity Error]", self:GetEntity(), "doesn't have a spot", ...)
			end
			return -1
		end
		return origGetRandomSpot(self, ...)
	end
end

local origIsAutosaveAllowed = IsAutosaveAllowed
function IsAutosaveAllowed()
	if IsModEditorMap() and options.NoAutosaveOnModMap then
		return
	end
	return origIsAutosaveAllowed()
end

local origFormatAsFloat = FormatAsFloat
function FormatAsFloat(v, ...)
	v = tonumber(v) or 0
	return origFormatAsFloat(v, ...)
end

local GetStorageStatusAndDecayProp = ComplexResStorage.GetStorageStatusAndDecayProp
function ComplexResStorage:GetStorageStatusAndDecayProp(...)
	if not self.storage_temperature then
		return
	end
	return GetStorageStatusAndDecayProp(self, ...)
end

AutoResolveMethods.CanBeCopied = "or"
Object.CanBeCopied = empty_func

AutoResolveMethods.CanDisableSupply = "or"
Object.CanDisableSupply = empty_func

AutoResolveMethods.GetUpgradeRolloverText = "or"
Object.GetUpgradeRolloverText = empty_func

----

DefineClass("BuildingDefaults")

BuildingDefaults.ShouldIlluminate = empty_func

AppendClass.Building = {
	__parents = { "BuildingDefaults" },
}
AppendClass.BuildingComponent = {
	__parents = { "BuildingDefaults" },
}

----

MapVar("BlockedBuildings", false)

function UpdateBlockedBuilding(bld, unreachable)
	local blocked = BlockedBuildings
	if unreachable then
		if not blocked then
			blocked = {}
			BlockedBuildings = blocked
		end
		if not blocked[bld] then
			blocked[bld] = true
			Msg("BuildingBlocked", bld)
		end
	else
		if blocked and blocked[bld] then
			blocked[bld] = nil
			Msg("BuildingUnblocked", bld)
		end
	end
end
local UpdateBlockedBuilding = UpdateBlockedBuilding

local Building_OnSetUnreachable = Building.OnSetUnreachable
function Building:OnSetUnreachable(unit, unreachable)
	procall(UpdateBlockedBuilding, self, unreachable)
	return Building_OnSetUnreachable(self, unit, unreachable)
end

OnMsg.BuildingRemoved = UpdateBlockedBuilding

function GetResTransporterCarryAmount(res) -- used to fill ResourceUnits, which is required by the C++ code
	local def = Resources[res]
	return def and def.carry_amount or 0
end

----

AutoResolveMethods.rfnToggleGarrisoned = true
Unit.rfnToggleGarrisoned = empty_func
AutoResolveMethods.OnSetGarrisoned = nil
RecursiveCallMethods.OnSetGarrisoned = true

function Robot:rfnToggleGarrisoned(state)
	local station = self.robot_station
	if not station then return end
	if state == nil then
		state = not self.garrisoned
	elseif state == self.garrisoned then
		return
	end
	self.garrisoned = state
	self.garrison_start_time = self.garrisoned and GameTime() or false
	self:OnSetGarrisoned(self.garrisoned)
	self:TrySetCommand("CmdInterrupt")
	ObjModifiedDelayed(self)
	if IsValid(station) then
		ObjModifiedDelayed(station)
	end
end

----

function FuncPieces:CompositeDefPerentsCombo(prop)
	return ClassDescendantsCombo(self.ObjectBaseClass, true, function(class)
		return class ~= self.id
	end)
end

SetPropMeta("CompositeDef", "object_class", "items", FuncPieces.CompositeDefPerentsCombo)