local is_debug = Platform.debug
local DebugPrint = DebugPrint
local starts_with = string.starts_with
local format = string.format
local select = select
local procall = procall
local type = type
local empty_func = empty_func

FuncNames = {
	[empty_func] = "empty_func",
	[return_true] = "return_true",
	[return_0] = "return_0",
	[return_100] = "return_100",
}
FuncSources = {
	empty_func = {"return", "return nil", ""},
	return_true = {"return true"},
	return_0 = {"return 0"},
	return_100 = {"return 100"},
}
FuncPieces = setmetatable({}, { __newindex = function(table, name, func)
	FuncNames[func] = name
	rawset(table, name, func)
end})
	
----

function ConsolePrintNoLogNL(str)
	if type(str) ~= "string" then return end
	if not str:ends_with('\n') then
		str = str .. '\n'
	end
	return ConsolePrintNoLog(str)
end

console_print = CreatePrint{
	"",
	output = ConsolePrintNoLogNL,
}
console_printf = CreatePrint{
	"",
	format = "printf",
	output = ConsolePrintNoLogNL,
}

if FirstLoad then
	__marked_hash = {}
end
local __marked_hash = __marked_hash
function MarkHash(...)
	local hash = xxhash(...)
	if __marked_hash[hash] then
		return
	end
	__marked_hash[hash] = true
	return true
end
local MarkHash = MarkHash

function string_format(fmt, ...)
	if select("#", ...) > 0 then
		local ok, res = procall(format, fmt, ...)
		if ok then
			return res
		end
	end
	return fmt
end

local show_headers = {"Failed", "Error", "Cannot", "Outdated"}
local function HandlePrint(once, str, ...)
	str = string_format(str, ...)
	if once and not MarkHash("mod_print", str) then
		return
	end
	if is_debug then
		for _, header in ipairs(show_headers) do
			if starts_with(str, header) then
				console_print("once", "<style UISubtitles><color 255 0 0>[Mod problem]</color></style><style BugReportScreenshot>", str, "</style>")
				break
			end
		end
	end
	return DebugPrint("[mod]", str, "\n")
end

mod_print = function(str, ...)
	if str == "once" then
		return HandlePrint(true, ...)
	else
		return HandlePrint(false, str, ...)
	end
end

local mod_printed
local item_printed
local time_printed

function ModSmartPrint(obj, ...)
	if time_printed ~= RealTime() then
		time_printed = RealTime()
		mod_printed = nil
		item_printed = nil
	end
	if IsKindOf(obj, "ModDef") then
		if mod_printed ~= obj.id then
			mod_printed = obj.id
			item_printed = nil
			printf("Mod %s \"%s\"", obj.id, obj.title)
		end
		return ModSmartPrint(...)
	end
	if IsKindOf(obj, "ModItem") then
		local descr = TTranslate(obj.ModItemDescription, obj, false)
		local item_id = obj.class .. " " .. descr
		if item_printed ~= item_id then
			item_printed = item_id
			printf("    %s \"%s\"", obj.class, descr)
		end
		return ModSmartPrint(...)
	end
	print("       ", obj, ...)
end

function ModItem:ModPrint(...)
	return ModSmartPrint(self.mod, self, ...)
end

function ModDef:ModPrint(...)
	return ModSmartPrint(self, ...)
end

----

if FirstLoad then
	ModSmartLogCache = {}
end
local ModSmartLogCache = ModSmartLogCache

-- Fix for a broken syntax highlighting in the mod manager log after certain special symbols
local logidx, inlog = 0, false
local function FixLogMessage()
	local log = ModMessageLog
	local last = #log
	local find = string.find_plain
	local chars = {"'", '"', '[['}
	local next_chars = {['[['] = ']]'}
	while logidx < last do
		logidx = logidx + 1
		local idx = 0
		local msg = log[logidx]
		local char
		while true do
			local char, min_idx = false, max_int
			for i, chari in ipairs(chars) do
				local idxi = find(msg, chari, idx + 1) or max_int
				if idxi < min_idx then
					min_idx = idxi
					char = next_chars[chari] or chari
				end
			end
			if not char then
				break
			end
			idx = find(msg, char, min_idx + 1)
			if not idx then
				log[logidx] = msg .. char
				break
			end
		end
	end
end

local origModLog = ModLog
function ModLog(...)
	if inlog then return end
	logidx = #ModMessageLog
	origModLog(...)
	inlog = true
	procall(FixLogMessage)
	inlog = false
end
local ModLog = ModLog

function ModLogF(log, fmt, ...)
	if type(log) == "string" then
		return ModLogF(false, fmt, ...)
	end
	local msg = string_format(fmt, ...)
	return ModLog(log, Untranslated(msg))
end
local ModLogF = ModLogF

local function __ModSmartLogF(obj, fmt, ...)
	if (fmt or "") == "" then
		return ModLog("")
	end
	local str = string_format(fmt, ...)
	if IsKindOf(obj, "ModDef") then
		str = "--[[ " .. obj.title .. " ]] " .. str
	end
	local hash = xxhash(str)
	if ModSmartLogCache[hash] then
		return
	end
	ModSmartLogCache[hash] = true
	return ModLog(Untranslated(str))
end

function ModSmartLogF(obj, fmt, ...)
	return procall(__ModSmartLogF, obj, fmt, ...)
end

function ModDef:ModLogF(fmt, ...)
	return ModSmartLogF(self, fmt, ...)
end

function ModItem:ModLogF(fmt, ...)
	return ModSmartLogF(self.mod, fmt, ...)
end

----

function forward_pcall(ok, ...)
	if not ok then return end
	return ...
end
local forward_pcall = forward_pcall

function rawcall(func, ...)
	return forward_pcall(procall(func, ...))
end

LuaReloaded = true

if FirstLoad then
	LuaReloaded = false
	ModsReloading = true
	DlcListLoaded = false
	UIChangeList = {}
end

function OnMsg.ModsReloaded()
	if ModsReloading then
		ModsReloading = false
		CreateRealTimeThread(function()
			Msg("ModsFirstPostReload")
			MsgClear("ModsFirstPostReload")
		end)
	end
	local list = UIChangeList
	UIChangeList = false
	for _, entry in ipairs(list) do
		local parent, match, change, source = entry[1], entry[2], entry[3], entry[4]
		UIChange(parent, match, change, source)
	end
end

function WaitModsFirstLoad()
	if ModsReloading then
		WaitMsg("ModsFirstPostReload")
	end
end

local function __UIForEachControl(parent, passed, indent, callback, ...)
	if not parent or not parent[1] or passed[parent] then
		return
	end
	passed[parent] = true
	indent = indent + 1
	for idx, control in ripairs(parent) do
		local res = callback(control, parent, idx, indent, ...)
		if res then
			return control, parent, idx, res
		end
	end
	for _, control in ripairs(parent) do
		local sub_control, sub_parent, sub_idx, res = __UIForEachControl(control, passed, indent, callback, ...)
		if sub_control then
			return sub_control, sub_parent, sub_idx, res
		end
	end
end

function UIForEachControl(parent, callback, ...)
	if type(parent) == "string" then
		assert(next(XTemplates), "XTemplates not yet loaded!")
		local template = XTemplates and XTemplates[parent]
		if not template then
			ModLogF(true, "No such XTemplate %s found!", parent)
			return
		end
		parent = template
	end
	return __UIForEachControl(parent, {}, 0, callback, ...)
end

local function MatchControl(control, parent, idx, indent, match)
	if type(match) == "string" then
		return control.ActionId == match
	end
	for key, value in pairs(match) do
		if control[key] ~= value then
			return
		end
	end
	return true
end

function UIFindControl(parent, match)
	return UIForEachControl(parent, MatchControl, match)
end

local __count = 0
local function __TryChange(control, parent, idx, indent, match, change)
	if MatchControl(control, parent, idx, indent, match) then
		__count = __count + 1
		for member, value in pairs(change) do
			control[member] = value
		end
	end
end

function UIChangeControl(parent, match, member, value, source)
	return UIChange(parent, match, { [member] = value }, source)
end

function UIChange(parent, match, change, source)
	if UIChangeList then
		table.insert(UIChangeList, {parent, match, change, source})
		return
	end
	__count = 0
	UIForEachControl(parent, __TryChange, match, change)
	if type(parent) == "table" then
		parent = parent.id
	end
	if __count == 0 then
		ModSmartLogF(source, "No such UI control %s found in '%s'!", ValueToStr(match), parent)
	elseif type(match) == "string" or table.count(match) == 1 then
		for member, value in pairs(change) do
			ModSmartLogF(source, "%s '%s' %s = %s", parent, ValueToStr(match), tostring(member), ValueToStr(value))
		end
	end
	return __count
end

----

function ColorToTag(color)
	local r, g, b = GetRGB(color or 0)
	return string_format("<color %d %d %d>", r, g, b)
end

----

function IsSimpleType(value)
	local vtype = type(value)
	if vtype == "string" or vtype == "number" or vtype == "boolean" or vtype == "nil" then
		return true
	end
	if vtype == "table" then
		return IsT(value) or IsRange(value)
	end
	if vtype == "userdata" then
		return IsPoint(value) or IsBox(value)
	end
end

function SimpleValueToStr(value, max_len)
	local str
	if IsT(value) then
		str = 'T("' .. TTranslate(value) .. '")'
	else
		str = IsSimpleType(value) and ValueToLuaCode(value) or type(value)
	end
	if (max_len or 0) > 0 and #str > max_len then
		str = string.sub(str, 1, max_len) .. "..."
	end
	return str
end

function SetPropMeta(classname, id, meta_name, meta_value, source)
	OnMsg.ClassesGenerate = function(classdefs)
		local class
		if type(classname) == "string" then
			class = classdefs[classname]
		else
			class = classname
			classname = classname.class
		end
		local found = 0
		for _, prop in ipairs(class.properties) do
			if prop.id == id then
				found = found + 1
				old_value = prop[meta_name]
				prop[meta_name] = meta_value
				ModSmartLogF(source, "%s.%s: %s = %s", classname, id, meta_name, string.trim(ValueToStr(meta_value), 64, '..."'))
			end
		end
		if found == 0 then
			ModSmartLogF(source, "No prop '%s' found in '%s'", id, classname)
		end
	end
end

----

function SyncSeed(purpose)
	return xxhash(MapLoadRandom, purpose, GameTime())
end

function SyncRand(range, purpose)
	if range <= 1 then return 0 end
	return xxhash(MapLoadRandom, purpose, GameTime()) % range
end
  
function SyncChance(chance, purpose)
	if chance <= 0 then return false end
	if chance >= 100 then return true end
	return chance > SyncRand(100, purpose)
end

----

-- plain text find
local string_find = string.find
function string.find_plain(str, what, from)
	return string_find(str, what, from or 1, true)
end

-- plain text remove
function string.remove(str, what)
	local i, j = 1
	while true do
		i, j = string_find(str, what, i, true)
		if not i then
			return str
		end
		str = str:sub(1, i - 1) .. str:sub(j + 1)
	end
end

-- plain text count
function string.count(str, what)
	local count = 0
	local i = 0
	while true do
		i = string_find(str, what, i + 1, true)
		if not i then
			break
		end
		count = count + 1
	end
	return count
end

function string.find_last(str, what)
	local last_i, last_j
	local i, j = 1
	while true do
		i, j = str:find(what, i, true)
		if not i then
			break
		end
		last_i, last_j = i, j
		i = j + 1
	end
	return last_i, last_j
end

----

function RawGetGlobal(name)
	local orig_error = error
	error = empty_func
	local value = _G[name]
	error = orig_error
	return value
end



function TryDefineClass(name, ...)
	if not RawGetGlobal(name) then
		return DefineClass(name, ...)
	end
end

----

function GetModLoaded(mod_id)
	if not mod_id then return end
	return table.find_value(ModsLoaded, "id", mod_id)
end

----

function NonGroupResourceFilter(preset)
	return not preset.is_group
end
function GroupResourceFilter(preset)
	return preset.is_group
end

----

local all_states
function GetAllStatesCombo()
	all_states = all_states or table.keys(EntityStates, true)
	return all_states
end

local function GetMember(self, member)
	return self[member]
end 

function ResolveObjEntity(obj)
	if not obj then
		return ""
	end
	if obj.GetEntity then
		local entity = obj:GetEntity() or ""
		if entity ~= "" then
			return entity
		end
	end
	local getter = obj.GetProperty or GetMember
	return getter(obj, "entity") or getter(obj, "Entity") or getter(obj, "unit_entity") or ""
end

function GetEntityStateItems(obj)
	local entity = ResolveObjEntity(obj)
	local states = IsValidEntity(entity) and GetStates(entity) or {}
	table.sort(states)
	table.insert(states, 1, "")
	return states
end

function GetEntitySpotsItems(obj)
	local entity = ResolveObjEntity(obj)
	local states = IsValidEntity(entity) and GetStates(entity) or ""
	if #states == 0 then return empty_table end
	local idx = table.find(states, "idle")
	local spots = {}
	local spot_begin, spot_end = GetAllSpots(entity, states[idx] or states[1])
	for spot = spot_begin, spot_end do
		spots[GetSpotName(entity, spot)] = true
	end
	return table.keys(spots, true)
end

----

local remove = table.remove
function table.remove_entries(array, entry)
	for i = #(array or ""), 1, -1 do
		if array[i] == entry then
			remove(array, i)
		end
	end
end

----

function OnMsg.DlcsLoaded()
	DlcListLoaded = true
end
function AreDlcsLoaded()
	return DlcListLoaded or DataLoaded
end
function WaitDlcsLoaded()
	if not AreDlcsLoaded() then
		WaitMsg("DlcsLoaded", 100)
	end
end

function WaitDesktopCreated()
	if not terminal.desktop then
		WaitMsg("DesktopCreated", 100)
	end
end

function ForceModLuaReload()
	SaveLocalStorage = empty_func
	GedToggleDebugWaypoints()
end

function WaitAccountStorageSaving()
	while IsValidThread(SaveAccountStorageThread) do
		WaitMsg(SaveAccountStorageThread, 100)
	end
end

if Platform.debug then

local origIsDlcAvailable = IsDlcAvailable
function IsDlcAvailable(dlc)
	assert(AreDlcsLoaded(), "DLC info not yet available!")
	return origIsDlcAvailable(dlc)
end

end -- Platform.debug