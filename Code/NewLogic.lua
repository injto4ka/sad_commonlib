DefineClass.SpawnResourceAt = {
	__parents = { "Effect", },
	__generated_by_class = "EffectDef",

	properties = {
		{ id = "Resource", name = "Resource", help = "The id of the desired resource", 
			editor = "preset_id", default = false, preset_class = "Resource", preset_filter = NonGroupResourceFilter },
		{ id = "Amount", name = "Amount", help = "Resource amount", 
			editor = "number", default = const.ResourceScale, scale = "res", min = const.ResourceScale },
		{ id = "Instant", name = "Instant", help = "Without animation",
			editor = "bool", default = false },
	},
	EditorView = Untranslated("Spawn Resource <res_amount(Resource, Amount)> <u(Resource)>"),
	Documentation = "Spawns a resource around the context object.",
	EditorNestedObjCategory = "Preset",
	RequiredObjClasses = { "MapObject" },
}

function SpawnResourceAt:ValidateObject(obj, parentobj_text, ...)
	if not IsValidPos(obj) then
		parentobj_text = string.concat("", parentobj_text, ...) or "Unknown"
		assert(false, string_format("%s: Object for %s must be on the map!", parentobj_text, self.class))
		return
	end
	return Effect.ValidateObject(self, obj, parentobj_text, ...)
end

function SpawnResourceAt:__exec(obj, context)
	if not IsValidPos(obj) then
		return
	end
	local params = not self.Instant and { jump_from = obj }
	PlaceResourcePile(obj, self.Resource, self.Amount, nil, params)
end

function SpawnResourceAt:GetError()
	if not self.Resource then
		return "Select a valid resource"
	elseif Resources[self.Resource] and Resources[self.Resource].is_group then
		return "Select a non-group resource"
	elseif (self.Amount or 0) <= 0 then
		return "Select a correct amounts"
	end
end

----

function Human:ChangeDef(name, gain_traits)
	local def = CharacterDefs[name]
	if not def then
		assert(def, "No such character: " .. tostring(name))
		return
	end
	self:RemoveReactions(self.human_def)
	self.human_def = def
	self.composite_part_groups = next(def.CompositePartGroups) and def.CompositePartGroups or nil
	self:ApplyDef()
	if gain_traits then
		for _, id in ipairs(def.Traits) do
			self:SetTrait(id, true, true)
		end
	end
	ObjModified(self)
end

----
