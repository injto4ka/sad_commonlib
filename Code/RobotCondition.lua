local IsKindOf = IsKindOf
local ResolveCritChance = ResolveCritChance
local GetAccountStorageOptionValue = GetAccountStorageOptionValue
local ObjModifiedDelayed = ObjModifiedDelayed
local PostMsg = PostMsg
local MatchGameScenarioTags = MatchGameScenarioTags
local rawget = rawget
local ipairs = ipairs
local type = type

local help = [[The <style GedHighlight>RobotCondition</style> is a simplified equivalent of the <style GedHighlight>HealthCondition</style>.
The robot conditions are applied via <style GedHighlight>Reactions</style>, <style GedHighlight>Modifiers</style> and the custom <style GedHighlight>OnAdd</style> and <style GedHighlight>OnRemove</style> functors.
As the weapons apply damage directly to robots and buildings, these effects are supposed to affect other additional stats (e.g. Movement, Combat, etc)]]

DefineClass.RobotCondition = {
	__parents = { "UnitReactionsPreset", "StatusEffect", "ModifiersPreset", },
	__generated_by_class = "PresetDef",

	properties = {
		{ category = "UI", id = "DisplayName",      name = "Display name",       editor = "text",     default = false, translate = true, },
		{ category = "UI", id = "ShowFloatingText", name = "Show floating text", editor = "bool",     default = true, },
		{ category = "UI", id = "FloatingTextIcon", name = "Floating text Icon", editor = "ui_image", default = false, },
		{ category = "UI", id = "Description",      name = "Description",        editor = "text",     default = false, translate = true, lines = 1, max_lines = 5, },
		
		{ category = "Status Effect", id = "Modifiers", name = "Modifiers",  editor = "nested_list", default = false, base_class = "ModifyRobot", all_descendants = true, },
		{ category = "Status Effect", id = "AllowedInScenarios", name = "Allowed in scenarios", editor = "set",    default = false, items = function (self) return GatherAllScenarioTags() end, help = "If left empty, this preset is allowed in all scenarios" },
		{ category = "Status Effect", id = "UnitTags",           name = "Unit tags",            editor = "set",    default = false, items = function (self) return PresetsCombo("UnitTags") end, help = "No tags means any unit.", three_state = true },
		{ category = "Status Effect", id = "Polarity",           name = "Polarity",             editor = "choice", default = "neutral", items = {"neutral", "positive", "negative"}, },
	},
	HasParameters = true,
	GlobalMap = "RobotConditions",
	PersistAsReference = false,
	EditorMenubarName = "Robot Conditions",
	EditorIcon = "CommonAssets/UI/Icons/clinic healthcare hospital medical medical kit.png",
	EditorMenubar = "Editors",
	EditorCustomActions = {
		{
			FuncName = "TestAdd",
			Icon = "CommonAssets/UI/Ged/play",
			Name = "Add",
			Rollover = "Add to selected robot",
			Toolbar = "main",
		},
	},
	body_part = false,
	reason = false,
	source = false,
	Documentation = help,
}

DefineModItemPreset("RobotCondition", { EditorName = "Robot Condition", EditorSubmenu = "Robots" })

function RobotCondition:OnExpire(owner, ...)
	owner:RemoveRobotCondition(self, "expire")
end

function RobotCondition:RemoveFromOwner(owner, reason)
	owner:RemoveRobotCondition(self, reason)
end

function RobotCondition:OnStackLimitReached(owner)
	local effect = owner:FirstEffectByCounter(self:StackLimitCounter())
	if effect then
		assert(IsKindOf(effect, "RobotCondition"))
		owner:RemoveRobotCondition(effect, "ReplaceOldest")
		return owner:AddStatusEffect(self)
	end
end

function RobotCondition:StackLimitCounter()
	return self.id
end

function RobotCondition:TestAdd()
	NetSyncEvent("Cheat", "CheatApplyRobotCondition", self.id, Selection)
end

function RobotCondition:GetDisplayNameAndIcon()
	local icon = self.FloatingTextIcon or ""
	if icon ~= "" then
		icon = Untranslated("<image ".. icon .. " 1000>")
	end
	local text = "<color_tag><icon><condition></color>"
	local count = self.display_count or 0
	if count > 1 then
		text = text .. " x<count>"
	end
	return Untranslated{text,
		icon = icon,
		condition = self.DisplayName,
		color_tag = self.Polarity == "positive" and TLookupTag("<positive>") or self.Polarity == "negative" and TLookupTag("<negative>") or TLookupTag("<em>"),
		count = count,
	}
end

function FormatModifierText(modifier, obj)
	obj = obj or g_Classes[modifier.obj_class or ""]
	if not IsKindOf(obj, "PropertyObject") then return end
	local prop_meta = obj:GetPropertyMetadata(modifier.prop)
	if not prop_meta or not prop_meta.name then return end
	local name = IsT(prop_meta.name) and prop_meta.name or Untranslated(prop_meta.name)
	local amount, percent
	if modifier.add ~= 0 then
		amount, percent = modifier.add, false
	else
		amount, percent = modifier.mul, true
	end
	local scale = 1
	if percent then
		amount = DivRound(amount, 10) - 100
	else
		scale = GetPropScale(prop_meta.scale)
	end
	if amount == 0 then return nil end
	local sign_T = amount < 0 and T(555613400236, "-") or T(510818369089, "+")
	local amount_T = percent and T{886549935977, "<percent(amount)>", amount = abs(amount)} or FormatAsFloat(abs(amount), scale, 3, true)
	local is_positive = amount > 0 
	local positivity_open_T = (amount ~= 0 and is_positive) and T(408541079442, "<positive>") or T(390121665979, "<negative>")
	local positivity_close_T = (amount ~= 0 and not is_positive) and T(562779942632, "</positive>") or T(323231961283, "</negative>")
	return T{561467779447, "<name><right><positivity_open><sign><amount><positivity_close><left>",
		name = name,
		sign = sign_T,
		amount = amount_T,
		positivity_open = positivity_open_T,
		positivity_close = positivity_close_T,
	}
end
local FormatModifierText = FormatModifierText

function RobotCondition:GetEffectsDescription(obj)
	local effects = { }
	if (self.Description or "") ~= "" then
		table.insert(effects, self.Description)
		table.insert(effects, "")
	end
	local modifiers = table.ifilter(self.Modifiers, function(i, modifier) return IsKindOf(modifier, "ModifyRobot") end)
	table.sortby_field(modifiers, "prop")
	for i,modifier in ipairs(modifiers) do
		local text = FormatModifierText(modifier, obj)
		if IsT(text) and text ~= "" then
			table.insert(effects, text)
		end
	end
	if self.Expiration and self.expiration_time and self.expiration_time - GameTime() >= 0 then
		table.insert(effects, "")
		table.insert(effects, T{976879128263, "<newline>Time remaining<right><time_icon><em><duration(time_left)></em>",
			time_left = self.expiration_time - GameTime(),
		})
	end

	return table.concat(effects, "\n")
end

function CheatApplyRobotCondition(id, units)
	return CheatCallFor(units, "Robot", "AddRobotCondition", id, "cheat")
end

function ModItemRobotCondition:TestModItem(ged)
	ModItemPreset.TestModItem(self, ged)
	if IsKindOf(SelectedObj, "Robot") then
		SelectedObj:AddRobotCondition(self.id, "mod")
	end
end

function OnMsg.ModUnloadLua(id)
	if id == CurrentModId then
		Presets.RobotCondition = nil
	end
end


----

DefineClass.ModifyUnitProperty = {
	__parents = { "ModifyProperty", "ModifyUnit" },
	HasSubObjectProp = false,
	HasIdProp = false,
	HasDisplayTextProp = false,
}

local origModApply = Modifier.ModApply
function Modifier:ModApply(value)
	if not value then return 0 end
	return origModApply(self, value)
end

local CreateInstance = PropertyObject.CreateInstance
function ModifiersPreset:CreateInstance(instance, ...)
	instance = CreateInstance(self, instance, ...)
	if #self.Modifiers > 0 and not rawget(instance, "Modifiers") then
		-- if the preset is applied multiple times via instances, its modifiers should also be applied multiple times as instances
		local modifiers = {}
		for i, modifier in ipairs(self.Modifiers) do
			modifiers[i] = CreateInstance(modifier, { container = instance } )
		end
		instance.Modifiers = modifiers
	end
	return instance
end

local origPostLoad = ModifiersPreset.PostLoad
function ModifiersPreset:PostLoad()
	for _, modifier in ipairs(self.Modifiers or empty_table) do
		modifier.__index = modifier
	end
	return origPostLoad(self)
end

local remove_entry = table.remove_entry
local origRemoveModifierObj = Modifiable.RemoveModifierObj
function Modifiable:RemoveModifierObj(modifier, prop)
	origRemoveModifierObj(self, modifier, prop)
	prop = prop or modifier.prop
	local modifications = self.modifications
	local modification_list = modifications and modifications[prop or false]
	if not modification_list then return end
	while remove_entry(modification_list, modifier) do
	end
	local container = modifier.container
	if not container then return end
	while container and remove_entry(modification_list, "container", container) do
	end
end

DefineClass.ModifyRobot = {
	__parents = { "ModifyUnitProperty" },
	obj_class = "Robot",
}

----
-- ModifyProperty
-- list all modifiable props for all descendents of the target class

local class_to_modifiable_props = {}
function ClassModifiablePropsNonTranslatableCombo(class)
	if not class then
		return ModifiablePropsComboItems
	end
	if type(class) == "table" then
		class = class.class
	end
	local props = class_to_modifiable_props[class]
	if not props then
		props = {}
		ClassDescendantsListInclusive(class, function(name, def)
			for _, prop in ipairs(def:GetProperties()) do
				if prop.modifiable and prop.editor == "number" then
					props[prop.id] = true
				end
			end
		end)
		props = table.keys2(props, true, "")
		class_to_modifiable_props[class] = props
	end
	return props
end

function FuncPieces:NoDisplayTextProp()
	return not self.HasDisplayTextProp
end

SetPropMeta("ModifyProperty", "display_text", "no_edit", FuncPieces.NoDisplayTextProp, CurrentModDef)
SetPropMeta("ModifyProperty", "display_text", "dont_save", FuncPieces.NoDisplayTextProp, CurrentModDef)
SetPropMeta("ModifyProperty", "prop", "default", "", CurrentModDef)

function ModifyProperty:GetError()
	if (self.prop or "") == "" then
		return "Missing property to modify"
	elseif self.add == 0 and self.mul == 1000 and self.add_min == 0 and self.add_max == 0 then
		return "Default values result in no modification"
	end
end

function ModifyProperty:__exec(...)
	return self:OnStart(...)
end

----

local scale = 1000

DefineClass.RobotConditionComponent = {
	__parents = { "StatusEffectsObject" },
	properties = {
		{ category = "Robot", id = "Movement",     name = T(722918221003, "Movement"),     editor = "number", modifiable = true, template = true, default = 100*scale, scale = scale, min = 10*scale, max = 500*scale },
		{ category = "Robot", id = "Combat",       name = T(635114201129, "Combat"),       editor = "number", modifiable = true, template = true, default = 0, scale = scale, min = 0, max = 500*scale },
		{ category = "Robot", id = "Regeneration", name = T(888105678794, "Regenaration"), editor = "number", modifiable = true, template = true, default = 0, scale = scale, help = "Integrity change per hour" },
	}
}

function RobotConditionComponent:Init()
	self:UpdateMoveSpeed()
end

local hour = const.HourDuration
function RobotConditionComponent:OnObjUpdate(time, delta)
	if self.Health <= 0 then return end
	-- health regen if there is no bleeding, otherwise bleed
	local change = self.Regeneration
	if change == 0 then return end
	self:ChangeHealth(change * delta / hour, "time")
end

CombatObject.GetHitEffects = empty_func
CombatObject.GetCombatEfficiency = return_0

AutoResolveMethods.GetHitEffects = "or"
AutoResolveMethods.ModifyHitChance = "modify"
AutoResolveMethods.AvoidAttackModify = "modify"
AutoResolveMethods.GetCombatEfficiency = function (method_list)
	table.remove_entries(method_list, empty_func)
	table.remove_entries(method_list, return_0)
	local count = #(method_list or "")
	if count == 0 then return return_0 end
	if count == 1 then return method_list[1] end
	return function (obj, ...)
		local result = 0
		for i = 1, count do
			result = Max(result, (method_list[i](obj, ...)))
		end
		return result
	end
end


AppendClass.Robot = {
	__parents = { "RobotConditionComponent" },
}

Robot.ExpireStatusEffects = nil

function Robot:GetHitEffects(target, weapon_def)
	return self:CallReactions_Modify("GatherHitEffects", nil, target, weapon_def)
end

function Robot:ModifyHitChance(hit_chance, target, weapon_def, target_dist)
	return self:CallReactions_Modify("ModifyHitChance", hit_chance, target, weapon_def, target_dist)
end

function Robot:AvoidAttackModify(hit_chance, attacker, weapon_def, attacker_dist)
	return self:CallReactions_Modify("AvoidAttackModify", hit_chance, attacker, weapon_def, attacker_dist)
end

function Robot:GetCombatEfficiency()
	return self:CallReactions_Modify("ModifyCombatEfficiency", self.Combat / scale)
end

function RobotConditionComponent:OnModifiableValueChanged(prop, old_value, new_value)
	if prop == "Movement" then
		self:UpdateMoveSpeed()
	end
end

function Robot:UpdateMoveSpeed()
	if not IsValid(self) then return end
	self:SetMoveSpeed(self.Movement / 100)
end

function Robot:AddRobotCondition(condition_id, reason, source)
	if not condition_id then return end
	local template = type(condition_id) == "table" and condition_id or RobotConditions[condition_id]
	assert(IsKindOf(template, "RobotCondition"))
	if not template
	or not MatchGameScenarioTags(template.AllowedInScenarios)
	or not self:CheckUnitTags(template.UnitTags, true) then
		return
	end
	local cond = template:CreateInstance({
		reason = reason,
		source = source,
	})
	if not self:AddStatusEffect(cond) then
		return
	end
	cond:ApplyModifiers(self)
	self:AddReactions(cond, cond.unit_reactions)
	PostMsg("RobotConditionAdded", self, cond, reason, source)
	ObjModifiedDelayed(self:GetPropContext("RobotConditions"))
	self:OnRobotConditionAdded(cond, reason, source)
	
	if cond.ShowFloatingText and reason ~= "init" and self:ShouldShowCombatUIText() then
		CreateFloatingText(self, cond:GetDisplayNameAndIcon(), nil, "Task")
	end
	return cond
end

function Robot:RemoveRobotConditions(condition_id, reason)
	assert(RobotConditions[condition_id])
	local status_effects = self.status_effects
	for i = #(status_effects or ""), 1, -1 do
		local effect = status_effects[i]
		if effect.id == condition_id and IsKindOf(effect, "RobotCondition") then
			self:RemoveRobotCondition(effect, reason)
		end
	end
end

function Robot:RemoveRobotCondition(cond, reason)
	if type(cond) == "string" then
		cond = self:FirstEffectByIdClass(cond, "RobotCondition")
	end
	if not cond or not self:HasRobotCondition(cond) then return end
	self:RemoveReactions(cond)
	cond:UnapplyModifiers(self)
	self:RemoveStatusEffect(cond)
	PostMsg("RobotConditionRemoved", self, cond)
	ObjModifiedDelayed(self:GetPropContext("RobotConditions"))
	self:OnRobotConditionRemoved(cond, reason)
end

AutoResolveMethods.OnRobotConditionAdded = true
Robot.OnRobotConditionAdded = empty_func

AutoResolveMethods.OnRobotConditionRemoved = true
Robot.OnRobotConditionRemoved = empty_func

DefineStoryBitTrigger("Robot Condition Added", "RobotConditionAdded")
DefineStoryBitTrigger("Robot Condition Removed", "RobotConditionRemoved")

function Robot:HasRobotCondition(cond)
	return self:ForEachEffectByClass("RobotCondition", function(cond_have, cond_check)
		return cond_have == cond_check
	end, cond)
end

function Robot:HasRobotConditionById(cond_id)
	return self:ForEachEffectByClass("RobotCondition", function(cond_have, id)
		return cond_have.id == id
	end, cond_id)
end

AutoResolveMethods.ShouldShowCombatUIText = "and"
function Robot:ShouldShowCombatUIText()
	return GetAccountStorageOptionValue("ShowEnemyCombatUI") or not self.Invader
end

function RobotConditionComponent:OnAttackReceived(attacker, weapon_def, weapon_obj, aoe_power)
	if self:IsDead() then
		return
	end
	local hit_effects
	local crit_effects = weapon_def.RobotCritHitEffect or ""
	if #crit_effects > 0 then
		local chance = ResolveCritChance(weapon_def, attacker, self)
		if chance > 0 and self:Random(100, "CritChance") < chance then
			hit_effects = crit_effects
		end
	end
	hit_effects = hit_effects or weapon_def.RobotHitEffect
	local none_chance = weapon_def.RobotAvoidHitEffect or 0
	if not hit_effects or #hit_effects == 0 or none_chance > 0 and self:Random(100, "RobotCondition") < none_chance then
		return
	end
	local entry = self:TableWeightedRand(hit_effects, 2, "RobotCondition")
	if entry then
		self:AddRobotCondition(entry[1], "combat", nil, attacker)
	end
end

----

AppendClass.WeaponResource = {
	properties = {
		{ category = "Weapon", id = "RobotAvoidHitEffect", name = "Robot Hit Effect Skip", help = "Chance to avoid applying a hit effect to robots.", 
			editor = "number", default = 0, min = 0, max = 100 },
		{ category = "Weapon", id = "RobotHitEffect", name = "Robot Hit Effects", help = "List of robot conditions applied on hit. One condtion is randomly picked.", 
			editor = "preset_id_list", default = false, preset_class = "RobotCondition", weights = true, value_key = 1, weight_key = 2 },
		{ category = "Weapon", id = "RobotCritHitEffect", name = "Robot Critical Hit Effects", help = "List of robot conditions applied on critical hit. One condtion is randomly picked.", 
			editor = "preset_id_list", default = false, preset_class = "RobotCondition", weights = true, value_key = 1, weight_key = 2 },
	}
}
