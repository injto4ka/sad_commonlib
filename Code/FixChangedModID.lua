if FirstLoad then
	ChangedModID = false
end

function IsValidModID(id)
	return type(id) == "string" and id ~= "" and not string.find(id, "[^_%w]")
end

function FixChangedModID(old_id, new_id)
	if not IsValidModID(old_id) then
		ModLogF(true, "Invalid ID", old_id)
		return
	end
	new_id = new_id or ""
	if new_id ~= "" and not IsValidModID(new_id) then
		ModLogF(true, "Invalid ID", new_id)
		return
	end
	local changed = rawget(_G, "ChangedModID") or {}
	local prev_id = changed[old_id]
	if prev_id then
		if prev_id == new_id then
			return
		end
		ModLogF(true, "Mod ID '%s' replaces '%s' already registered to '%s'", new_id, old_id, prev_id)
	end
	if new_id == "" then
		ModLogF(true, "Mod ID '%s' will not be required in saved games", old_id)
	else
		local mod = table.find_value(ModsList, "id", new_id)
		if not mod then
			ModLogF(true, "Cannot find mod with ID '%s'", new_id)
			return
		end
		ModLogF(true, "Mod '%s' has changed ID '%s' to '%s' in saved games", mod.title, old_id, new_id)
	end
	changed[old_id] = new_id
	rawset(_G, "ChangedModID", changed)
end

----

local orig_GatherGameMetadata = GatherGameMetadata
function GatherGameMetadata(...)
	local metadata = orig_GatherGameMetadata(...)
	if not metadata then
		return
	end
	local moddefs = Mods or empty_table
	local active = metadata.active_mods
	for i, info in ripairs(active) do
		local moddef = moddefs[info.id] or empty_table
		if moddef.optional_mod then
			table.remove(active, i)
			-- leave a debug trace in case that the mod is actually needed and causes problems in saved games when missing:
			metadata.optional_mods = table.create_add(metadata.optional_mods, info)
		end
	end
	return metadata
end

local orig_LoadMetadata = LoadMetadata
function LoadMetadata(...)
	local err, metadata = orig_LoadMetadata(...)
	local changed = rawget(_G, "ChangedModID") or empty_table
	if not err and metadata then
		local moddefs = Mods or empty_table
		local active = metadata.active_mods
		for i, info in ripairs(active) do
			local moddef = moddefs[info.id]
			if moddef and moddef.optional_mod then
				table.remove(active, i)
			else
				local new_id = changed[info.id]
				if new_id then
					if new_id == "" then
						table.remove(active, i)
					else
						info.id = new_id
					end
				end
			end
		end
	end
	return err, metadata
end

AppendClass.ModDef = {
	properties =
	{
		{ category = "Mod", id = "optional_mod", name = "Optional", editor = "bool", default = false, help = "Optional mod are not required in saved games if missing" },
	},
}

if FirstLoad then
	__optional_mod_warning = false
end

local origOnEditorSetProperty = ModDef.OnEditorSetProperty or empty_func
function ModDef:OnEditorSetProperty(prop_id, old_value, ged)
	if prop_id == "optional_mod" and not old_value and self.optional_mod and not __optional_mod_warning then
		__optional_mod_warning = true
		ged:ShowMessage("Warning", "Optional mods that are actually needed in a saved game may break or even crash the game!")
	end
	return origOnEditorSetProperty(self, prop_id, old_value, ged)
end