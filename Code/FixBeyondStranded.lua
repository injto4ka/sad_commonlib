ApplySettings = empty_func
local origInjectCustomActivities
function OnMsg.ClassesGenerate()
	local bs_def, idx = GetModLoaded("q2kyFEG")
	if not bs_def or bs_def.version > 3452 then return end
	local my_idx = table.find(ModsLoaded, CurrentModDef) or 0
	if my_idx > idx then
		local mods_loaded = ModsLoaded
		ModsLoaded = { bs_def }
		local ok, err = procall(ModsLoadCode)
		ModSmartLogF(CurrentModDef, "'Beyond Stranded' needs to set a dependency! Reloading result: %s %s", tostring(ok), err or "")
		ModsLoaded = mods_loaded
	end
	origInjectCustomActivities = RawGetGlobal("InjectCustomActivities")
	InjectCustomActivities = empty_func
end
function OnMsg.ModsReloaded()
	if origInjectCustomActivities then
		origInjectCustomActivities()
		ModSmartLogF(CurrentModDef, "'Beyond Stranded' mod startup fixed")
	end
end