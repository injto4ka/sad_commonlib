local customActions = {
	{ Menubar = "Action", Toolbar = "main", Name = "Copy Code", FuncName = "ActionCopyCode", Icon = "CommonAssets/UI/Ged/rollover-mode.tga" },
}
AppendClassMembers.EditorCustomActions = table.iappend
AppendClass.Preset = {
	EditorCustomActions = customActions,
}

function Preset:ActionCopyCode(ged)
	CopyToClipboard(ValueToLuaCode(self))
end

function OnMsg.ClassesPostprocess()
	ClassDescendants("Preset", function(name, def)
		local actions = def.EditorCustomActions
		if actions ~= Preset.EditorCustomActions then
			for _, action in ipairs(customActions) do
				table.insert_unique(actions, action)
			end
		end
	end)
end

local Preset_GetPresetSaveLocations = Preset.GetPresetSaveLocations
function Preset:GetPresetSaveLocations()
	local list = Preset_GetPresetSaveLocations(self)
	local my_mod = self.mod
	for _, mod in ipairs(ModsLoaded) do
		if mod ~= my_mod and not mod.packed then
			list[#list + 1] = { text = "mod " .. mod.title, value = "Mod/" .. mod.id }
		end
	end
	return list 
end

local Preset_GetSaveFolder = Preset.GetSaveFolder
function Preset:GetSaveFolder(save_in)
	save_in = save_in or self.save_in
	if string.starts_with(save_in, "Mod/") then
		return save_in .. "/Presets"
	end
	return Preset_GetSaveFolder(self, save_in)
end

---- Copy past into mod editor


local origGedRestoreFromClipboard = GedRestoreFromClipboard
function GedRestoreFromClipboard(base_class)
	if base_class ~= "ModItem" then
		return origGedRestoreFromClipboard(base_class)
	end
	local stored_objs = string.gsub(GedClipboard.stored_objs, "PlaceObj%('([%w_]+)',", function(class)
		if g_Classes["ModItem" .. class] then
			return "PlaceObj('ModItem" .. class .. "',"
		end
	end)
	local ok, objs = pcall(GedPasteObjCode, stored_objs, "Ged: Error pasting as mod item!")
	if not ok then
		return
	end
	for i, obj in ripairs(objs) do
		if not IsKindOf(obj, "ModItem") then
			table.remove(objs, i)
		end
	end
	return objs
end

local origGedOpPasteModItem = GedOpPasteModItem
function GedOpPasteModItem(socket, root, path)
	local res, undo = origGedOpPasteModItem(socket, root, path)
	if res == "cannot find where to paste" then
		local orig_class = GedClipboard.base_class
		if not IsKindOf(g_Classes[orig_class], "ModItem") then
			GedClipboard.base_class = "ModItem"
			local ok
			ok, res, undo = pcall(origGedOpPasteModItem, socket, root, path)
			GedClipboard.base_class = orig_class
		end
	end
	return res, undo
end
