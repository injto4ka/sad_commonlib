local action_fx_classes = {"ActionFXSound", "ActionFXObject", "ActionFXDecal", "ActionFXLight", "ActionFXColorization", "ActionFXParticles", "ActionFXRemove"}

local function GetFilteredPresets(obj, group, class)
	local presets = {}
	for _, preset in ipairs(Presets.FXPreset[group]) do
		if preset ~= obj and not preset.Obsolete and not preset.Disabled and IsKindOf(preset, class) then
			presets[#presets + 1] = {
				value = preset.id,
				text = tostring(preset.Action) .. "-" .. tostring(preset.Moment) .. "-" .. tostring(preset.Actor) .. "-" .. tostring(preset.Target) .. " (" .. tostring(preset.id) .. ")"
			}
		end
	end
	table.sortby_field(presets, "text")
	table.insert(presets, 1, {value = "", text = ""})
	return presets
end
local function HasFilteredPresets(obj, group, class)
	for _, preset in ipairs(Presets.FXPreset[group]) do
		if preset ~= obj and not preset.Obsolete and IsKindOf(preset, class) then
			return true
		end
	end
end
local function GetFilteredGroups(obj, class)
	local groups = {}
	for group in pairs(Presets.FXPreset) do
		if type(group) == "string" and group ~= "Obsolete" and HasFilteredPresets(obj, group, class) then
			groups[#groups + 1] = group
		end
	end
	table.sort(groups)
	table.insert(groups, 1, "")
	return groups
end

for _, class in ipairs(action_fx_classes) do
	AppendClass["ModItem" .. class] = {
		properties = { 
			{
				category = "Mod", id = "__copy_group", name = "Copy from group", default = "", editor = "combo", dont_save = true,
				items = function(obj)
					return GetFilteredGroups(obj, class)
				end,
				no_edit = function (obj)
					return not obj.HasGroups
				end,
			},
			{
				category = "Mod", id = "__copy", name = "Copy from", default = "", editor = "combo", dont_save = true,
				items = function(obj) 
					return GetFilteredPresets(obj, obj.__copy_group, class) 
				end,
				read_only = function(self)
					return self.__copy_group == ""
				end,
			},
		},
		OnCopyFrom = function(self, preset)
			RebuildFXRules()
		end
	}
end