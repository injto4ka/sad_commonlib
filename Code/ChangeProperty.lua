local forbidden_classes = {
	Achievement = true,
	PlayStationActivities = true,
	RichPresence = true,
	TrophyGroup = true,
}
local options = CurrentModOptions or empty_table

AppendClass.ModItemChangePropBase = {
	properties = {
		{ category = "Item", id = "PropColor", name = "Prop Color", editor = "color", default = false, alpha = false, },
		{ category = "Item", id = "ValueColor", name = "Value Color", editor = "color", default = false, alpha = false, },
		{ category = "Mod", id = "NoCollisionWarnings", name = "No Collision Warnings", editor = "bool", default = false },
	},
	HasObsolete = true,
	HasDisableConditions = true,
	HasRequiredDlc = true,
}

local properties = ModItemChangePropBase.properties
local class_prop, idx = table.find_value(properties, "id", "TargetClass")
class_prop.items = function(self)
	local items = {}
	for name in pairs(Presets) do
		if not forbidden_classes[name] then
			items[#items + 1] = name
		end
	end
	table.sort(items)
	table.insert(items, 1, "")
	return items
end

table.insert(properties, idx + 1, {
	category = "Mod",
	id = "TargetGroup", name = "Group",
	default = "", editor = "choice",
	items = function(self)
		local class = self.TargetClass
		local classdef = g_Classes[class]
		return PresetGroupNames(classdef and classdef.PresetClass or class)
	end,
	no_edit = function(self)
		local def = g_Classes[self.TargetClass]
		return not def or def.GlobalMap
	end,
	reapply = true,
})

local id_prop, idx = table.find_value(properties, "id", "TargetId")
id_prop.items = function(self)
	local class, group = self.TargetClass, self.TargetGroup
	local def = g_Classes[class]
	if def and def.GlobalMap then
		return PresetsCombo(class)
	elseif group ~= "" then
		return PresetsCombo(class, group)
	end
end

function ModItemChangePropBase:ResolveTargetMap()
	local classdef = g_Classes[self.TargetClass]
	if not classdef then
		return
	end
	local name = classdef.GlobalMap
	if name then
		return _G[name]
	end
	local class = classdef.PresetClass or self.TargetClass
	return table.get(Presets, class, self.TargetGroup)
end

function ModItemChangePropBase:GetTargetValueData()
	return table.get(ModItemChangeProp_OrigValues, self.TargetClass, self.TargetGroup, self.TargetId, self.TargetProp)
end

function ModItemChangePropBase:SetTargetValueData(data)
	return table.set(ModItemChangeProp_OrigValues, self.TargetClass, self.TargetGroup, self.TargetId, self.TargetProp, data)
end

function ModItemChangePropBase:SetTargetValue(value)
	self.tweaked_value = value
end

function ModItemChangePropBase:GetTargetValue()
	local value = self.tweaked_value
	if value == nil then
		if self.EditType == "Append To Table" then
			return false
		end
		local preset = self:ResolveTargetPreset()
		local prop = preset and self:ResolvePropTarget(preset)
		if prop then
			value = preset:GetProperty(self.TargetProp)
			local clone = preset:ClonePropertyValue(value, prop)
			if not rawequal(clone, value) then
				self.tweaked_value = clone
				value = clone
			end
		end
	end
	return value
end

function ModItemChangePropBase:GetOriginalValue()
	local data = self:GetTargetValueData()
	local orig_value = data and data.OrigValue
	if orig_value ~= nil then
		return orig_value
	end
	return self:GetPropValue()
end

function ModItemChangePropBase:ResolvePropTarget(preset)
	preset = preset or self:ResolveTargetPreset()
	local props = preset and preset:GetProperties()
	return props and table.find_value(props, "id", self.TargetProp)
end

function ModItemChangePropBase:GetPropValue()
	local preset = self:ResolveTargetPreset()
	local prop = preset and self:ResolvePropTarget(preset)
	if prop then
		return preset:GetProperty(self.TargetProp)
	end
end

function ModItemChangePropBase:ApplyChange(apply)
	local preset = self:ResolveTargetPreset()
	if not preset then return end
	apply = apply and not self.Obsolete and not self:IsModItemDisabled()
	local data = self:GetTargetValueData()
	local new_value
	if apply then
		if not data then
			data = {
				AppliedChanges = {},
				ChangeStack = {},
				OrigValue = false,
			}
			self:SetTargetValueData(data)
		elseif data.AppliedChanges[self] then
			return
		end
		local current_value = self:GetPropValue()
		if #data.ChangeStack == 0 then
			local prop = self:ResolvePropTarget(preset)
			assert(prop, "Missing target property!")
			if not prop then
				return
			end
			data.OrigValue = current_value
			current_value = preset:ClonePropertyValue(current_value, prop)
		end
		if self.EditType == "Code" then
			local orig_value = data and data.OrigValue
			if orig_value == nil then
				orig_value = current_value
			end
			local ok, res = procall(self.TargetFunc, self, current_value, orig_value)
			if ok then
				new_value = res
			else
				ModLogF(true, "%s %s: %s", self.class, self.mod.title, res)
			end
		elseif self.EditType == "Append To Table" then
			local seen = {}
			new_value = {}
			for _, list in ipairs{current_value, self.tweaked_value} do
				for _, v in ipairs(list) do
					if not seen[v] then
						seen[v] = true
						new_value[#new_value + 1] = v
					end
				end
			end
		else -- Replace
			new_value = self.tweaked_value
		end
		if not data.AppliedChanges[self] then
			table.insert(data.ChangeStack, self)
		end
		data.AppliedChanges[self] = new_value
	elseif data and data.AppliedChanges[self] then
		data.AppliedChanges[self] = nil
		table.remove_value(data.ChangeStack, self)
		local obj = data.ChangeStack[#data.ChangeStack]
		if not obj then
			new_value = data.OrigValue
		else
			new_value = data.AppliedChanges[obj]
		end
	end
	if new_value ~= nil then
		self:AssignValue(preset, new_value)
	end
end

local CanAppendToTableListProps = {
	dropdownlist = true,
	string_list = true,
	preset_id_list = true,
	number_list = true,
	point_list = true,
	T_list = true,
	nested_list = true,
	property_array = true,
}

function ModItemChangePropBase:CanAppendToTable()
	local prop = self:ResolvePropTarget()
	return prop and CanAppendToTableListProps[prop.editor]
end

function FuncPieces:ChangeProp_items()
	local items = {"Replace", "Code"}
	if self:CanAppendToTable() then
		table.insert(items, "Append To Table")
	end
	return items
end

SetPropMeta("ModItemChangePropBase", "EditType", "items", FuncPieces.ChangeProp_items, CurrentModDef)

function ModItemChangePropBase:OnEditorSetProperty(prop_id, old_value, ged)
	if self:GetPropertyMetadata(prop_id).reapply then
		self:ApplyChange(false)
		self.tweaked_value = nil
	end
	if self.EditType == "Append To Table" and not self:CanAppendToTable() then
		self.EditType = "Replace"
	end
	return ModItem.OnEditorSetProperty(self, prop_id, old_value, ged)
end

-- Fix the usage of the default prop vallue as an override
function ModItemChangePropBase:IsDefaultPropertyValue(prop_id, ...)
	if prop_id == "TargetValue" then
		return
	end
	return ModItem.IsDefaultPropertyValue(self, prop_id, ...)
end

function ModItemChangePropBase:AssignValue(preset, value)
	preset:SetProperty(self.TargetProp, value)
	local target_class = g_Classes[self.TargetId]
	if target_class and target_class.__generated_by_class == preset.class then
		rawset(target_class, self.TargetProp, value)
	end
	preset:PostLoad()
	ModSmartLogF(self.mod, "%s '%s' %s = %s", preset.class, self.TargetId, self.TargetProp, SimpleValueToStr(value))
end

---- Fix the conflict warning when the only difference is the group:

function ModItemChangePropBase:CheckCollisionWidth(other)
	return self.TargetId == other.TargetId and self.TargetProp == other.TargetProp and self:ResolveTargetMap() == other:ResolveTargetMap() and not other.Obsolete and not other:IsModItemDisabled()
end

function ModItemChangePropBase:GetCollisionError()
	if not self.mod or self.Obsolete or self:IsModItemDisabled() then
		return
	end
	local target_preset = self:ResolveTargetPreset()
	if target_preset and IsKindOf(target_preset, "ModItem") then
		return string_format("Changing the property '%s' of mod item '%s' is suggested to be done inside the dedicated preset mod item that already exists in this mod.", self.TargetProp, target_preset.id)
	end
	
	local err = self.mod:ForEachItem(function(mod_item)
		if mod_item ~= self and IsKindOf(mod_item, "ModItemChangePropBase") and self:CheckCollisionWidth(mod_item) then
			return string_format("The property '%s' is already modified in mod item '%s'", self.TargetProp, mod_item.name and mod_item.name ~= "" and mod_item.name or (mod_item.TargetId .. "." .. mod_item.TargetProp))
		end
	end)
	if err then
		return err
	end
	
	for _, mod in ipairs(ModsLoaded) do
		if mod:ItemsLoaded() then
			local err = mod:ForEachItem(function(mod_item)
				if mod.id ~= self.mod.id and IsKindOf(mod_item, "ModItemChangePropBase") then
					if self:CheckCollisionWidth(mod_item) then
						return string_format("The property '%s' is already modified in loaded mod '%s'", self.TargetProp, mod.title)
					end
				end
			end)
			if err then
				return err
			end
		end
	end
end

function ModItemChangePropBase:GetWarning()
	if self.NoCollisionWarnings or options.IgnorePropCollision then
		return
	end
	return self:GetCollisionError()
end

local origOnModLoad = ModItemChangePropBase.OnModLoad
function ModItemChangePropBase:OnModLoad()
	local err = self:GetCollisionError()
	if err then
		self:ModLogF("\nCollision warning: %s\n", err)
	end
	return origOnModLoad(self)
end

function ModItemChangePropBase:OverwriteProp(prop_id, props, prop)
	local preset = self:ResolveTargetPreset()
	local my_prop = table.find_value(props, "id", prop_id)
	if not my_prop or not preset then return end -- shield just in case
	local keep = {
		id = my_prop.id,
		name = my_prop.name,
		category = my_prop.category,
		no_edit = my_prop.no_edit,
		dont_save = my_prop.dont_save,
		read_only = my_prop.read_only,
		os_path = my_prop.os_path,
	}
	table.clear(my_prop)
	table.overwrite(my_prop, keep)
	for name, value in pairs(prop) do
		if not keep[name] then
			if type(value) == "function" then
				my_prop[name] = function(self, ...)
					return value(preset, ...)
				end
			else
				my_prop[name] = value
			end
		end
	end
end

function ModItemChangePropBase:GetModItemDescription()
	if self.name ~= "" then
		return ModItem.GetModItemDescription(self)
	end
	local dscr
	local name = "<TargetId>"
	if self.NameColor then
		name = ColorToTag(self.NameColor) .. name .. "</color>"
	end
	local prop = "<TargetProp>"
	if self.PropColor then
		prop = ColorToTag(self.PropColor) .. prop .. "</color>"
	end
	local value = self:GetTargetValue()
	if self.EditType == "Code" or not IsSimpleType(value) then
		dscr = Untranslated(name .. "." .. prop)
	else
		if type(value) == "number" then
			local prop_meta = self:GetPropertyMetadata("TargetValue")
			if prop_meta.scale then
				value = FormatNumberProp(value, prop_meta.scale)
			end
		else
			value = ValueToStr(value)
		end
		if self.ValueColor then
			value = ColorToTag(self.ValueColor) .. value .. "</color>"
		end
		dscr = Untranslated(name .. "." .. prop .. " = " .. value)
	end
	return self:AddEditorPostfix(dscr)
end

----

AppendClass.ModItemLootDefEdit = {
	properties = {
		{ id = "TargetGroup" },
	},
}

function ModItemLootDefEdit:GetPropValue()
	local preset = self:ResolveTargetPreset()
	return preset and table.icopy(preset)
end

----

local origSoundTypePostLoad = SoundTypePreset.PostLoad or empty_func
function SoundTypePreset:PostLoad()
	origSoundTypePostLoad(self)
	if DataLoaded then
		LoadSoundType(self)
	end
end

local origSoundPostLoad = SoundPreset.PostLoad or empty_func
function SoundPreset:PostLoad()
	origSoundPostLoad(self)
	if DataLoaded then
		LoadSoundBank(self)
	end
end
SoundPreset.OnEditorNew = nil

----

-- Show the ID of the FX presets
AppendClass.FXPreset = {
	properties = {
		{ category = "Preset", id = "Id", editor = "text", read_only = true, },
	},
}

----
