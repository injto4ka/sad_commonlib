ModItemSoundPreset.EditorName = "Sound Bank"

----

if LuaRevision < 352677 then
	-- Modded sound crash on playing
	-- Fixed in game verison 1.22.240404
	if FirstLoad then
		SoundProcessed = {}
		origPlaySound = PlaySound
		origSetSound = CObject.SetSound
	end

	local function FetchSoundDurations(sample_or_sound)
		if SoundProcessed[sample_or_sound] then
			return
		end
		SoundProcessed[sample_or_sound] = true
		local bank = SoundPresets[sample_or_sound]
		if not bank then
			GetSoundDuration(sample_or_sound)
			return
		end
		for i, sample in ipairs(bank) do
			GetSoundDuration(sample.file)
		end
	end


	PlaySound = function(sample_or_sound, ...)
		FetchSoundDurations(sample_or_sound)
		return origPlaySound(sample_or_sound, ...)
	end

	CObject.SetSound = function(self, sample_or_sound, ...)
		FetchSoundDurations(sample_or_sound)
		return origSetSound(self, sample_or_sound, ...)
	end
end