local ipairs = ipairs
local IsKindOf = IsKindOf
local procall = procall

function ModDef:ForEachItem(callback, ...)
	for i, item in ipairs(self.items) do
		local res = item:ForEachItem(callback, ...)
		if res ~= nil then
			return res
		end
	end
end

function ModDef:GetItemsOfKind(class)
	local results = {}
	self:ForEachItem(function(mc)
		if IsKindOf(mc, class) then
			results[#results+1] = mc
		end
	end)
	return results
end

function ModDef:HasFolders()
	for _, item in ipairs(self.items) do
		if item.is_mod_folder then
			return true
		end
	end
end

function ModDef:ForEachFolder(exclude, callback, ...)
	for _, item in ipairs(self.items) do
		if item ~= exclude then
			local res = item:ForEachFolder(exclude, callback, ...)
			if res ~= nil then
				return res
			end
		end
	end
end

function GetModFolderNames(obj)
	local mod = obj.mod
	if not mod then return end
	local names = {}
	mod:ForEachFolder(obj, function(folder, names)
		names[folder:GetFullName()] = true
	end, names)
	names = table.keys(names, true)
	table.insert(names, 1, "")
	return names
end

function ModDef:GatherItems(items)
	items = items or {}
	self:ForEachItem(function(item, items)
		items[#items + 1] = item
	end, items)
	return items
end

local flat_funcs = {"GetOptionItems", "UpdateEntities", "UpdateLocTables"}

for _, func_name in ipairs(flat_funcs) do
	local origFunc = ModDef[func_name]
	ModDef[func_name] = function(self, ...)
		if not self:HasFolders() then
			return origFunc(self, ...)
		end
		local items = self.items
		self.items = self:GatherItems()
		local success, res1, res2, res3 = procall(origFunc, self, ...)
		self.items = items
		if not success then return end
		return res1, res2, res3
	end
end

function ResolveModItemPath(root, path)
	if not path or #path < 2 then return "invalid selection" end
	assert(path[1] == 1)

	local mod = root[path[1]]
	local mod_items = mod.items
	local item = mod_items[path[2]]
	
	local current_item, parent, last_idx = item, mod_items, path[2]
	for i=3,#path,1 do
		last_idx = path[i]
		parent = current_item
		current_item = parent[last_idx]
	end
	
	return current_item, parent, last_idx
end
local ResolveModItemPath = ResolveModItemPath

function GedOpTestModItem(socket, root, path)
	local mod_item = ResolveModItemPath(root, path)
	if not mod_item then
		return
	end
	mod_item:TestModItem(socket)
end

local origGedOpModItemHelp = GedOpModItemHelp
function GedOpModItemHelp(socket, root, path)
	if #path >= 2 then 
		local mod_item = ResolveModItemPath(root, path)
		if mod_item then
			local filename = DocsRoot .. mod_item.class .. ".md.html"
			local path_to_mod_item = ConvertToOSPath(filename)
			OpenAddress(path_to_mod_item)
			return
		end
	end
	return origGedOpModItemHelp(socket, root, path)
end

----

function ModItem:ForEachItem(callback, ...)
	return callback(self, ...)
end

ModItem.ForEachFolder = empty_func

----

DefineClass.ModItemFolder =  {
	__parents = { "ModItem", "Container" },

	EditorName = "Folder",
	--EditorSubmenu = "UI",
	
	ContainerClass = "ModItem",
	EditorView = Untranslated("<ModItemDescription> <color 75 105 198><u(comment)></color>"),
	
	is_mod_folder = true,
	HasRequiredDlc = false,
}

function ModItemFolder:ForEachItem(callback, ...)
	for _, item in ipairs(self) do
		local res = item:ForEachItem(callback, ...)
		if res ~= nil then
			return res
		end
	end
end

function ModItemFolder:ForEachFolder(exclude, callback, ...)
	if exclude == self then
		return
	end
	local res = callback(self, ...)
	if res ~= nil then
		return res
	end
	for _, item in ipairs(self) do
		if exclude ~= item then
			local res = item:ForEachFolder(exclude, callback, ...)
			if res ~= nil then
				return res
			end
		end
	end
end

function ModItemFolder:GedTreeChildren()
	return self
end

function ModItemFolder:NeedsResave()
	for _, item in ipairs(self) do
		if item:NeedsResave() then
			return true
		end
	end
end

function ModItemFolder:OnModLoad(mod)
	for _, item in ipairs(self) do
		item.mod = mod
		local OnModLoad = item.OnModLoad or empty_func
		procall(OnModLoad, item, mod)
		mod.has_data = mod.has_data or item.is_data
	end
end

function ModItemFolder:OnModUnload(mod)
	for _, item in ipairs(self) do
		local OnModUnload = item.OnModUnload or empty_func
		procall(OnModUnload, item, mod)
		item:delete()
	end
end

local call_funcs = {"MarkClean", "PreSave", "PostSave", "OnEditorNew", "OnEditorDelete", "AssignToConsts"}
for _, func_name in ipairs(call_funcs) do
	ModItemFolder[func_name] = function(self, ...)
		for _, item in ipairs(self) do
			local func = item[func_name] or empty_func
			procall(func, item, ...)
		end
	end
end

function ModItemFolder:OnEditorNew(parent, ...)
	local mod = self.mod
	for _, item in ipairs(self) do
		item.mod = mod
		procall(item.OnEditorNew, item, self, ...)
	end
	return ModItem.OnEditorNew(self, parent, ...)
end

function ModItemFolder:GetFullName(parent, ...)
	local name = self.name
	local parent = self
	local GetParent = GetParentTableOfKindNoCheck
	while true do
		parent = GetParent(parent, "ModItemFolder")
		if not parent then
			break
		end
		name = parent.name .. " / " .. name
	end
	return name
end

function OnMsg.ClassesPostprocess()
	ModItemFolder.__ancestors.ModItemConstDef = true -- Hack!!! Mimic a ModItemConstDef in order to assign constants on start
end
	
----

function OnMsg.ModCodeChanged(file, change)
	for i,mod in ipairs(ModsLoaded) do
		if not mod.packed then
			for _, item in ipairs(mod.items) do
				if item:IsKindOf("ModItemFolder") then
					item:ForEachItem(function(child_item)
						if child_item:IsKindOf("ModItemCode") and string.find_lower(file, child_item.name) then
							ObjModified(child_item)
							return true
						end
					end)
				end
			end
		end
	end
end

function OnMsg.ClassesPostprocess() -- reapply changed properties to generated classes
	for _, mod in ipairs(ModsLoaded) do
		if mod:ItemsLoaded() then
			for _, item in ipairs(mod.items) do
				if item:IsKindOf("ModItemFolder") then
					item:ForEachItem(function(child_item)
						if IsKindOf(child_item, "ModItemChangePropBase") and child_item.TargetProp ~= "__children" then
							local preset = child_item:ResolveTargetPreset()
							local class = g_Classes[child_item.TargetId]
							if class and preset and class.__generated_by_class == preset.class then
								rawset(class, child_item.TargetProp, preset[child_item.TargetProp])
							end
						end
					end)
				end
			end
		end
	end
end

local function ParentNodeByPath(root, path)
	for i = 1, #path - 1 do
		if not root then return end
		local f = root.GedTreeChildren
		root = rawget(f and (f(root) or empty_table) or root, path[i])
	end
	return root
end

local function TreeRelocateNode(root, old_path, new_path)
	local old_parent = ParentNodeByPath(root, old_path)
	local new_parent = ParentNodeByPath(root, new_path)
	local node = table.remove(TreeNodeChildren(old_parent), old_path[#old_path])
	local parent_table = TreeNodeChildren(new_parent)
	table.insert(parent_table, new_path[#new_path], node)
	ParentTableModified(node, parent_table)
	ObjModified(old_parent)
	ObjModified(new_parent)
	ObjModified(root)
end

function GedOpTreeMoveItemInwards(socket, root, selection)
	if IsTreeMultiSelection(selection) then
		return GedTreeMultiOp(socket, root, "GedOpTreeMoveItemInwards", "forward_inwards", selection)
	end

	if not selection then return end
	local parent = ParentNodeByPath(root, selection)
	local new_path = table.copy(selection)
	local leaf_idx = table.remove(new_path)
	if leaf_idx > 1 then
		local parent_leaf_idx = selection.parent_leaf_idx or (leaf_idx - 1)
		local new_parent = TreeNodeChildren(parent)[parent_leaf_idx]
		if not IsKindOf(new_parent, "Container") then
			return "error"
		end
		local items = TreeNodeChildren(ParentNodeByPath(root, selection))
		if not new_parent:IsValidSubItem(items[leaf_idx]) then
			return "error"
		end
		table.insert(new_path, parent_leaf_idx)
		table.insert(new_path, #TreeNodeChildren(new_parent) + 1)
		TreeRelocateNode(root, selection, new_path)
		return new_path, function() TreeRelocateNode(root, new_path, selection) end
	end
end

local origGedOpTreeNewItem = GedOpTreeNewItem
function GedOpTreeNewItem(socket, root, path, class_or_instance, idx)
	if path and not idx then
		local parent = TreeNodeByPath(root, unpack_params(path)) 
		if IsKindOf(parent, "Container") and parent:IsValidSubItem(class_or_instance) then
			idx = "child"
		end
	end
	return origGedOpTreeNewItem(socket, root, path, class_or_instance, idx)
end

function FuncPieces:ModItemCodeValidate(value)
	value = value:trim_spaces()
	if value == "" then
		return "Please enter a valid name"
	end
	return self.mod:ForEachItem(function(item)
		if item.class == "ModItemCode" and item ~= self and item.name == value then
			return "A code item with that name already exists"
		end
	end)
end

SetPropMeta("ModItemCode", "name", "validate", FuncPieces.ModItemCodeValidate, CurrentModDef)

function GetModEntities(typ)
	local results = {}
	-- ignore type for now, return all types
	for _, mod in ipairs(ModsLoaded) do
		mod:ForEachItem(function(mc)
			if IsKindOf(mc, "BaseModItemEntity") then
				results[#results+1] = mc.entity_name
			end
		end)
	end
	table.sort(results)
	return results
end

AppendClass.ModItem = {
	properties = {
		{ category = "Mod", id = "ModFolder", name = "Folder", editor = "choice", default = "", items = GetModFolderNames, dont_save = true },
	},
}

function ModItem:GetModFolder()
	local parent = ParentTableCache[self]
	return IsKindOf(parent, "ModItemFolder") and parent:GetFullName()
end

function ModItem:SetModFolder(name)
	name = name or ""
	local mod = self.mod
	if not mod then
		return
	end
	local old_folder = ParentTableCache[self]
	local old_items = IsKindOf(old_folder, "ModItemFolder") and old_folder or mod.items
	local idx = table.find(old_items, self)
	if not idx then
		return
	end
	
	local new_folder
	if name then
		new_folder = mod:ForEachFolder(self, function(folder, name)
			if folder:GetFullName() == name then
				return folder
			end
		end, name)
		if not IsKindOf(new_folder, "ModItemFolder") then
			return
		end
	end
	local new_items = new_folder or mod.items
	if type(new_items) ~= "table" or new_items == old_items or new_items == self then
		return
	end
	
	table.remove(old_items, idx)
	table.insert(new_items, self)
	ParentTableCache[self] = new_folder or mod
	ObjModified(mod)
end

function ForEachLoadedModItem(class, callback, ...)
	if type(class) == "function" then
		return ForEachLoadedModItem(nil, class, callback, ...)
	end
	for _,mod in ipairs(ModsLoaded) do
		mod:ForEachItem(function(item, ...)
			if not class or item:IsKindOf(class) then
				procall(callback, item, ...)
			end
		end, ...)
	end
end