local origGetFuncSourceStringIndent = GetFuncSourceStringIndent
function GetFuncSourceStringIndent(indent, value, new_name, ...)
	if new_name == "" then
		local name = FuncNames[value]
		if name then
			return name
		end
		local body = GetFuncBody(value)
		for name, src in pairs(FuncSources) do
			if table.find(src, body) then
				return name
			end
		end
	end
	return origGetFuncSourceStringIndent(indent, value, new_name, ...)
end

local origGetGlobalName = GetGlobalName
function GetGlobalName(func)
	local name = FuncNames[func]
	if name then
		return name
	end
	return origGetGlobalName(func)
end