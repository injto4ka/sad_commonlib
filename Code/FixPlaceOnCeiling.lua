-- Fixed in game version 1.22.240321
if LuaRevision >= 352127 then
	return
end
function CeilingPieceAutoResolve:IsConstructionLockIgnored(lock, pos, angle, lock_pos, lock_angle)
	if lock.lock_class == "ceiling" then
		local x, y, z, x0, y0, z0
		if lock_pos then
			x, y, z = lock_pos:xyz()
		elseif IsValid(lock) then
			x, y, z = lock:GetPosXYZ()
		end
		if pos then
			x0, y0, z0 = pos:xyz()
		elseif IsValid(self) then
			x0, y0, z0 = self:GetPosXYZ()
		end
		return x == x0 and y == y0 and z == z0
	else
		local z, z0
		if lock_pos then
			z = lock_pos:z()
		elseif IsValid(lock) then
			z = lock:GetZ()
		end
		if pos then
			z0 = pos:z()
		elseif IsValid(self) then
			z0 = self:GetZ()
		end
		return z and z0 and round(z, const.SlabSizeZ) >= z0
	end
end