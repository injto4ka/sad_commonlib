AppendClass.Unit = {
	path_last_count = max_int,
	path_step_approach = false,
	path_step_loop = false,
	path_step_loop_pos = false,
	path_step_loops = 0,
	path_step_max_loops = 10,
}

Unit.OnLoopFixed_OnGotoStep = Unit.OnGotoStep
function Unit:OnGotoStep(...)
	local last_count = self.path_last_count
	local current_count = self:GetPathPointCount()
	if last_count < current_count then
		if self.path_step_loop == last_count then
			if self.path_step_loop_pos == GetPosHash(self) then
				if self.path_step_loops == self.path_step_max_loops then
					DebugPrint("Interrupted path loop of", self.class, "at", self:GetPosXYZ())
					return true -- interrupt
				end
				--print("path_step_loops", self.class, self.path_step_loops, last_count, current_count)
				self.path_step_loops = self.path_step_loops + 1
			end
		elseif not self.path_step_loop or self.path_step_approach then
			self.path_step_loop = last_count
			self.path_step_loop_pos = GetPosHash(self)
		end
		self.path_step_approach = nil
	elseif last_count > current_count then
		if self.path_step_loop and not self.path_step_approach then
			self.path_step_approach = true
		end
	end
	self.path_last_count = current_count
	return self:OnLoopFixed_OnGotoStep(...)
end

Unit.OnLoopFixed_StopMoving = Unit.StopMoving
function Unit:StopMoving(...)
	self.path_last_count = nil
	self.path_step_loop = nil
	self.path_step_loops = nil
	self.path_step_loop_pos = nil
	self.path_step_approach = nil
	return self:OnLoopFixed_StopMoving(...)
end