AppendClass.ModItemOption = {
	properties = {
		{ id = "OnApply", editor = "func", default = false, params = "self, value, prev_value", help = "Will always be invoked when the mod options are applied (requires Common Lib)" },
		{ id = "RequiresRestart", name = "Requires Game Restart", editor = "bool", default = false, help = "Will restart the game on change (requires Common Lib)" },
		{ id = "DisplayColor", name = "Display Color", editor = "color", default = false, alpha = false, },
		{ id = "HiddenCondition", editor = "expression", default = false, params = "self", help = "Will not be visible to the player (requires Common Lib)" },
	},
	HasDisableConditions = true,
	HasRequiredDlc = true,
}

if FirstLoad then
	ModOptionsApplied = {}
end

function OnMsg.ApplyModOptions(mod_id)
	local mod_def = Mods[mod_id]
	local options = mod_def.options or empty_table
	local applied = ModOptionsApplied[mod_id]
	if not applied then
		applied = {}
		ModOptionsApplied[mod_id] = applied
	end
	local printed
	for _, item in ipairs(mod_def:GetOptionItems()) do
		if not item:IsModItemDisabled() then
			local id = item.name
			local value = options[id]
			if value == nil then
				value = item.DefaultValue
			end
			local prev_value = applied[id]
			if item.OnApply then
				procall(item.OnApply, item, value, prev_value)
			end
			if item.RequiresRestart and prev_value ~= nil and value ~= prev_value then
				local text = Untranslated("Applying some of the options requires a game restart!")
				CreateRealTimeThread(AskToRestartGame, text, 1000)
			end
			applied[id] = value
			printed = table.create_add(printed, string.format("\t%s = %s\n", id, SimpleValueToStr(value)))
		end
	end
	if printed then
		table.sort(printed, CmpLower)
		mod_def:ModLogF("Options applied:\n%s", table.concat(printed))
	end
end

function ModItemOption:GetOptionMeta()
	local no_edit = function(prop)
		if self:IsModItemDisabled() then
			return true
		end
		if self.HiddenCondition then
			local success, value = procall(self.HiddenCondition, self)
			return success and value
		end
	end
	local display_name = self.DisplayName
	if not display_name or display_name == "" then
		display_name = self.name
	end
	if self.DisplayColor then
		display_name = ColorToTag(self.DisplayColor) .. display_name .. "</color>"
	end
	return {
		id = self.name,
		name = Untranslated(display_name),
		editor = self.ValueEditor,
		default = self.DefaultValue,
		help = self.Help,
		no_edit = no_edit,
	}
end

ModItemOption.EditorView = Untranslated("<color 128 128 128>Option</color> <ModItemDescription> <color 75 105 198><u(comment)></color>")
ModItemOption.ModItemDescription = Untranslated("<name> = <color 200 150 100><DefaultValueStr></color>")
function ModItemOption:GetDefaultValueStr()
	return tostring(self.DefaultValue)
end

function ModItemOption:OnEditorNew()
	if self.mod then
		self.mod.has_options = true
		self.mod:LoadOptions()
	end
end

----

ModItemOptionToggle.GetEditorView = nil
function ModItemOptionToggle:GetDefaultValueStr()
	return self.DefaultValue and "On" or "Off"
end

----

if LuaRevision < 352677 then
	-- All mods have options
	-- Fixed in game verison 1.22.240404
	local function FixModOptionList()
		UIForEachControl(XTemplates.OptionsDialog, function(control)
			if control.comment == "mods" then
				control.condition = function (parent, context, item, i)
					return IsKindOf(item.options, "ModOptionsObject") and next(item.options:GetProperties())
				end
				return true
			end
		end)
	end

	OnMsg.ModsReloaded = FixModOptionList
end