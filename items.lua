return {
PlaceObj('ModItemFolder', {
	'name', "Constants",
}, {
	PlaceObj('ModItemConstDef', {
		LoadBeforeCode = true,
		NameColor = 4285046365,
		ValueColor = 4288254604,
		id = "MaxAnimSpeed",
		value = 32767,
	}),
	PlaceObj('ModItemConstDef', {
		LoadBeforeCode = true,
		NameColor = 4285046365,
		ValueColor = 4288254604,
		id = "MaxAnimWeight",
		value = 32767,
	}),
	PlaceObj('ModItemConstDef', {
		LoadBeforeCode = true,
		NameColor = 4285046365,
		ValueColor = 4288254604,
		id = "AnimWeightScale",
		value = 100,
	}),
	PlaceObj('ModItemConstDef', {
		LoadBeforeCode = true,
		NameColor = 4285046365,
		ValueColor = 4288254604,
		group = "Social",
		id = "SocialDistance",
		scale = "m",
		value = 10000,
	}),
	PlaceObj('ModItemConstDef', {
		NameColor = 4285046365,
		ValueColor = 4288254604,
		group = "Translation",
		id = "SAD_ModError",
		lines = 1,
		translate = true,
		type = "text",
		value = T(490137281641, --[[ModItemConstDef Translation SAD_ModError value]] "Mod Error"),
	}),
	PlaceObj('ModItemConstDef', {
		NameColor = 4285046365,
		ValueColor = 4288254604,
		group = "Translation",
		id = "SAD_MapEditorHelp_Title",
		lines = 1,
		translate = true,
		type = "text",
		value = T(399317583379, --[[ModItemConstDef Translation SAD_MapEditorHelp_Title value]] "Welcome to the Map Editor!"),
	}),
	PlaceObj('ModItemConstDef', {
		NameColor = 4285046365,
		ValueColor = 4288254604,
		group = "Translation",
		id = "SAD_MapEditorHelp_Body",
		lines = 1,
		multiline = true,
		translate = true,
		type = "text",
		value = T(212486169267, --[[ModItemConstDef Translation SAD_MapEditorHelp_Body value]] "Here are some short tips to get you started.\n\nCamera controls:\n  • <mouse_wheel_up> - zoom in/out\n  • hold <middle_click> - pan the camera\n  • hold <Ctrl> - faster movement\n  • hold <Alt> - look around\n  • hold <Ctrl>+<Alt> - rotate camera\n\nLook through the editor tools on the left - for example, press <N> to place objects.\n\nUse <right_click> to access object properties and actions."),
	}),
	PlaceObj('ModItemConstDef', {
		LoadBeforeCode = true,
		NameColor = 4285046365,
		ValueColor = 4288254604,
		comment = "Remove the NEW keyword",
		group = "config",
		id = "NewFeaturesUpdate",
		lines = 1,
		multiline = true,
		translate = true,
		type = "bool",
		value = false,
	}),
	PlaceObj('ModItemConstDef', {
		LoadBeforeCode = true,
		NameColor = 4285046365,
		ValueColor = 4288254604,
		group = "Default",
		id = "ObsoleteMods",
		items = function (self, prop) return VersionEncodedModsCombo end,
		lines = 1,
		multiline = true,
		translate = true,
		type = "string_list",
		value = {
			"IIcOFRs 1 1 25",
			"Chapi_MapEditor 0 0 588",
		},
	}),
	}),
PlaceObj('ModItemFolder', {
	'name', "Events",
}, {
	PlaceObj('ModItemMsgDef', {
		Description = "Modifies the direct damage inflicted by the unit in combat",
		Params = "damage, weapon_def, attack_target",
		Target = "Unit",
		group = "Unit",
		id = "ModifyDamageInflicted",
	}),
	PlaceObj('ModItemMsgDef', {
		Description = "Modifies the direct damage received by the unit in combat",
		Params = "damage, weapon_def, attacker",
		Target = "Unit",
		group = "Unit",
		id = "ModifyDamageReceived",
	}),
	PlaceObj('ModItemMsgDef', {
		Description = "Modifies the weapon range",
		Params = "range, weapon_def",
		Target = "Unit",
		group = "Unit",
		id = "ModifyWeaponRange",
	}),
	PlaceObj('ModItemMsgDef', {
		Description = "Someone failed to reach a building",
		Params = "bld",
		group = "Unit",
		id = "BuildingBlocked",
	}),
	PlaceObj('ModItemMsgDef', {
		Description = "A previously blocked building has been reached",
		Params = "bld",
		group = "Unit",
		id = "BuildingUnblocked",
	}),
	}),
PlaceObj('ModItemFolder', {
	'name', "Game Rules",
}, {
	PlaceObj('ModItemGameRuleDef', {
		NameColor = 4294665474,
		comment = "Alternative efficiency computing",
		description = T(890194455311, --[[ModItemGameRuleDef Mastery description]] "Efficiency of activities depending on Manipulation and Consciousness will increase significantly when these stats are high, but will degrade more quickly when they are low."),
		display_name = T(350930787257, --[[ModItemGameRuleDef Mastery display_name]] "Mastery"),
		id = "Mastery",
		mod_version_major = 1,
		mod_version_minor = 8,
		msg_reactions = {
			PlaceObj('MsgReaction', {
				Event = "PreHumanInit",
				Handler = function (self, unit)
					unit.GetSkillAndEfficiency = unit.GetSkillAndEfficiency_Mastery
				end,
				param_bindings = false,
			}),
			PlaceObj('MsgReaction', {
				Event = "LoadGame",
				Handler = function (self, game)
					if IsGameRuleActive(self.id) then
						for _, unit in ipairs(AllSurvivors) do
							unit.GetSkillAndEfficiency = unit.GetSkillAndEfficiency_Mastery
						end
					end
				end,
				param_bindings = false,
			}),
		},
	}),
	}),
PlaceObj('ModItemFolder', {
	'name', "Options",
}, {
	PlaceObj('ModItemOptionToggle', {
		'name', "AutoEnableNewMods",
		'DisplayName', "Auto Enable New Mods",
		'Help', "Will auto enable mods after subscribing to them",
		'DefaultValue', true,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "AutoEnableModDependency",
		'DisplayName', "Auto Enable Mod Dependencies",
		'Help', "Will auto enable mod dependencies",
		'DefaultValue', true,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "AutoButcherCorpses",
		'DisplayName', "Auto Butcher Corpses",
		'Help', "Auto butcher (or scavenge) enemies when defeated",
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "HideEmptyResources",
		'DisplayName', "Hide Empty Resources",
		'Help', "Don't display empty resources in the infobar UI",
		'OnApply', function (self, value, prev_value)
			            rawset(StrandedInfobar, "hide_empty_resources", value)
		end,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "ShowAdditionalTasks",
		'DisplayName', "Show Additional Tasks",
		'Help', "Display visual indication for the future activity tasks for the selected unit",
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "StartGamePaused",
		'comment', "Copy of the same option in Bonus Starting Survivors",
		'DisableIfMods', {
			"WEmzEd3",
		},
		'DisplayName', "Start Game Paused",
		'Help', "Available if the mod Bonus Starting Survivors isn't enabled",
		'OnApply', function (self, value, prev_value)
			config.StartGameOnPause = value
		end,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "NoAutosaveOnModMap",
		'DisplayName', "No Autosave On Mod Map",
		'Help', "Disable autosaving while on the mod editor map",
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "FixGameStutter",
		'DisplayName', "Fix Game Stuttering",
		'Help', "Enable if the game is stuttering / freezing. May decrease the FPS as a side effect. The problem is related to a recent update of Windows 11.",
		'OnApply', function (self, value, prev_value)
			local changed = table.changed(config, "FixGameStutter")
			
			if not value then
				if changed then
					table.restore(config, "FixGameStutter")
				end
				return
			end
			
			if changed then
				return
			end
			
			local hwinfo = GetHardwareInfo(EngineOptions.GraphicsApi, EngineOptions.GraphicsAdapterIndex)
			if not hwinfo then
				return
			end
			
			local cores = hwinfo.cpuCores
			
			table.change(config, "FixGameStutter", {
				RenderTaskThreads = Clamp(cores - 1, 3, config.RenderTaskThreads),
				GameTaskThreads = Clamp(cores - 1, 3, config.GameTaskThreads),
				IOTaskThreads = Clamp(cores / 2, 2, config.IOTaskThreads),
				AudioTaskThreads = Clamp(cores / 2, 2, config.AudioTaskThreads),
			})
		end,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "AllowOutdatedMods",
		'DisplayName', "Allow outdated mods",
		'Help', "This is not recommended!",
		'RequiresRestart', true,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "DisableErrorReporting",
		'comment', "Non debug",
		'DisplayName', "Disable Error Popups",
		'Help', "Don't show error popups about mods",
		'HiddenCondition', function (self) return Platform.debug end,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "NoLocChangeOnCopy",
		'comment', "Debug",
		'DisplayName', "No Loc ID change on Copy",
		'Help', "When copying items in the mod editor, don't change their loc ID (Debug only)",
		'DisplayColor', RGBA(255, 244, 194, 255),
		'HiddenCondition', function (self) return not Platform.debug end,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "ShowPropLocID",
		'comment', "Debug",
		'DisplayName', "Show Loc ID",
		'Help', "Will display the loc ID below each translated editor property (Debug only)",
		'DisplayColor', RGBA(255, 244, 194, 255),
		'HiddenCondition', function (self) return not Platform.debug end,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "IgnorePropCollision",
		'comment', "Debug",
		'DisplayName', "Ignore Prop Collision",
		'Help', "Ignore change property collision warning",
		'DisplayColor', RGBA(255, 244, 194, 255),
		'HiddenCondition', function (self) return not Platform.debug end,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "StartOnEditorMap",
		'comment', "Debug",
		'DisplayName', "Start On Editor Map",
		'Help', "The game will directly start in the editor map (Debug only)",
		'DisplayColor', RGBA(255, 244, 194, 255),
		'HiddenCondition', function (self) return not Platform.debug end,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "AutoSyncMissingMods",
		'comment', "Debug",
		'DisplayName', "Auto Sync Missing Mods",
		'Help', "Will download and enable missing mods when loading a saved game (Debug only)",
		'DisplayColor', RGBA(255, 244, 194, 255),
		'HiddenCondition', function (self) return not Platform.debug or not Platform.steam end,
		'DefaultValue', true,
	}),
	PlaceObj('ModItemOptionToggle', {
		'name', "NoDebugForEditors",
		'comment', "Debug",
		'DisplayName', "No Debug For Editors",
		'Help', "Start the editors without debugging to gain performance. Don't use this option if the game freezes! (Debug only)",
		'DisplayColor', RGBA(255, 244, 194, 255),
		'HiddenCondition', function (self) return not Platform.debug end,
	}),
	PlaceObj('ModItemOptionNumber', {
		'name', "StoryBitLogSize",
		'comment', "Debug",
		'DisplayName', "Story Bit Log Size",
		'Help', "Modifies the size of the story bit history log (Debug only)",
		'DisplayColor', RGBA(255, 244, 194, 255),
		'HiddenCondition', function (self) return not Platform.debug end,
		'DefaultValue', 100,
		'MaxValue', 1000,
	}),
	}),
PlaceObj('ModItemFolder', {
	'name', "Updates",
}, {
	PlaceObj('ModItemUpdate', {
		'name', "ModUpdates",
		'mod_map_only', true,
		'version_major', 1,
		'version_minor', 8,
		'whats_new_message', T(457017492715, "<em>Game Updates</em>\n\nThe mods can notify the player about updates via a new mod item <positive>Mod Update</positive>\n\n\n<em>Translated Texts</em>\n\nDuplicated localization ID-s will raise an error on game start.\n\nThe localization ID will be displayed under the respective translated texts in the editors (only in debug mode if the option <positive>Show Loc ID</positive> is enabled)."),
		'whats_new_time', 1736602136,
	}),
	PlaceObj('ModItemUpdate', {
		'name', "Consts",
		'mod_map_only', true,
		'version_major', 1,
		'version_minor', 10,
		'whats_new_message', T(861538098588, "<em>Const mod items</em>\n\nThe const mod items can load before the mod code items via the property <em>Load Before Code</em>"),
		'whats_new_time', 1741076096,
	}),
	}),
PlaceObj('ModItemFolder', {
	'name', "Code",
}, {
	PlaceObj('ModItemCode', {
		'name', "_Utils",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/_Utils.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "_Stubs",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/_Stubs.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "Debug",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/Debug.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "GeneralFixes",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/GeneralFixes.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "Loc",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/Loc.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "DirectDamage",
		'comment', "Refactor for the damage to living units to match the direct integrity damage",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/DirectDamage.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "NoLoadingScreens",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/NoLoadingScreens.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "ScenarioEquip",
		'comment', "Allows to equip the characters with alternative equipment",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/ScenarioEquip.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "ManualInteractions",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/ManualInteractions.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "FakePreorder",
		'comment', "Allows loading savegames made with the Preorder DLC",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/FakePreorder.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "Optimizations",
		'comment', "Improved speed or memory consumption",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/Optimizations.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "Components",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/Components.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "NewLogic",
		'comment', "Effects & Conditions",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/NewLogic.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "AutoButcher",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/AutoButcher.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "SkinMaterials",
		'comment', "Changes the appearance of the slab presets",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/SkinMaterials.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "RobotCondition",
		'comment', "Equivalent of teh HealthCondition for Robots & Buildings",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/RobotCondition.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "GameRules",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/GameRules.lua",
	}),
	PlaceObj('ModItemCode', {
		'name', "LightningStrikes",
		'NameColor', RGBA(237, 246, 177, 255),
		'CodeFileName', "Code/LightningStrikes.lua",
	}),
	PlaceObj('ModItemFolder', {
		'name', "Fixes",
	}, {
		PlaceObj('ModItemCode', {
			'name', "FixNests",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixNests.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixBeyondStranded",
			'comment', "Fixes for the mod 'Beyond Stranded'",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixBeyondStranded.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixChangedModID",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixChangedModID.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixLightModels",
			'comment', "[WIP]",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixLightModels.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixRoomVisibility",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixRoomVisibility.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixModEntity",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixModEntity.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixSavedTraits",
			'comment', "Obsolete",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixSavedTraits.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixPresetChoice",
			'comment', "Avoid listing all presets when a specific preset class is requested",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixPresetChoice.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixDespawn",
			'comment', "Ensures a reachable despawn destination",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixDespawn.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixStepLoop",
			'comment', "Avoids a soft lock during movement",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixStepLoop.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixPlaceOnCeiling",
			'comment', "Obsolete",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixPlaceOnCeiling.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixMissingFuncs",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixMissingFuncs.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixModSound",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixModSound.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixWeaponDir",
			'comment', "Corrects the weapon direction when making a cone attack",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixWeaponDir.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixEfficiency",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixEfficiency.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixModOption",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixModOption.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FixUI",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FixUI.lua",
		}),
		}),
	PlaceObj('ModItemFolder', {
		'name', "Modding",
	}, {
		PlaceObj('ModItemCode', {
			'name', "ModEditor",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/ModEditor.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "SaveToMod",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/SaveToMod.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "ModCapture",
			'comment', "Store/Restore the enabled mods",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/ModCapture.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "ModFolder",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/ModFolder.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "MapEditor",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/MapEditor.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "ModItems",
			'comment', "New mod items",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/ModItems.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "CopyFromActionFX",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/CopyFromActionFX.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "ChangeProperty",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/ChangeProperty.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "FuncSaveByName",
			'comment', "Allows to save certain predefined functions by name",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/FuncSaveByName.lua",
		}),
		PlaceObj('ModItemCode', {
			'name', "GedAppSingleton",
			'comment', "Property to open ged apps as singleton",
			'NameColor', RGBA(237, 246, 177, 255),
			'CodeFileName', "Code/GedAppSingleton.lua",
		}),
		}),
	}),
PlaceObj('ModItemFolder', {
	'name', "Entities",
}, {
	PlaceObj('ModItemEntity', {
		'name', "FlamethrowerTurret",
		'comment', "Shrinked the collision to avoid being hit through adjacent walls",
		'NameColor', RGBA(119, 184, 211, 255),
		'fade_category', "Never",
		'entity_name', "FlamethrowerTurret",
		'material_type', "Metal",
	}),
	PlaceObj('ModItemEntity', {
		'name', "Fort_Concrete_Slab",
		'comment', "Fixed missing collision in the lower part",
		'NameColor', RGBA(119, 184, 211, 255),
		'fade_category', "Never",
		'entity_name', "Fort_Concrete_Slab",
		'material_type', "Concrete",
	}),
	PlaceObj('ModItemEntity', {
		'name', "Fort_Concrete_Slab_02",
		'comment', "Fixed missing collision in the lower part",
		'NameColor', RGBA(119, 184, 211, 255),
		'fade_category', "Never",
		'entity_name', "Fort_Concrete_Slab_02",
		'material_type', "Concrete",
	}),
	PlaceObj('ModItemEntity', {
		'name', "Fort_Concrete_Slab_03",
		'comment', "Fixed missing collision in the lower part",
		'NameColor', RGBA(119, 184, 211, 255),
		'fade_category', "Never",
		'entity_name', "Fort_Concrete_Slab_03",
		'material_type', "Concrete",
	}),
	PlaceObj('ModItemEntity', {
		'name', "Terrace_Carbon_Top_01",
		'comment', "Fixed missing collision",
		'NameColor', RGBA(119, 184, 211, 255),
		'fade_category', "Never",
		'entity_name', "Terrace_Carbon_Top_01",
		'material_type', "Carbon",
	}),
	PlaceObj('ModItemEntity', {
		'name', "Terrace_Concrete_Top_01",
		'comment', "Fixed missing collision",
		'NameColor', RGBA(119, 184, 211, 255),
		'fade_category', "Never",
		'entity_name', "Terrace_Concrete_Top_01",
		'material_type', "Concrete",
	}),
	}),
PlaceObj('ModItemFolder', {
	'name', "Localization",
}, {
	PlaceObj('ModItemLocTable', {
		'NameColor', RGBA(179, 128, 230, 255),
		'language', "French",
		'filename', "Localization/French.csv",
	}),
	PlaceObj('ModItemLocTable', {
		'NameColor', RGBA(179, 128, 230, 255),
		'language', "Brazilian",
		'filename', "Localization/Brazilian.csv",
	}),
	PlaceObj('ModItemLocTable', {
		'NameColor', RGBA(179, 128, 230, 255),
		'language', "German",
		'filename', "Localization/German.csv",
	}),
	PlaceObj('ModItemLocTable', {
		'NameColor', RGBA(179, 128, 230, 255),
		'language', "Japanese",
		'filename', "Localization/Japanese.csv",
	}),
	PlaceObj('ModItemLocTable', {
		'NameColor', RGBA(179, 128, 230, 255),
		'language', "Koreana",
		'filename', "Localization/Korean.csv",
	}),
	PlaceObj('ModItemLocTable', {
		'NameColor', RGBA(179, 128, 230, 255),
		'language', "Polish",
		'filename', "Localization/Polish.csv",
	}),
	PlaceObj('ModItemLocTable', {
		'NameColor', RGBA(179, 128, 230, 255),
		'language', "Russian",
		'filename', "Localization/Russian.csv",
	}),
	PlaceObj('ModItemLocTable', {
		'NameColor', RGBA(179, 128, 230, 255),
		'language', "Schinese",
		'filename', "Localization/Schinese.csv",
	}),
	PlaceObj('ModItemLocTable', {
		'NameColor', RGBA(179, 128, 230, 255),
		'language', "Spanish",
		'filename', "Localization/Spanish.csv",
	}),
	}),
PlaceObj('ModItemFolder', {
	'name', "UI",
}, {
	PlaceObj('ModItemXTemplate', {
		NameColor = 4294964457,
		group = "Shortcuts",
		id = "SAD_CommonLib_UI",
		PlaceObj('XTemplateAction', {
			'comment', "Map Editor help",
			'ActionId', "Help",
			'ActionMode', "Editor",
			'ActionName', T(515768319493, --[[ModItemXTemplate SAD_CommonLib_UI ActionName]] "Help"),
			'ActionIcon', "CommonAssets/UI/Ged/help.tga",
			'ActionToolbar', "XEditorStatusbar",
			'ActionShortcut', "F1",
			'ActionMouseBindable', false,
			'OnAction', empty_func,
			'IgnoreRepeated', true,
		}),
		PlaceObj('XTemplateAction', {
			'ActionId', "actionExtendField",
			'ActionMode', "Game",
			'ActionSortKey', "2000",
			'ActionName', T(883436914073, --[[ModItemXTemplate SAD_CommonLib_UI ActionName]] "Extend field"),
			'ActionShortcut', "]",
			'ActionBindable', true,
			'ActionMouseBindable', false,
			'BindingsMenuCategory', "Construction",
			'OnAction', function (self, host, source, ...)
				local obj = SelectedObj
				if obj and IsKindOf(obj, "FieldBase") then
					BeginConstruction(obj.class, {
						action_field = obj,
						building_class_override = GetFarmBuildingClassForCrop(obj.Crop),
					})
				end
			end,
			'IgnoreRepeated', true,
			'replace_matching_id', true,
		}),
		PlaceObj('XTemplateAction', {
			'ActionId', "actionReduceField",
			'ActionMode', "Game",
			'ActionSortKey', "2001",
			'ActionName', T(248365614783, --[[ModItemXTemplate SAD_CommonLib_UI ActionName]] "Reduce field"),
			'ActionShortcut', "[",
			'ActionBindable', true,
			'ActionMouseBindable', false,
			'BindingsMenuCategory', "Construction",
			'OnAction', function (self, host, source, ...)
				local obj = SelectedObj
				if obj and IsKindOf(obj, "FieldBase") then
					BeginConstruction(obj.class, {
						action_field = obj,
						building_class_override = GetFarmBuildingClassForCrop(obj.Crop),
						reduce_field = true,
					})
				end
			end,
			'IgnoreRepeated', true,
			'replace_matching_id', true,
		}),
		PlaceObj('XTemplateAction', {
			'ActionId', "Debug",
			'ActionTranslate', false,
			'ActionName', "Debug",
			'ActionMenubar', "DevMenu",
			'OnActionEffect', "popup",
			'replace_matching_id', true,
		}, {
			PlaceObj('XTemplateAction', {
				'comment', "Copy Camera Location to Clipboard",
				'RolloverText', "Copy Camera Location to Clipboard",
				'ActionId', "CopyCameraLocation",
				'ActionTranslate', false,
				'ActionName', "Copy Camera Location to Clipboard",
				'ActionShortcut', "Ctrl-Alt-F1",
				'OnAction', function (self, host, source, ...)
					local camera_string = GetCameraLocationString()
					CopyToClipboard(camera_string)
					print("Copied to clipboard: ", camera_string)
				end,
				'replace_matching_id', true,
			}),
			PlaceObj('XTemplateAction', {
				'comment', "VME Viewer",
				'RolloverText', "VME Viewer",
				'ActionId', "DE_VMEViewer",
				'ActionTranslate', false,
				'ActionName', "VME Viewer",
				'ActionIcon', "CommonAssets/UI/Icons/alert attention danger error warning.png",
				'OnAction', function (self, host, source, ...)
					OpenVMEViewer()
				end,
				'replace_matching_id', true,
			}),
			}),
	}),
	PlaceObj('ModItemTextStyle', {
		ShadowColor = 4278190080,
		ShadowSize = 1,
		TextColor = 4294964457,
		TextFont = T(492921893003, --[[ModItemTextStyle NextActivities TextFont]] "droid, 15, bold"),
		id = "NextActivities",
	}),
	PlaceObj('ModItemXTemplate', {
		__is_kind_of = "XText",
		group = "Stranded",
		id = "IPRobotCondition",
		PlaceObj('XTemplateWindow', {
			'__class', "XText",
			'RolloverTemplate', "Rollover",
			'RolloverText', T(485910787504, --[[ModItemXTemplate IPRobotCondition RolloverText]] "<EffectsDescription>\n"),
			'RolloverTitle', T(142936025813, --[[ModItemXTemplate IPRobotCondition RolloverTitle]] "<DisplayName>"),
			'Clip', false,
			'RolloverOnFocus', true,
			'RelativeFocusOrder', "new-line",
			'TextStyle', "InfopanelText",
			'Translate', true,
			'Text', T(--[[ModItemXTemplate IPRobotCondition Text]] "<DisplayNameAndIcon>"),
		}, {
			PlaceObj('XTemplateTemplate', {
				'__template', "RolloverLine",
				'Margins', box(0, -1, 0, 0),
			}),
			PlaceObj('XTemplateFunc', {
				'name', "OnSetFocus",
				'func', function (self, ...)
					XText.OnSetFocus(self, ...)
					local tab = GetDialog(self):ResolveId("idFirstTab")
					local scroll_area = tab and tab:ResolveId("idContent")
					if scroll_area then
						scroll_area:ScrollIntoView(self)
					end
				end,
			}),
			}),
	}),
	PlaceObj('ModItemXTemplate', {
		__is_kind_of = "InfopanelSection",
		group = "Infopanel Sections",
		id = "tabOverview_RobotConditions",
		PlaceObj('XTemplateTemplate', {
			'__context_of_kind', "Robot",
			'__context', function (parent, context) return SubContext(context, {context:GetPropContext("RobotConditions")}) end,
			'__condition', function (parent, context) return IsObjectFieldResearched(context, UIPlayer) or not context.Invader end,
			'__template', "InfopanelSection",
			'Id', "idRobotConditions",
			'OnContextUpdate', function (self, context, ...)
				XContextWindow.OnContextUpdate(self, context, ...)
				local unit = ResolvePropObj(context)
				local conditions = {}
				local counters = {}
				unit:ForEachEffectByClass("RobotCondition", function(condition)
					local count = counters[condition.id] or 0
					if count == 0 then
						conditions[#conditions + 1] = condition
					end
					counters[condition.id] = count + 1
				end)
				for _, condition in ipairs(conditions) do
					condition.display_count = counters[condition.id]
				end
				
				local container = self.idFactors
				local sn, fn = #container, #conditions
				
				--clear all Ids to prevent duplication
				for _, child in ipairs(container) do
					child:SetId("")
				end
				
				--repurpose
				local min = Min(sn, fn)
				for i=1,min do
					local condition = conditions[i]
					local child = container[i]
					child:SetContext(condition)
					child:SetId(condition.id)
				end
				--create new
				for i = min+1, fn do
					local condition = conditions[i]
					local child = XTemplateSpawn("IPRobotCondition", container, condition)
					child:SetId(condition.id)
					child:Open()
				end
				--delete unused
				for i = #container,fn+1,-1 do
					local win = container[i]
					if win then
						win:delete()
					end
				end
				self.idNone:SetVisible(#container == 0)
			end,
			'Title', T(802421784986, --[[ModItemXTemplate tabOverview_RobotConditions Title]] "Health conditions"),
		}, {
			PlaceObj('XTemplateWindow', {
				'Id', "idFactors",
				'LayoutMethod', "VList",
			}),
			PlaceObj('XTemplateWindow', {
				'__class', "XText",
				'Id', "idNone",
				'Visible', false,
				'FoldWhenHidden', true,
				'TextStyle', "InfopanelText",
				'Translate', true,
				'Text', T(730408337527, --[[ModItemXTemplate tabOverview_RobotConditions Text]] "None"),
			}),
			}),
	}),
	PlaceObj('ModItemXTemplate', {
		__is_kind_of = "XDialog",
		comment = "Copy of the original but larger and with a scrollbar for the choices",
		group = "Stranded",
		id = "StoryBitPopupLarge",
		PlaceObj('XTemplateWindow', {
			'__class', "ResolvePriorityDialog",
			'HAlign', "center",
			'VAlign', "center",
			'MinWidth', 1200,
			'MinHeight', 600,
			'MaxWidth', 1200,
			'MaxHeight', 600,
			'FadeOutTime', 500,
			'DialogPriority', 100,
			'HideByHigherPriority', true,
		}, {
			PlaceObj('XTemplateWindow', {
				'__class', "XHideDialogs",
				'LeaveDialogIds', {
					"InfobarDialog",
					"SurvivorSelectorDialog",
					"InGameInterface",
				},
			}),
			PlaceObj('XTemplateLayer', {
				'layer', "XPauseLayer",
			}),
			PlaceObj('XTemplateLayer', {
				'layer', "XCameraLockLayer",
			}),
			PlaceObj('XTemplateLayer', {
				'layer', "XSuppressInputLayer",
			}),
			PlaceObj('XTemplateLayer', {
				'layer', "ScreenBlur",
			}),
			PlaceObj('XTemplateFunc', {
				'name', "Open(self, ...)",
				'func', function (self, ...)
					TurboSpeed(false)
					NetSetPause(true)
					CloseInGameInterfaceMode()
					ResolvePriorityDialog.Open(self, ...)
					HideGamepadCursor(self)
					DisableGameShortcuts(self)
					XDestroyRolloverWindow(true)
					EnableXShortcutToRelation(true, "dpad")
				end,
			}),
			PlaceObj('XTemplateFunc', {
				'name', "Close(self, result)",
				'func', function (self, result)
					if result and self.context then
						local choices = self.idChoices
						for i=#choices,1,-1 do
							local choice = choices[i]
							if i == result then
								choice:SetFadeOutTime(self:GetFadeOutTime() + 300)
							end
							choice:SetParent(terminal.desktop)
							choice.OnPress = empty_func
							local x, y = choice.box:minxyz()
							local w, h = choice.box:sizexyz()
							choice.UpdateLayout = function(self)
								self:SetBox(x, y, w, h)
								XButton.UpdateLayout(self)
							end
							choice:Close()
						end
						NetSyncEvent("CloseStoryBitPopup", self.context.id, result)
					end
					EnableXShortcutToRelation(false)
					ShowGamepadCursor(self)
					SetInviewHint("StoryBit")
					XDialog.Close(self, result)
				end,
			}),
			PlaceObj('XTemplateFunc', {
				'name', "OnDelete",
				'func', function (self, ...)
					NetSetPause(false)
					EnableGameShortcuts(self)
					ResolvePriorityDialog.OnDelete(self, ...)
				end,
			}),
			PlaceObj('XTemplateFunc', {
				'name', "OnSetFocus(self)",
				'func', function (self)
					EnableXShortcutToRelation(true, "dpad")
					local choices_win = self:ResolveId("idChoices")
					if choices_win and GetUIStyleGamepad() then
						local focus = not choices_win:IsFocused(true) and choices_win:GetRelativeFocus(point(1, 0), "down")
						if focus then 
							focus:SetFocus()
						end
					end
					if (self.context and #self.context.choices or 0) > 1 then
						SetInviewHint("StoryBit", T(487894043889, "<ShortcutName('actionSelect')> Select<space><DPadUpDown> Navigate<space><rsupdown> Scroll text"))
					else
						SetInviewHint("StoryBit", T(647709775223, "<ShortcutName('actionSelect')> Select<space><rsupdown> Scroll text"))
					end
					ResolvePriorityDialog.OnSetFocus(self)
				end,
			}),
			PlaceObj('XTemplateFunc', {
				'name', "OnKillFocus(self)",
				'func', function (self)
					EnableXShortcutToRelation(false)
					ResolvePriorityDialog.OnKillFocus(self)
				end,
			}),
			PlaceObj('XTemplateWindow', {
				'Dock', "box",
				'VAlign', "top",
				'MinHeight', 65,
				'MaxHeight', 65,
			}, {
				PlaceObj('XTemplateWindow', {
					'__class', "XFrame",
					'IdNode', false,
					'Dock', "box",
					'Transparency', 30,
					'RelativeFocusOrder', "skip",
					'Image', "UI/Hud/ip_header",
					'FrameBox', box(20, 0, 50, 0),
				}),
				}),
			PlaceObj('XTemplateWindow', {
				'__class', "XBlurRect",
				'Margins', box(0, 65, 0, 0),
				'Dock', "box",
				'BlurRadius', 20,
				'Mask', "UI/Hud/ip_background",
				'FrameLeft', 20,
				'FrameRight', 50,
				'FrameBottom', 20,
			}),
			PlaceObj('XTemplateWindow', {
				'__class', "XFrame",
				'Id', "idBackground",
				'IdNode', false,
				'Margins', box(0, 65, 0, 0),
				'Dock', "box",
				'Transparency', 100,
				'Image', "UI/Hud/ip_background",
				'FrameBox', box(20, 0, 50, 20),
			}),
			PlaceObj('XTemplateWindow', {
				'__class', "XFrame",
				'IdNode', false,
				'Margins', box(9, 65, 9, 9),
				'Dock', "box",
				'Transparency', 100,
				'RelativeFocusOrder', "skip",
				'Image', "UI/Hud/line",
				'TileFrame', true,
			}),
			PlaceObj('XTemplateWindow', {
				'Margins', box(8, 9, 0, 9),
				'Dock', "left",
				'LayoutMethod', "VList",
				'LayoutVSpacing', 10,
			}, {
				PlaceObj('XTemplateWindow', {
					'__class', "XImage",
					'Id', "idImage",
					'IdNode', false,
					'HAlign', "right",
					'Image', "UI/Messages/message_placeholder",
					'ImageFit', "smallest",
				}),
				PlaceObj('XTemplateWindow', {
					'comment', "lines",
					'__class', "XImage",
					'Id', "idPortraitLines",
					'IdNode', false,
					'Dock', "box",
					'LayoutMethod', "None",
					'Visible', false,
					'FoldWhenHidden', true,
					'Image', "UI/Portraits/screen_effect",
					'ImageFit', "smallest",
				}),
				PlaceObj('XTemplateCode', {
					'comment', "set image",
					'run', function (self, parent, context)
						local image = parent:ResolveId("idImage")
						if image and (context and context.image or "") ~= "" then
							image:SetImage(context.image)
							local portrait_lines = parent:ResolveId("idPortraitLines")
							if portrait_lines and string.find(context.image, "_large") then
								portrait_lines:SetVisible(true)
							end
						end
					end,
				}),
				}),
			PlaceObj('XTemplateWindow', {
				'Margins', box(20, 14, 20, 30),
				'MinWidth', 470,
				'LayoutMethod', "VPanel",
				'FillOverlappingSpace', true,
			}, {
				PlaceObj('XTemplateWindow', {
					'__class', "XText",
					'Id', "idTitle",
					'Margins', box(0, 5, 0, 0),
					'Dock', "top",
					'VAlign', "bottom",
					'TextStyle', "StoryBitTitle",
					'Translate', true,
					'Text', T(965852397567, --[[ModItemXTemplate StoryBitPopupLarge Text]] "<title>"),
				}),
				PlaceObj('XTemplateWindow', {
					'Margins', box(0, 25, 0, 10),
					'VAlign', "top",
					'MinWidth', 470,
				}, {
					PlaceObj('XTemplateWindow', {
						'__class', "XContentTemplateScrollArea",
						'Id', "idScrollArea",
						'MinWidth', 470,
						'VScroll', "idScroll",
					}, {
						PlaceObj('XTemplateWindow', {
							'__class', "XText",
							'Id', "idText",
							'Padding', box(0, 0, 0, 0),
							'TextStyle', "StoryBitText",
							'Translate', true,
							'Text', T(689048817878, --[[ModItemXTemplate StoryBitPopupLarge Text]] "<text>"),
						}),
						PlaceObj('XTemplateWindow', {
							'__class', "XScrollThumb",
							'Id', "idScroll",
							'ZOrder', 10,
							'Padding', box(10, 0, 0, 0),
							'Dock', "right",
							'HAlign', "center",
							'MinWidth', 16,
							'MaxWidth', 16,
							'FoldWhenHidden', false,
							'DrawOnTop', true,
							'MouseCursor', "UI/Cursors/cursor_rollover.tga",
							'Target', "node",
							'FullPageAtEnd', true,
							'SnapToItems', true,
							'AutoHide', true,
							'MinThumbSize', 30,
							'FixedSizeThumb', false,
						}, {
							PlaceObj('XTemplateWindow', {
								'__class', "XFrame",
								'Dock', "box",
								'Image', "UI/Hud/scrollbar_pad",
								'FrameBox', box(0, 6, 0, 6),
							}),
							PlaceObj('XTemplateWindow', {
								'__class', "XFrame",
								'Id', "idThumb",
								'Image', "UI/Hud/scrollbar",
								'FrameBox', box(0, 5, 0, 5),
								'SqueezeX', false,
							}),
							}),
						}),
					PlaceObj('XTemplateAction', {
						'ActionId', "idScrollDown",
						'ActionGamepad', "RightThumbDown",
						'OnAction', function (self, host, source, ...)
							local scroll_area = host:ResolveId("idScrollArea")
							if scroll_area:GetVisible() then
								return scroll_area:OnMouseWheelBack()
							end
						end,
					}),
					PlaceObj('XTemplateAction', {
						'ActionId', "idScrollUp",
						'ActionGamepad', "RightThumbUp",
						'OnAction', function (self, host, source, ...)
							local scroll_area = host:ResolveId("idScrollArea")
							if scroll_area:GetVisible() then
								return scroll_area:OnMouseWheelForward()
							end
						end,
					}),
					}),
				PlaceObj('XTemplateWindow', {
					'Id', "idChoices",
					'Margins', box(0, 10, 0, 10),
					'Dock', "bottom",
					'MaxHeight', 210,
					'LayoutMethod', "VList",
				}, {
					PlaceObj('XTemplateWindow', {
						'__condition', function (parent, context) return context and not context.can_interact end,
						'__class', "XText",
						'Padding', box(0, 0, 0, 0),
						'TextStyle', "StoryBitText",
						'Translate', true,
						'Text', T(696868380593, --[[ModItemXTemplate StoryBitPopupLarge Text]] "Only the game host can select an outcome."),
					}),
					PlaceObj('XTemplateWindow', {
						'__class', "XContentTemplateScrollArea",
						'Id', "idScrollArea2",
						'MinWidth', 470,
						'LayoutMethod', "VList",
						'VScroll', "idScroll2",
					}, {
						PlaceObj('XTemplateForEach', {
							'array', function (parent, context) return context and context.choices end,
							'__context', function (parent, context, item, i, n) return { line_number = i, text = item, actor = context.actor, outcome = context.extra_texts[i], enabled = context.can_interact and context.choice_enabled[i] } end,
							'run_after', function (child, context, item, i, n, last)
								local enabled = context.enabled or false
								child:SetEnabled(enabled)
								if enabled then
									child:SetFocusOrder(point(1, i))
								end
								child.action = XAction:new({
									ActionShortcut = tostring(i),
									ActionShortcut2 = i == last and "Escape" or "",
									ActionGamepad = i == last and "ButtonB" or "",
									OnAction = function(self, host, source)
										host:Close(i)
									end,
									ActionState = function(self, host)
										return not enabled and "disabled"
									end,
								}, GetDialog(child))
							end,
						}, {
							PlaceObj('XTemplateWindow', {
								'__class', "XButton",
								'LayoutMethod', "VList",
								'Background', RGBA(255, 255, 255, 0),
								'FXMouseIn', "ButtonHoverBasic",
								'FXPress', "ButtonClickBasic",
								'FocusedBackground', RGBA(255, 255, 255, 0),
								'OnPressEffect', "action",
								'RolloverBackground', RGBA(255, 255, 255, 0),
								'PressedBackground', RGBA(255, 255, 255, 0),
							}, {
								PlaceObj('XTemplateFunc', {
									'name', "OnSetRollover(self, rollover)",
									'func', function (self, rollover)
										XButton.OnSetRollover(self, rollover)
										self.idText:SetRollover(rollover)
										self.idOutcome:SetRollover(rollover)
									end,
								}),
								PlaceObj('XTemplateWindow', {
									'__class', "XText",
									'Id', "idText",
									'Padding', box(0, 0, 0, 0),
									'TextStyle', "StoryBitAnswer",
									'Translate', true,
									'Text', T(604444143780, --[[ModItemXTemplate StoryBitPopupLarge Text]] "<line_number>. <text>"),
								}),
								PlaceObj('XTemplateWindow', {
									'__class', "XText",
									'Id', "idOutcome",
									'Padding', box(0, 0, 0, 0),
									'FoldWhenHidden', true,
									'TextStyle', "StoryBitOutcome",
									'Translate', true,
									'Text', T(835519342919, --[[ModItemXTemplate StoryBitPopupLarge Text]] "<outcome>"),
									'HideOnEmpty', true,
								}),
								}),
							}),
						PlaceObj('XTemplateWindow', {
							'__class', "XScrollThumb",
							'Id', "idScroll2",
							'ZOrder', 10,
							'Padding', box(10, 0, 0, 0),
							'Dock', "right",
							'HAlign', "center",
							'MinWidth', 16,
							'MaxWidth', 16,
							'FoldWhenHidden', false,
							'DrawOnTop', true,
							'MouseCursor', "UI/Cursors/cursor_rollover.tga",
							'Target', "node",
							'FullPageAtEnd', true,
							'SnapToItems', true,
							'AutoHide', true,
							'MinThumbSize', 30,
							'FixedSizeThumb', false,
						}, {
							PlaceObj('XTemplateWindow', {
								'__class', "XFrame",
								'Dock', "box",
								'Image', "UI/Hud/scrollbar_pad",
								'FrameBox', box(0, 6, 0, 6),
							}),
							PlaceObj('XTemplateWindow', {
								'__class', "XFrame",
								'Id', "idThumb",
								'Image', "UI/Hud/scrollbar",
								'FrameBox', box(0, 5, 0, 5),
								'SqueezeX', false,
							}),
							}),
						PlaceObj('XTemplateAction', {
							'ActionId', "idScrollDown2",
							'ActionGamepad', "RightThumbDown",
							'OnAction', function (self, host, source, ...)
								local scroll_area = host:ResolveId("idScrollArea2")
								if scroll_area:GetVisible() then
									return scroll_area:OnMouseWheelBack()
								end
							end,
						}),
						PlaceObj('XTemplateAction', {
							'ActionId', "idScrollUp2",
							'ActionGamepad', "RightThumbUp",
							'OnAction', function (self, host, source, ...)
								local scroll_area = host:ResolveId("idScrollArea2")
								if scroll_area:GetVisible() then
									return scroll_area:OnMouseWheelForward()
								end
							end,
						}),
						}),
					}),
				}),
			}),
	}),
	}),
PlaceObj('ModItemFolder', {
	'name', "Changed Props",
}, {
	PlaceObj('ModItemFolder', {
		'name', "FX",
	}, {
		PlaceObj('ModItemChangeProp', {
			'comment', "Stops the persistent sound of the energy sword",
			'TargetClass', "FXPreset",
			'TargetGroup', "Default",
			'TargetId', "ndXkf3n_",
			'TargetProp', "EndRules",
			'TargetValue', {
				PlaceObj('ActionFXEndRule', {
					'EndAction', "ApplyBodyPart",
					'EndMoment', "end",
				}),
			},
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		}),
	PlaceObj('ModItemFolder', {
		'name', "Weapons",
	}, {
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "Resource",
			'TargetId', "Melee_LaserSword",
			'TargetProp', "DamageCone",
			'TargetValue', 0,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "Resource",
			'TargetId', "Melee_LaserSword",
			'TargetProp', "ProjectileSpot",
			'TargetValue', "Origin",
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "Resource",
			'TargetId', "Melee_LaserSword",
			'TargetProp', "FriendlyFire",
			'TargetValue', false,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		}),
	PlaceObj('ModItemFolder', {
		'name', "Units",
	}, {
		PlaceObj('ModItemChangeProp', {
			'comment', "Remove the smoking pipe object from the crawlers",
			'TargetClass', "CompositeBodyPreset",
			'TargetId', "CrawlerGimbal",
			'TargetProp', "Entity",
			'TargetValue', "InvisibleObject",
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		}),
	PlaceObj('ModItemFolder', {
		'name', "UI",
	}, {
		PlaceObj('ModItemChangeProp', {
			'comment', "Avoid opening the mod manager twice",
			'TargetClass', "XTemplate",
			'TargetId', "ModManager",
			'TargetProp', "Singleton",
			'TargetValue', true,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		}),
	PlaceObj('ModItemFolder', {
		'name', "Buildings",
	}, {
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "BuildingCompositeDef",
			'TargetId', "CookStove_Electric",
			'TargetProp', "PowerOutput",
			'TargetValue', 500,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "BuildingCompositeDef",
			'TargetId', "CookStove_Electric",
			'TargetProp', "PowerTemperature",
			'TargetValue', 90000,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "BuildingCompositeDef",
			'TargetId', "CookStove_Electric",
			'TargetProp', "RadiationTemperature",
			'TargetValue', 10000,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "BuildingCompositeDef",
			'TargetId', "CookStove_Electric",
			'TargetProp', "RadiationRange",
			'TargetValue', 2000,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "BuildingCompositeDef",
			'TargetId', "CookStove_ScrapMetal",
			'TargetProp', "PowerOutput",
			'TargetValue', 1500,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "BuildingCompositeDef",
			'TargetId', "CookStove_ScrapMetal",
			'TargetProp', "PowerTemperature",
			'TargetValue', 90000,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "BuildingCompositeDef",
			'TargetId', "CookStove_ScrapMetal",
			'TargetProp', "RadiationTemperature",
			'TargetValue', 10000,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "BuildingCompositeDef",
			'TargetId', "CookStove_ScrapMetal",
			'TargetProp', "RadiationRange",
			'TargetValue', 3000,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		PlaceObj('ModItemChangeProp', {
			'comment', "Damage increased to compensate for AoE reduction",
			'TargetClass', "HealthCondition",
			'TargetId', "TurretMissilePod_BlastWound",
			'TargetProp', "HealthLoss",
			'TargetValue', 30000,
			'ValueColor', RGBA(153, 146, 140, 255),
		}),
		}),
	PlaceObj('ModItemFolder', {
		'name', "StoryBits",
	}, {
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "StoryBit",
			'TargetId', "LoveInterest",
			'TargetProp', "Prerequisites",
			'EditType', "Append To Table",
			'TargetValue', {
				PlaceObj('CheckExpression', {
					Expression = function (self, obj) return obj:IsAdult() and obj.other_social:IsAdult() end,
					param_bindings = false,
				}),
			},
		}),
		}),
	PlaceObj('ModItemFolder', {
		'name', "Resources",
	}, {
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "Resource",
			'TargetId', "Alcohol",
			'TargetProp', "adult_only",
			'TargetValue', true,
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "Resource",
			'TargetId', "AlcoholBeer",
			'TargetProp', "adult_only",
			'TargetValue', true,
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "Resource",
			'TargetId', "AlcoholBlueberryWine",
			'TargetProp', "adult_only",
			'TargetValue', true,
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "Resource",
			'TargetId', "AlcoholMoonshine",
			'TargetProp', "adult_only",
			'TargetValue', true,
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "Resource",
			'TargetId', "SmokeleafPipe",
			'TargetProp', "adult_only",
			'TargetValue', true,
		}),
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "Resource",
			'TargetId', "Stimulants",
			'TargetProp', "adult_only",
			'TargetValue', true,
		}),
		}),
	PlaceObj('ModItemFolder', {
		'name', "Lightmodels",
	}, {
		PlaceObj('ModItemFolder', {
			'name', "Sobrius",
		}, {
			PlaceObj('ModItemFolder', {
				'name', "Stormy",
			}, {
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Sunrise",
					'TargetProp', "fog_start",
					'TargetValue', 60000,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Sunrise",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Sunrise",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Sunrise",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(255, 244, 194, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Day",
					'TargetProp', "fog_start",
					'TargetValue', 60000,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(255, 244, 194, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Day",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Sunset",
					'TargetProp', "fog_start",
					'TargetValue', 60000,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Sunset",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Sunset",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Sunset",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Night",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Night",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Stormy_Night",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				}),
			PlaceObj('ModItemFolder', {
				'name', "ThunderStorm",
			}, {
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Sunrise",
					'TargetProp', "fog_start",
					'TargetValue', 60000,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Sunrise",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Sunrise",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Sunrise",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(255, 244, 194, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Day",
					'TargetProp', "fog_start",
					'TargetValue', 60000,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(255, 244, 194, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Day",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Sunset",
					'TargetProp', "fog_start",
					'TargetValue', 60000,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Sunset",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Sunset",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Sunset",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Night",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Night",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_ThunderStorm_Night",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				}),
			PlaceObj('ModItemFolder', {
				'name', "Rain",
			}, {
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Sunrise",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Sunrise",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Sunrise",
					'TargetProp', "cloud_min_count",
					'TargetValue', 15,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Sunrise",
					'TargetProp', "cloud_count_change_time",
					'TargetValue', 40000,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(255, 244, 194, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Day",
					'TargetProp', "cloud_mists",
					'TargetValue', true,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(255, 244, 194, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Day",
					'TargetProp', "cloud_min_count",
					'TargetValue', 15,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(255, 244, 194, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Day",
					'TargetProp', "cloud_max_count",
					'TargetValue', 30,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(255, 244, 194, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Day",
					'TargetProp', "cloud_count_change_time",
					'TargetValue', 40000,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Sunset",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Sunset",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Sunset",
					'TargetProp', "cloud_min_count",
					'TargetValue', 15,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Sunset",
					'TargetProp', "cloud_count_change_time",
					'TargetValue', 40000,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Night",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Night",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Night",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Night",
					'TargetProp', "cloud_count_change_time",
					'TargetValue', 40000,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Rain_Night",
					'TargetProp', "cloud_min_count",
					'TargetValue', 15,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				}),
			PlaceObj('ModItemFolder', {
				'name', "Snow",
			}, {
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Snow_Sunrise",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Snow_Sunrise",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(228, 211, 66, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Snow_Sunrise",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(255, 244, 194, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Snow_Day",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(255, 244, 194, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Snow_Day",
					'TargetProp', "wind",
					'TargetValue', "light_gusts",
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Snow_Sunset",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 64,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Snow_Sunset",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(243, 86, 50, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Snow_Sunset",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Snow_Night",
					'TargetProp', "fog_height_falloff",
					'TargetValue', 400,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Snow_Night",
					'TargetProp', "sky_object_env_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				PlaceObj('ModItemChangeProp', {
					'NameColor', RGBA(137, 141, 236, 255),
					'TargetClass', "LightmodelPreset",
					'TargetGroup', "Default",
					'TargetId', "Sobrius_Snow_Night",
					'TargetProp', "stars_intensity",
					'TargetValue', 0,
					'ValueColor', RGBA(153, 146, 140, 255),
				}),
				}),
			}),
		}),
	PlaceObj('ModItemFolder', {
		'name', "Other",
	}, {
		PlaceObj('ModItemChangeProp', {
			'TargetClass', "GameRuleDef",
			'TargetId', "SurvivorsSkillsMin",
			'TargetProp', "effect_type",
			'TargetValue', "negative",
		}),
		}),
	}),
PlaceObj('ModItemFolder', {
	'name', "Other",
}, {
	PlaceObj('ModItemBugReportTag', {
		Automatic = true,
		comment = "Indicates the presence of the Common Lib",
		id = "Clib",
	}),
	PlaceObj('ModItemNotificationPreset', {
		expiration = 240000,
		fx_action = "UINotificationImportant",
		game_time = true,
		group = "Buildings",
		id = "BlockedBuildings",
		msg_reactions = {
			PlaceObj('MsgReaction', {
				Event = "BuildingBlocked",
				Handler = function (self, bld)
					if bld.player ~= UIPlayer then return end
					self:AddObject(bld)
				end,
			}),
			PlaceObj('MsgReaction', {
				Event = "BuildingUnblocked",
				Handler = function (self, bld)
					if bld.player ~= UIPlayer then return end
					self:RemoveObject(bld)
				end,
			}),
		},
		priority = "Important",
		rollover_text = T(171261307731, --[[ModItemNotificationPreset BlockedBuildings rollover_text]] "Can't be reached"),
		rollover_title = T(454080077433, --[[ModItemNotificationPreset BlockedBuildings rollover_title]] "Unreachable"),
		text = T(495034472435, --[[ModItemNotificationPreset BlockedBuildings text]] "Out of reach"),
		text_many = T(495034472435, --[[ModItemNotificationPreset BlockedBuildings text_many]] "Out of reach"),
	}),
	}),
}
